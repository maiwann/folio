---
layout: projet
title: Zam
vignette_url: /img/vignette/projets/vignette_defaut.jpg
problematique:  Alléger la charge de préparation des réponses aux amendements
competences: analyse de l'activité · améliorations itératives · pair design
categories: ux
bouton: null
---

Cette page est en cours de rédaction.

Ce portfolio date de 2016, il était temps de le mettre un peu à jour… mais sans se laisser piéger par l'envie de perfection. Aussi il y aura plusieurs pages comme celles-ci !

Mais pour Zam… il y a [une série d'articles](https://www.maiwann.net/blog/zam1-premier-contact/) qui détaille le tout ! Bonne lecture :)
