---
layout: projet
title: Exodus Privacy
vignette_url: /img/vignette/projets/vignette_defaut.jpg
problematique: Informer avec clarté sur un sujet technique et préoccupant
competences: Recherche utilisateur · Mobile first
categories: ux
bouton: null
---

Cette page est en cours de rédaction.

Ce portfolio date de 2016, il était temps de le mettre un peu à jour… mais sans se laisser piéger par l'envie de perfection. Aussi il y aura plusieurs pages comme celles-ci !

Mais pour Exodus… il y a [un article](https://framablog.org/2019/11/29/collaborer-pour-un-design-plus-accessible-lexemple-dexodus-privacy/) qui détaille un peu le sujet ! Bonne lecture :)
