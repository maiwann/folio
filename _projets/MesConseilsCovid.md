---
layout: projet
title: Mes Conseils Covid
vignette_url: /img/vignette/projets/vignette_defaut.jpg
problematique: Conseiller de façon personnalisée dans un contexte anxiogène
competences: Mobile first · Pédagogie
categories: ui
bouton: null
---

Cette page est en cours de rédaction.

Ce portfolio date de 2016, il était temps de le mettre un peu à jour… mais sans se laisser piéger par l'envie de perfection. Aussi il y aura plusieurs pages comme celles-ci !

Désolée d'avance !
