---
layout: projet
title: SppNaut
vignette_url: /img/vignette/projets/vignette_defaut.jpg
problematique: Moderniser le processus de publication des instructions nautiques
competences: Analyse de l'activité · Priorisation des fonctionnalités
categories: ux
bouton: null
---

Cette page est en cours de rédaction.

Ce portfolio date de 2016, il était temps de le mettre un peu à jour… mais sans se laisser piéger par l'envie de perfection. Aussi il y aura plusieurs pages comme celles-ci !

Désolée d'avance !
