---
layout: projet
title: Scribouilli
vignette_url: /img/vignette/projets/vignette_defaut.jpg
problematique: Faciliter la création de petits sites aux non-informaticien·nes
competences: Priorisation des fonctionnalités · Test utilisateur·ices
categories: side
bouton: null
---

Cette page est en cours de rédaction.

Ce portfolio date de 2016, il était temps de le mettre un peu à jour… mais sans se laisser piéger par l'envie de perfection. Aussi il y aura plusieurs pages comme celles-ci !

Désolée d'avance !
