---
layout: post
title: "Zam 12: Nouvelles observations"
subtitle: ""
date: 2020-12-18 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam12-nouvelles-observations/
---

# Zam 12: Nouvelles observations

En 2019, je repars observer les utilisateurs pour deux projets de loi :

- À Bercy, au ministère des finances, pour le projet de loi de taxe sur les services numériques
- Au ministère de l'Éducation nationale pour le projet de loi "École de la confiance"

Depuis mes dernières observations, Zam a beaucoup évolué. Le concept des tables chamboule complètement le fonctionnement, et nous voulions voir comment les utilisateurs s'en étaient emparés !

Je vous note ci-dessous pêle-mêle le rapport d'étonnement de ces deux séances d'observation :

## Les notifications invisibles

Après une question d'un des utilisateurs, me demandant si il était envisagé de fournir une information sur la mise à jour d'un amendemnt (savoir lorsqu'il était rectifié si cela arrivait), je lui ai signalé que l'information existait grâce à des notifications qui popaient en bas de son écran.

<figure>
  <img src="/img/posts/2019_Zam/13/1.png" alt="">
</figure>
>>> Capture d'écran avec une notif'

Il m'annonce alors qu'il ne les avait jamais vues ! Et encore mieux : il m'a ensuite dit "qu'il regarderait mieux pour voir la prochaine" et plusieurs notifications sont apparues sans qu'il ne les voient.

Nous avons donc réalisé que ces notifications, que l'on pensait pouvoir être utiles à nos utilisateurs, étaient en fait inconsciemment ignorées, et que le fait de ne pas avoir 90% des informations fournies par ces notifications ne leur manquait pas.

Nous les avons alors enlevées pour alléger l'interface et diminuer la charge cognitive, car ce n'est pas parce que les utilisateurs ne sont pas conscients de leur présence que cela ne leur rajoute pas une information visuelle à traiter !

## Détection des identiques

Les utilisateurs passent beaucoup de temps à lire les amendements puis à essayer de se rappeller si ils ont déjà lu un amendement identique et de quand cela date-t-il (cette lecture, une lecture plus ancienne). Et cela est d'autant plus difficile que parfois, ils mettent des amendements identiques alors qu'une seule lettre peux faire changer drastiquement la réponse.

J'ai par exemple assisté à une erreur d'un des utilisateurs qui, en travaillant, avait signalé comme étant identiques des amendements qui portaient sur une taxe. Sauf que, au sein du texte, l'un proposait une taxe sur les **produits matériels** et un autre sur… les **produits immatériels.**

Pour deux lettres, le sens change du tout au tout et la réponse n'est possiblement plus la même !

Or, en informatique, il est extrêmement courant pour les développeurs d'utiliser un comparateur de texte pour visualiser les changements récents dans ce qu'ils ont ajouté au code. Visiblement, un outil de ce genre aiderait grandement les utilisateurs qui là passent un temps important à relier des amendements et à vérifier que leur identicité est une réalité… ou non !


## Perdu entre les lectures

C'est le grand drame de ces observations. Dans l'article 6 : Première observation, je vous racontais mon malaise face aux utilisateurs qui faisaient de nombreux copiers/collers à travers des amendements qu'ils avaient déjà traités dans une lecture précédente. Ils lisent l'amendement, le reconnaissent comme ayant déjà été traité, souvent dans une lecture précédente, sortent de la lecture en cours, vont dans la seconde, retrouve l'amendement, copie son contenu, retourne sur la lecture en cours, et colle le contenu dans le bon amendement.

Pour pallier à ça, nous avions réalisé un petit module "Réutiliser", afin qu'il soit possible, très rapidement, de copier coller le contenu d'un amendement en le sélectionnant dans une liste déroulante, sans sortir de l'amendement en cours.

Cependant… Zam étant initialement organisé en lectures, il n'était pas possible de réutiliser une réponse d'un amendement issu d'une autre lecture, alors que cela correspond à une grande quantité de cas…

Le bloc que nous pensions conçus spécifiquement pour aider les utilisateurs sur un de leurs problèmes principaux était en fait… inutilisé car inutilisable tant que ce fossé entre les lectures existait ! Lors des observations, cela se tradauisait par des petits papiers listant les correspondances entre les anciens et les nouveaux amendements (qui évidemment n'ont pas le même numéro), et des allées et venues entre les différentes lectures au sein de Zam, avec des utilisateurs qui se perdaient et devaient systématiquement vérifier si ils étaient dans la lecture en cours ou dans une ancienne lecture.


## Astuce de gestion des identiques

En lien avec le point précédent, nous avons pu observer une régulation des utilisateurs. Lorsqu'ils s'aperçoivent qu'ils doivent traiter plusieurs amendements identiques, ils choisissent un amendement "tête de file" qu'ils remplissent, puis se contentent d'écrire sur les suivants que les amendements X, Y et W sont similaire au Z.

Cela leur permet de limiter les allers retours et les corrections, en partant du principe que, très certainement, lorsque les amendements seront traités en commission, à l'Assemblée ou au Sénat, ils seront traités ensemble et donc ne demandent pas de réponse dissociée.


## Et sinon, pourquoi vous avez enlevé l'objet ?

Demande récurrente si il en est une : Les utilisateurs demandent encore et toujours de remettre l'objet sur l'index. Le besoin métier est vraiment difficile à extraire, mais d'après Mélodie se serait pour deux raisons :

- Il permet de connaitre le contenu de l'amendement, et donc de savoir de quel "thème d'amendement" il s'agit  (plutôt une taxe sur les bassines ou un règlement pour les skieurs ?)
- Il est détourné pour signifier le niveau de validation à un instant T, donc par exemple dans un objet on pourrait trouver : "[Validé CAB] Règlement sur les limonades"

C'est important et intéressant de remonter jusqu'aux besoins métiers, nous les notons sur la feuille de route, à prioriser vis à vis du problème remonté dans les deux points précédents : la gestion des identiques !

Notifications, reconnaissance et traitement des identiques, objet et niveau de validation, cela nous fait une belle liste pour les prochaines fonctionnalités à venir !

Dans le prochain article, je vous parlerai de la [première rencontre physique de l'équipe Zam !!](/blog/zam13-aix-y-zam/)


## Les prochains articles :

- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
