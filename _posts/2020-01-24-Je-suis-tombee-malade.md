---
layout: post
title: "Je suis tombée malade…"
subtitle: ""
date: 2019-01-24 19:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: none
permalink: blog/je-suis-tombee-malade/
---

Je suis tombée malade, et ça ne m'était jamais arrivée. Et comme je ressens le besoin, à la fois d'en garder une trace, et de la partager avec d'autres (je me dis que j'ai rencontré des types de comportements qui méritent vraiment que j'informe d'autres personnes pour le jour où, peut-être, elles seront malades aussi, ou elles auront un·e proche malade), j'écris cet article.

CW global : Je vais parler de douleurs menstruelles et de négligence médicale. Rien de trop violent pour la négligence médicale cela dit. Et je préciserai des CW avant les paragraphes concernés par un contenu en particulier.

Pour celleux qui veulent un raccourci, il y a un bilan en bas :p


## Les premiers symptômes

Depuis toujours, j'ai eu mal au ventre pendant mes règles. Et j'ai récemment appris qu'il existait une maladie pour ça, nommée l'endométriose. Mais au vu des récits qui m'en étaient faits, j'étais assez loin de subir des symptômes aussi avancés. Jusqu'à présents, j'étais KO le premier jour, et j'avais besoin de me poser sur un canapé, mais rien de plus (vous sentez que je minimise ? :D)

## Début 2019

### Douleurs menstruelles

Au début de l'année 2019, alors que je dois rejoindre les copains du japonais pour un repas le samedi midi, je commence à avoir mal au ventre. Je décide d'y aller quand même, l'appartement de mon ami n'étant qu'à 20 minutes de bus+tram. Je me reposerai sur place.

Mais sitôt montée dans le bus, je sens que ça ne va pas se passer dans la plus grande des détentes. Je rentre en mode *gestion de la douleur* : Je dégraphe mon pantalon, je m'avachis autant que possible sur le siège, et je tente de me détendre, en me rappellant que le trajet serait court.

Dès que j'arrive chez mon amie, je demande d'accéder à ses toilettes (c'est le seul endroit où il est possible d'être complètement détendue, sans élastique sur le bas du ventre, et sans risque de tacher du tissu, sans compter que les règles ont chez certaines personnes une incidence sur le système digestif… je ne vous fait pas de dessin).


(CW : Émétophobie)
Et là je sens la douleur monter, et je sens que ça va être ma fête. En plus, j'ai eu chaud le temps d'arriver chez ma pote, j'ai donc un peu transpiré, ce qui fait que maintenant, j'ai froid, donc je tremble et me crispe, ce qui augmente les douleurs et me donne chaud, ce qui me refait transpirer… J'arrive au point où j'ai la tête qui tourne, des vertiges alors que je suis assise, et j'ai fini par rendre mon petit déjeuner (oui la totale quoi). Je fais un break à un moment mais je n'ai réussi à sortir que 3 minutes avant de sentir qu'il était urgent que j'y retourne.

Le piège avec ces crises, c'est que ça secoue, mais une fois passées, je les oublie (jusqu'à la prochaine, bien sur ^^) et du coup je les minimise.

J'ai à nouveau des crises de ce genre en juillet, 2 d'affilée, qui m'obligent à ne me préoccuper que de gérer la douleur ou dormir pendant une bonne demi journée (et bien sur, annuler tout ce que j'avais prévu). L'une d'elle s'est déclenchée dans le métro, à 2 arrêts de chez moi, j'ai cru que je n'arriverai pas jusque chez moi (alors que je suis à 400m de la station x_x). Et les médicaments pour gérer la douleur (Antadys) ne marchent pas.

Je pense à ce moment là à établir la douleur sur une échelle de 10, même si je trouve l'exercice assez absurde. Si je place à 9 l'acouchement (que j'imagine comme méga douloureux), je met ma douleur sur 6 ou 7.

PS : J'ai découvert plus tard ce graphique qui me semble hyper adapté pour discuter avec un·e médecin de ses douleurs. Bon, sur ce graphique, je suis plutot à 9 par contre.

<figure>
  <img src="/img/posts/2020_je_suis_tombee_malade/echelle_douleur.png" alt="Echelle de la douleur

10 - Incapable de bouger : Alité-e, je ne peux pas bouger a
cause de la douleur. J'ai besoin que quelqu’un m‘emmene
aux urgences pour qu’on m'aide.

9 - Sévére : Je ne peux penser qu’à ma douleur. Je ne peux
quasiment pas parler ou bouger a cause de la douleur.

8 - Intense : Ma douleur est si sévère qu'il est dur de
penser a autre chose. Parler et écouter est difficile.

7 - Ingérable : J'ai mal tout le temps, cela m'empéche de
faire la plupart de mes activités.

6 - Alarmante : Je pense tout le temps a ma douleur.
J’abandonne beaucoup d'activités à cause de cela.

5 - Génante : Je pense à ma douleur la plupart du temps. Je
ne peux pas faire des activités que j'ai besoin de faire
quotidiennement a cause de cela.

4 - Modérée : Je suis conscient de ma douleur continuellement mais je peux continuer la plupart de mes
activités.

3 - Inconfortable : Ma douleur m'embête mais je peux
l'ignorer la plupart du temps.

2 - Légère : J'ai une douleur légère. Je suis conscient de
ma douleur seulement si j'y prête attention.

1 - Minime : Ma douleur se remarque a peine.

0 - Inexistante : Je n'ai pas de douleur." >
</figure>


### Fatigue

En parrallèle de ces journées au top de la douleur, je chope une grippe (juste avant SudWeb d'ailleurs, ce qui m'a obligée à ne pas y aller, super !)

Une sorte de grippe carabinée, qui me fait me coucher à 21h un soir, et qui m'empeche de me lever les jours suivants. Je décide donc d'aller voir le médecin généraliste, qui me prescrit de quoi finir ma grippe tranquille, et à qui, au passage, je parle de mes douleurs menstruelles et avec qui j'aborde tout de suite le sujet de l'endométriose. Il me prescrira une échographie vaginale pour vérifier si j'ai bien de l'endométriose.

Les symptomes de la grippe se finissent, mais je reste KO pendant encore 2 semaines, à avoir du mal à me lever le matin et tout le temps envie de faire la sieste alors que je fais de grosses nuits (nous sommes fin mai 2019). Je me dis alors que je suis peut-être surmenée, surtout que j'ai un gros mois de juin qui arrive, et que je vais devoir m'obliger à réduire la cadence. Je me souviens de m'être dit "Il faut que je me repose" puis "Mais comment est-ce qu'on se repose, en fait ?". Bref, il y avait bien un sujet :D

Il faut aussi dire que je ne m'inquiète pas car au début du printemps j'ai fait une prise de sang, demandée à mon médecin pour faire un "check up" lié au fait que je me sentais un peu fatiguée ces derniers temps, et vérifier que tout allait bien, et il m'a confirmé que tout allait bien (ça aura de l'importance plus tard).

## Été 2019

### La fatigue
Le mois de juin se passe, et je suis toujours fatiguée, mais c'est normal car j'avais plein de choses de prévues donc j'ai pas pu me reposer vraiment.

Le mois de juillet se passe, et je préviens les personnes avec qui je travaille que je veux beaucoup lever le pied. Cependant, il fait chaud, et je reste fatiguée, mais je me dis que c'est du à la chaleur.

Le mois d'août pars, je pars en Bretagne en vacances, et je n'arrive toujours pas à me lever plus tôt que 10h/11h, et à devoir revenir me coucher en début de soirée. Je me dis que c'est parce que je me déplace pas mal, et que tout celà est bien normal.

## Septembre 2019

### La fatigue

Le mois de septembre arrive, et je passe la première semaine de septembre à ne r.i.e.n. faire, jouer et regarder des séries, bref me reposer.

La 2e semaine, je suis fatiguée comme jamais. Là, plus moyen de minimiser, je me déplace pour aller voir le médecin et lui en parler.

Le médecin et son…interne (je suppose) me posent plein de questions, pour essayer de trouver la cause du problème. Est-ce que je dors bien ? (oui, des nuits de 9/10h) Et le travail, ça va bien en ce moment ? (oui oui, ça va super bien) Et pas trop stressée, sinon, dans la vie perso ? (non, tout est super).

Je vois bien que le médecin commence à chercher des raisons psychologiques à ce qu'il se passe, alors que tout va bien pour moi (vie pro, vie perso, vie familiale, tout.va.bien ). Mais comme il ne trouve rien, il m'envoie faire une prise de sang et me dis de revenir avec.

Je fais la prise de sang.

J'attends les résultats.

Je reviens avec. Les résultats ne montrent rien. "Tout est parfait", pas d'anémie notamment.

Je dois faire une autre prise de sang.

Je fais la seconde prise de sang.

J'attends les résultats.

A nouveau, les résultats ne montrent rien. Tout est parfait, pas de problème hormonal, thyroïdien ou autre. Au passage, je ne me sens pas prise super au sérieux par le médecin, qui a tendance à bien requestionner si je suis stressée en ce moment, ou avoir l'air dubitatif quand je dis que je suis "très fatiguée" et que ça dure. Mais je me dis aussi que je commence à être un peu sur les nerfs à force de ne rien trouver.

Le médecin me fait prendre rendez-vous chez une neurologue. Je prends rendez-vous, il y a un mois d'attente. Je commence à noter quels jours je suis en forme, et quels jours je suis fatiguée, car ce n'est pas continuel (même si je ne suis jamais en grande forme). Et je me dis qu'il faut que je commence à accumuler des notes si je veux être prise au sérieux, car je vois bien que à chaque fois que j'arrive et que je dis "je suis fatiguée", je ne suis pas vraiment prise au sérieux (alors que je n'ai aucune raison d'exagérer, je suis micro-entrepreneuse donc chaque jour non travaillé est un jour ou je ne peux pas me rémunérer, et je ne pouvais pas travailler les jours de grande fatigue).

### La gynéco

En parrallèle, j'ai pris rendez-vous pour la fameuse échographie vaginale qui me permettrait de savoir si j'ai bien de l'endométriose ou non.

La gynéco me demande de m'installer, et comme je me suis renseignée je sais que c'est une échographie "interne". De plus je suis proche de mon 1er jour de règles, et si je n'ai pas de grosses douleurs, je suis gênée dans tout le bas ventre. Autant vous dire que l'examen n'a pas été une partie de plaisir, non seulement parce que j'étais endolorie, mais en plus parce que la gynéco me parlait extrêmement peu, et appuyait bien là où ça faisait mal, sans me prévenir (ambiance au top).

Il faut voir que depuis le début, je lutte avec un sentiment de "Et si c'était rien ?" "Et si en fait, les médecins me trouvaient juste ridicule à me plaindre alors qu'en fait c'est normal d'avoir un peu mal de temps en temps ?"

Le seul moment où je me suis sentie connectée, c'est quand elle m'a demandé à combien je mettais la douleur, j'ai répondu à 6 ou 7, et elle m'a répondu "Ah oui… une douleur plutot vive donc". Ca m'a procuré un immense soulagement, ma douleur était validée par le corps médical et je me suis sentie un peu plus légitime à m'en plaindre (c'est assez impressionnant ce qu'on s'habitue à penser pour minimiser l'importance de la douleur)

Elle conclue en me disant "On ne voit rien, du coup vous allez faire un IRM vu le niveau de douleur dont vous me parler, on reprend rendez-vous ensuite" Par contre, bonne nouvelles, elle m'a dit que j'avais des ovaires très fertiles, et que c'était bon signe pour le futur (oui, je m'en fous complètement, je veux savoir pourquoi j'ai mal, mais bon…on ne crache pas sur une bonne nouvelle je suppose)

Je prends rendez-vous pour l'IRM, il y a 2 mois d'attente.


### L'attente

Je n'ai donc plus qu'à attendre les rendez-vous chez la neurologue et l'IRM pelvien pour trouver une cause à mes symptômes. En attendant, la fatigue prend de l'ampleur, et je me retrouve à vivre des journées où je n'ai que 3h d'énergie dans la journée.

Je me lève à 11h, et suis au bout du rouleau à 14h, comme si on m'avait assommée et que l'effet durait pendant plusieurs heures, jusqu'au coucher. Impossible de faire quoi que se soit d'actif, la moindre concentration m'est impossible. Je passe mes après-midi à regarder des séries, car si je dors j'ai peur de décaler mon rythme de sommeil. Le temps est long, extrêmement long.

En parrallèle, je me renseigne sur l'endométriose. En me rendant sur un site dédié à l'endométriose, [sur la page des symptômes](https://www.endofrance.org/la-maladie-endometriose/symptomes-endometriose/) je découvre que sur la liste des symptômes, j'en coche en tout 6 sur les 7 (la 7e étant "infertilité" donc difficile à savoir)

Mais surtout, je découvre que j'ai des symptômes que je n'avais pas identifiés comme tels ! :
- la Dyspareunie, douleur ressentie lors des rapports sexuels,
- des Troubles urinaires,
  - Des douleurs pelviennes et urinaires (ça pendant les règles)
  - Des dysuries (difficultés pour vider la vessie) (ça oui)
  - Une pollakiurie (envie fréquente d’uriner) (ça oui)
  - La présence parfois de sang dans les urines (ça non)

En effet, au milieu de mes vacances j'ai, de façon très étrange, commencé à aller aux toilettes… puis à y retourner sitôt sortie, comme si je n'arrivais pas du tout à vider ma vessie, et ça parfois 3 à 4 fois d'affilée ! Et le reste du temps, j'ai régulièrement envie d'aller aux toilettes, peut-être la moitié du temps, comme si il fallait que j'y aille toutes les demi-heures pour être tranquille. Mais je n'avais pas réalisé que ça pouvait être un symptôme !

Les autres symptômes étant notamment fatigue chronique, douleurs digestives et douleurs pelviennes, je cochais un sacré bingo. J'étais donc sure de moi, l'endométriose expliquait tout, même la fatigue.

Il ne me restait donc plus qu'à attendre l'IRM pour montrer tout ça !

En attendant, je passe mes journées à …ne rien pouvoir faire, à part me lever pour aller aux toilettes. Toutes les tâches quotidiennes s'accumulent, mais impossible de faire le ménage, les courses, les lessives… Quand, certains jours de façon miraculeuse, j'ai plus d'énergie que d'autres (d'aprs mes notes ça arrivera 1/3 du temps), je me précipite pour faire mes tâches et me tenir un peu à jour professionnellement, ce qui me pompe le peu d'énergie que j'avais. Ce n'est pas facile à vivre, heureusement que le chat me tiens compagnie, ça fait au moins une heureuse que je ne puisse que végéter à longueur de journée :D

## Octobre 2019

### La neurologue

Viens enfin le jour de rencontre de la neurologue. J'ai peu d'espoir, persuadée qu'on va surtout me renvoyer faire des examens supplémentaires. Et ça ne rate pas, les questions ne mnent à rien, je repars donc avec une IRM, cérébrale cette fois, à réaliser, ainsi qu'une nouvelle prise de sang. Elle dira tout de même au passage "même si sans doute, ça ne montrera rien" ce qui m'enfonce un peu plus.

Il faut dire qu'au bout d'un moment, entre les médecins qui ne me prennent pas au sérieux, moi qui suis fatiguée et donc déprimée, il n'y a pas besoin de grand chose pour que je remette en cause ce que je ressens, surtout qu'il n'y a pas de douleur à signaler. Mais la fatigue est tellement intense que finalement, elle me permet de tenir bon et de continuer le parcours médical.

### L'IRM pelvien

Encore un peu d'attente, et vient le temps de l'IRM pelvien (pour l'endométriose). Je me présente, attends qu'on vienne me chercher. Lorsqu'on donne mon nom, je rejoins la soignante qui m'explique qu'elle va m'accompagner. Elle me place dans une cabine et me dit de me changer, elle m'apporte une blouse et un sous-vêtement jetable, et me demande de me changer. Puis elle referme la porte et je me retrouve dans une pièce de 1m2, qui rendrait claustrophobique n'importe qui… J'essaie de me détendre, je me change, je fais bien attention à ne rien garder de métallique sur moi et… je ne sais pas quoi faire, on ne m'a pas donné d'indication. J'ouvre la porte, mais il n'y a personne dans la salle des soignantes… Je ne sais pas si je suis sensée la rejoindre, je suis à moitié nue car la blouse ne couvre pratiquement rien, et je me sens enfermée dans une pièce minuscule.

La soignante finit par venir me chercher, et me mène jusqu'à l'IRM. En quelques mots, elle m'explique qu'il faut que je m'allonge, que je ne bouge pas, et que j'ai une sonnette si ça ne va pas. Au moment où je vais pour m'allonger, sa collègue qui avait l'air jusque là trop occupée pour que je puisse lui dire bonjour, me place sans préavis un casque sur les oreilles. Déjà mal à l'aise, je me sens carrément agressée, même si je me doute bien qu'elles font ça toute la journée. Ma soignante me signale que ma tête va rester en dehors de la machine, et me dis que l'IRM dure 10 minutes, et…c'est parti.

Eh bien autant vous dire que ma tête à beau être à l'extérieur, la machine était vraiment très proche. De plus, les bruits étranges et très bruyants de la machine (genre alarme incendie), l'injonction à ne pas bouger alors que je n'ai pas eu le temps de m'installer confortablement, et le fait de ne pas savoir où se trouvaient les soignantes (est-ce que je peux leur parler ? est-ce que je peux cligner des yeux ou est-ce que c'est bouger ?), je ne suis pas au top de ma forme. Je me concentre pour respirer très profondément, le temps que ça passe.

Une fois fini, ma soignante n'est pas là. Sa collègue, qui m'a placé le casque sur les oreilles, me signale que je peux y aller. Je n'ai aucune idée de si tout est fini ou pas, mais puisqu'on me montre la cabine où se trouvent mes vêtements, je me rhabille rapido et je sors de la salle d'examen.

Ensuite, plus qu'à attendre les résultats. 2h après, la médecin qui analyse les IRM m'appelle pour me parler de ce qu'elle y voit. Elle me décrit un "épaississement des ligaments utéro-sacrés" qui peuvent expliquer des douleurs à l'arrière du dos (raté, j'ai mal au ventre), et des douleurs pendant les pénétrations (1 symptome sur 2 mais pas ce pour quoi je consulte). Par contre, pas de trace d'endométriose.

C'est la déception, mais j'ai rendez-vous pour parler des résultats avec la gynécologue dans la semaine qui suit, je me dis qu'on va pouvoir explorer d'autres pistes.

### La gynécologue, le retour


J'arrive chez la gynécologue, et lui refait un point : les douleurs qui m'amènent à consulter, l'échographie qui ne montre rien, et lui répète ce que m'a dit la médecin de l'IRM.

Je lui dit que je ne comprends pas, alors que je coche tous les symptômes de l'endométriose, ça pourrait ne pas être ça. Elle me dit alors "que si on ne voit rien, c'est que c'est pas ça, et qu'il ne faut pas que je me focalise dessus". Surprise, j'insiste en parlant des autres symptomes que j'ai identifiés, les troubles urinaires et les douleurs pendants les rapports. Mal m'en a pris, car elle m'a alors répondu que pour les troubles urinaires, je pourrai aller voi un urologue qui pourrait sans doute me mettre une caméra dans la vessie (ambiance). Elle enchaine ensuite sur la pilule, que j'ai arrêtée de prendre il y a plusieurs années car ce n'était pas un mode de contraception qui me convenait. Elle me demande si je compte la reprendre, ce à quoi je lui réponds que non. Elle me dit qu'il n'y a rien de plus à faire alors.

Elle me demande de me positionner pour m'ausculter ensuite, et je m'exécute comme un robot, complètement soumise à ce qu'elle me demande sans oser faire de remarque tellement je me sens mal. Je pleure sur la table d'auscultation alors qu'elle fait ses prélèvements, mais je fais en sorte de ne pas le faire voir.

On conclut l'entretien par un conseil : "Pour les douleurs pendants les rapports, il faut vous détendre".

Mortifiée, je pars de chez elle en larmes.

### Le médecin généraliste, le retour


Les jours passent, la fatigue ne s'améliore pas, je pars faire la prise de sang recommandée par la neurologue. Une semaine après, j'obtiens les résultats, et je file chez le généraliste pour lui parler de l'IRM et lui demander si cette nouvelle prise de sang donne de nouvelles informations.

Pour ma part, je note une carence en vitamine B12, pas très étonnante puisque je suis végétarienne *et* que j'ai de grosses difficultés à me nourrir depuis toujours, même si j'ai fait d'énorme progrès cette dernière année, ce dont je suis super fière :)

J'arrive donc chez le médecin, et lui tend le résultat de la prise de sang. Il la lit et me signale que je suis très carencée en vitamine B9 et D. "Vous mangez des fruits et des légumes ?" "Euh…oui" "Tous les jours ?" "Euh…oui". Je sens l'accusation dans sa voix, comme je suis plutot grosse je pense que c'est inconcevable que je puisse manger un peu sainement ?! En tout cas, il n'a pas l'air convaincu par ma réponse, et me dit qu'il "faudra que j'aille voir une diététicienne".


Je reviens sur la carence : "Est-ce que ça peut expliquer la fatigue ?" "Bien sur" me répond-il. Chouette, une vraie piste !! Et la B12 ? Ca ne semble pas un sujet pour lui. Il me prescrit donc une grosse cure de B9 (20 fois la dose quotidienne pendant le 1er mois !) et de vitamine D.



Puis, j'aborde le sujet de l'endométriose. La réaction est à peu près la même que celle de la gynéco : Vous voulez prendre la pilule ? Non, ben y a rien à faire, de toute façon l'examen montre rien. Fin du sujet. J'insiste, en parlant des problèmes urinaires que j'ai, car passer ma vie à faire des allers-retours aux toilettes, on a connu plus fun. En réaction à ma phrase "je n'arrive pas à vider ma vessie, il me demande "comment est-ce que vous le savez" (en effet, je ne le *sais* pas, c'est la sensation que j'ai), me prescrit une échographie de la vessie puis me dit "Mais…est-ce que vous vous tenez droite quand vous êtes au toilettes ? parce que parfois, la posture…"

Alors là, je me sens bien prise pour une conne, et ça commence à bien faire. Je précise que ça n'est pas le problème, et je pars chercher mes vitamines, mais je suis furieuse de m'être fait prendre pour une stupide pour la 2de fois par un soignant.

## Le répit

Et les vitamines fonctionnent ! Le mois de novembre est une vraie respiration, je peux enfin prévoir de faire des choses, me déplacer hors de ma ville, travailler, avancer mon mémoire de master. Je réalise ce que ça fait de retrouver ses capacités, et à quel point le fait de se retrouver privée subitement m'a placée dans un état où je me sentais complètement aliénée, à ne pas savoir ce qui se passait, comment aller mieux, et à n'être prise en compte par personne du corps médical.

Je passais toujours mon temps aux toilettes mais ça n'avait plus d'importance :)

Cependant, à la moitié du mois de décembre, je réalise que je recommence à être fatiguée. Ca n'a plus la même intensité qu'au début, mais je suis sur la même pente. Coincidence (ou pas), j'ai changé de traitement pour la vitamine B9, passant de 20 fois la dose quotidienne à seulement 2 fois la dose.

Comme j'avais une prise de sang de contrôle, je me presse de la réaliser pour savoir si, peut-être, j'ai un souci d'absorption des vitamines qu'on m'a donnée. Au passage, je prend rendez-vous chez une sage-femme qui fait un suivi gynécologique **qu'on m'a recommandée** pour avoir un deuxième avis.

La prise de sang indique que les carences en vitamine B9 et D ne sont plus d'actualité, bonne nouvelle !

Par contre, pas question de recommencer à être fatiguée comme avant. Cette fois, je me rend chez un généraliste **qu'on ma recommandée**, avec tous mes papiers, bien décidée à ne pas me laisser faire.

## Début 2020

### Échographie de la vessie

Je vais vous la faire courte :
- Elle ne montre rien
- Le médecin me demande ce que disent les relevés biologiques
- Je n'en ai pas fait
- Il trouve ça surprenant "C'est la 1e chose à faire normalement…"


### La sage-femme

Je vais à mon rendez-vous dans la plus choupi des salles d'attente (remplie d'affiches féministes et queer, c'était le bonheur). Je rentre et explique mon problème à la sage-femme. Une fois mon histoire terminée, elle me dit (et c'est la 1e soignante à me le formuler comme cela) : "Je ne vois pas ce que je peux faire de plus". C'est un soulagement immense d'entendre une soignante dire qu'elle ne voit pas quoi faire de plus, plutot que de dire "ben on voit rien, c'est que vous n'avez rien".

Cependant, je poursuis la discussion en disant "Mais du coup, il n'y a rien à faire ? J'ai des symptomes mais ca ne correspond à rien ?"
" - Ah si, si, vu ce que vous me décrivez, je ne vois pas comment ça pourrait ne pas être de l'endométriose"
"- Mais…les examens ne montrent rien…"
"- Ah mais les examens ne montrent pas toujours quelque chose !"

Là c'est l'épiphanie en moi "Ouiiii je le savais !", et en même temps la grosse colère "Non mais et les autres soignants qui insistent en disant que *je n'avais rien puisqu'on ne voyait rien,* quels blaireaux !"

Elle m'explique ensuite que ce que préconise la Haute autorité de santé pour l'endométriose, c'est 1. de traiter avec une contraception hormonale, et, si les symptômes persistent, de 2. faire les examens.

C'est sans doute ce qu'entendaient les précédents médecins par "et vous voulez pas prendre la pilule ?"

Elle finit ensuite par me donner l'éventail de choix que j'ai, de "ne rien faire" à la liste des contraceptions hormonales, en me parlant aussi d'osthéopathie gynécologique.

Par contre, quand je parle de mes soucis urinaires, elle m'assure que, comme ils sont constants et non pas cycliques / liés aux règles, ils ne sont pas liés à l'endométriose.


Bref, je sors de là avec le souci ~~endométriose~~ de réglé. Plus que 2 !



### Le nouveau généraliste

Je me rend chez le nouveau généraliste, avec tous mes papiers, relevés, échographie en main.

Et tout de suite, je vois qu'il prend le problème très au sérieux o.o

(Oui ça m'a surprise !)

Tout de suite il me precrit de la b12, pour compenser la carence indiquée dans la prise de sang (j'avais investigué et su qu'une grande dose de B9 pouvait cacher une carence en B12, ceci pourrait expliquer cela), et suspecte un souci de fer (il m'indique que ça peut se voir au niveau des ongles, qui auraient tendance à se fendre, et c'est complètement mon cas)

Pour la vessie, il me prescrit le fameux relevé biologique et des antibiotiques en me disant "on va pas vous laisser comme ça tout le week-end". Alors que ça faisait **4 mois** que ça durait !!!!

Je sors de chez lui et j'ai des ailes, rien que parce que j'ai été reconnue comme était réellement **malade** avec des symptômes qui ont été pris au **sérieux**.

Je file à la pharmacie et commence le traitement. Les vitamines ont l'air de fonctionner (ça fait 15 jours que je vis bien avec, donc on va dire que oui, ça marche !)

Cependant, pour les problèmes de vessie, les 1ers antibiotiques et l'analyse n'a rien donné. Je suis retournée le voir, et je prends des cachets qui me soulagent lorsque je les prends (je dois les prendre outes les 24h, et ils fonctionnent…20h environ, donc c'est pas parfait mais ça aide déjà **tellement**)

Donc il ne reste plus que ces histoires d'envie d'aller aux toilettes à régler, a priori :)

## Je récapitule

### L'endométriose

L'endométriose, ça ne se voit pas forcément, ni à l'IRM, ni à l'échographie pelvienne. Il n'y a a priori pas beaucoup de possibilités, soit contraception hormonale (stérilet hromonal, injection, implant, pilule…), soit osthéopathie gynéco (que je n'ai pas encore testé)


### La fatigue chronique

J'étais apparemment carencée en vitamine D, B9, B12 et en fer. C'était sans doute du au fait que
- je n'ai pas mangé équilibré pendant des années, et donc que j'ai puisé dans mon capital
- que je suis devenue végétarienne, ce qui n'aide pas à compenser "par erreur" mes soucis d'alimentation non équilibrée


### Les soignant·es

Les soignant·es qui ne t'écoutent pas et ne te prennent pas au sérieux, c'est catastrophique pour le processus de guérison. On perd du temps à attendre un rendez-vous avec un praticien qui nous prend pour une conne ou une simulatrice, et qui ne prends pas nos symptômes au sérieux pour des raisons que j'ignore toujours. Ca consomme énormément d'énergie, et ça provoque beaucoup de remise en cause personnelle alors que ça ne devrait pas, si on va mal on est légitime à se faire soigner.

Je ne remercierai jamais assez les ressources que met à disposition [Vivre Avec](https://www.youtube.com/channel/UCH3Hp3WAm0iGQBi_csusoUg/videos) dans lesquelles il dit et répète des choses qui semblent simples, telles que "Tu as le droit de changer de généraliste" mais qui sont, dans la réalité, extrêmement difficile à mettre en place. Lorsqu'on a commencé un parcours de soinavec un soignant, on a envie qu'il nous suive car c'est lui qui est sensé être le plus au courant de ce qu'il se passe, de l'évolution des symptômes, etc…

### Le moral

Le plus dur, ça a été le moral. Malgré l'environnement personnel et professionnel fabuleux que j'ai, il fallait gérer l'attente entre les rendez-vous, le fait de ne pas savoir ce qu'il se passait, si il y allait y avoir une amélioration ou non, et surtout le fait d'être prise au sérieux par les médecins ou non, et donc d'avoir une reconnaissance du corps médical sur la validité de ses symptômes m'a, personnellement, fait me sentir complètement légitime en quelques instants après des mois de remise en question. C'est un équilibre fragile dont, visiblement, la plupart des soignants que j'ai croisée n'ont absolument pas conscience, posté dans une posture de sachant, et remettant en cause mon vécu et mon quotidien si je ne tombais pas dans le bac de leurs connaissances (ce qui était complètement destructeur au passage)


Merci à toutes celles et ceux qui m'ont soutenue dans ce temps très long et pas facile.

Et bravo à moi d'avoir tenu le coup :p
