---
layout: post
title: "Recommencer à écrire"
subtitle: ""
date: 2022-03-22 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/recommencer-a-ecrire/
---

Avec le billet que je viens d'écrire [sur Mélenchon](https://www.maiwann.net/blog/pourquoi-melenchon/), je me suis relancée dans un truc tout bête et pourtant fort satisfaisant pour l'égo : J'ouvre mon éditeur de texte, j'écris, je publie (parfois même sans me relire) et des gens me lisent.

Je réalise avec stupeur que je n'ai pas écris en 2021, et je pense que c'est vraiment dommage parce que j'ai fait plein de choses chouettes, et que j'ai beaucoup de choses à dire.
Aussi je vais essayer de prendre le rythme d'écrire le plus souvent possible, peut-être quotidiennement sur le [modèle de Marien](https://marienfressinaud.fr/billets-courts.html).

Écrire court, écrire sans relire, mais écrire.

Et je met pêle-mêle les sujets que je pourrais aborder :
- Salariat sans subordination
- L'Échappée Belle
- Framasoft
- La situation politique
- Ma relation au covid
- Ma découverte du handicap
- Vivre dans une petite ville
- Apprendre à faire du vélo
- Ma thérapie et ses surprises
- Lecture des mémoires de mon grand-père
- Relation à la nourriture et à la cuisine
- Relation à la lecture
- Relation à la parentalité
- Être grosse
- Enjeux numériques et oppressions systémiques

Je me demande si ça arrive à tout le monde d'écrire un billet prévenant qu'on va écrire des billets ? Bizarre non vous en pensez quoi ?
