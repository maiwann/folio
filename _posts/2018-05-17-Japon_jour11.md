---
layout: post
title: "Japon Jour 11"
subtitle: ""
date: 2018-05-17 16:00:00 +0200
vignette: null
intro: "Shinkansen & Kyoto"
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/japon_jour11/
---

# Jour 11 : Shinkansen & Kyoto

Ce qui est pratique quand tu dois te lever ~~très~~ tôt au Japon c'est que tu te lèves quand même avec le soleil :D Nous voilà levé·e·s à 6h du matin pour attraper un Shinkansen (le fameux train japonais hyper rapide) et descendre jusqu'à Kyoto, l'ancienne capitale japonaise. J'ai beaucoup d'espoir de voir un coté plus traditionnel et moins urbanisé que le grand Tokyo !

Assis·e·s dans le train nous essayons de nous tenir éveillé·e·s pour voir le Mont Fuji… que nous n'apercevrons même pas (ou ptet bien que si mais on est pas bien sûr·e·s !)

Arrivée à Kyoto avec nos sacs, nous retrouvons une copine du cours de japonais (note pour moi-même: ne pas donner rendez-vous à une enseigne de conbini (les supermarchés ouverts 24/24 au Japon) car il y en avait 4 autour de la gare !!) et décidons de nous diriger vers un temple bouddhiste tout proche. Il faut dire que jusqu'à présent je n'avais fait que des sanctuaires shintos, j'avais bien envie de voir à quoi ressemblent les templs bouddhistes ! Eh bien sans surprise c'était assez majestueux… Le temple était en plusieurs batiments, avec des toits ornés de dorures et une immense cour.

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_1.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_2.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_3.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_4.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_5.jpg" alt="">
</figure>

Dans un temple bouddhiste, il est parfois possible de rentrer pour voir l'intérieur ! Nous enlevons nos chaussures, gravissons les marches et entrons dans le temple. Une cérémonie est en cours, il faut s'asseoir en seiza (à genoux) pour l'observer silencieusement. Nous ne comprenons pas ce qu'il s'y dit mais ça ressemble énormément à une messe !! Le ton de voix et la façon de prononcer les phrases peut-être que c'est une similitude dans toutes les religions monothéistes ? En tout cas lorsque le sang ne circule plus dans mes jambes il est temps d'y aller. Nous trouvons des tampons à l'extérieur, visionnons un mini film sur… l'histoire d'un moine qui a vu Bouddha ? (je suppose…) et il est temps de rejoindre notre logement pour la soirée, que nous avons choisi au bord d'une rivière/fleuve !

A l'arrivée, je réalise que le code du WiFi est inscrit sur une étiquette collée sur… la boite aux lettres (accessible depuis l'extérieur bien sur). De quoi faire criser pas mal d'informaticiens que je connais :p !

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_6.jpg" alt="">
</figure>

En repartant, nous mangeons des ramens dans un restaurant d'une chaine que nous avons déjà essayé (où l'interface comporte un bouton "mettre en anglais" ce qui ne change… rien sur l'interface !), puis nous allons vers la rue Gion où se trouve pleiiiin de boutiques que j'espère plus traditionnelles histoire de rapporter des souvenirs… jusqu'au moment où je réalise que sur le trottoir qui longe la route, se trouve des lampadaires avec des… pokemon !!!! Je suppose qu'ils nous emmènent jusqu'à un centre Pokémon et c'est bien le cas :D Nous partons donc en quête de ce centre pokémon. Je remercie au passage Marie-Astrid qui a, parce que je ne me sentais pas très bien, pris en photo chaque pokémon devant lequel nous sommes passé·e·s. C'est pourquoi, pour lui rendre hommage, je vais vous partager **chacune des photos qu'elle a prise** sur le chemin menant au centre pokémon.

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_7.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_8.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_9.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_10.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_11.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_12.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_13.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_14.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_15.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_16.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_17.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_18.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_19.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_20.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_21.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_22.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_23.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_24.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_25.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_26.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_27.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_28.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_29.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_30.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_31.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_32.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_33.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_34.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_35.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_36.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_37.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_38.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_39.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_40.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_41.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_42.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_43.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_44.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_45.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_46.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_47.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_48.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_49.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_50.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_51.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_52.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_53.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_54.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_55.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_56.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_57.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_58.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_59.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_60.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_61.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_62.jpg" alt="">
</figure>

Oui il y en a plein hein :D
Bon le centre Pokémon était moins impressionnant que celui de Tokyo, donc j'ai évité de perdre un nouveau bras à l'intérieur. Par contre j'ai pu apprendre à devenir une meilleure dresseuse de pokémon en jouant (et même que j'ai gagné !!)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_67.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_68.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_69.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_70.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_71.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_72.jpg" alt="">
</figure>

Au passage j'ai aperçu de mignonnes petites figurines Disney géométriques pour des meubles… Est-ce que ça m'a donné des idées ? (noooooooon)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_63.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_65.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_66.jpg" alt="">
</figure>

Sinon en sortant j'ai vu de jolies petites images sur les trottoirs (il y en a des trucs sur ces trottoirs japonais quand même :p)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_73.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_74.jpg" alt="">
</figure>

Nous partons pour la rue Gion, mais il y avait beaucoup de touristes et ce n'était pas vraiment l'aspect traditionnel auquel je pensais… pas grave, nous continuons de nous promener et comme d'habitude, il y a plein de verdure, des sanctuaires shintos à chaque coin de rue (ou presque). J'adore l'aspect des maisons japonaises, et leur empilement de pots de fleurs pour rajouter un maximum de plantes partout. En plus, à Kyoto, beaucoup de magasins proposent de louer des kimonos à la journée, ce qui permet de voir beaucoup de couples habillés traditionnellement et c'est assez sympa à voir (j'ai un peu l'impression de me rapprocher des mangas :3)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_75.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_76.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_77.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_78.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_79.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_80.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_81.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_82.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_83.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_84.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_85.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_86.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_87.jpg" alt="">
</figure>

On tombe même sur le temple de la rupture. D'après Mélanie, il faut passer dans le trou pour voir son voeu de séparation (d'une personne ou de la malchance par exemple) exaucé. Marrant de s'en remettre aux kami (esprits) pour ça :p

<figure>
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_88.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_89.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_90.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_91.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_92.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_93.jpg" alt="">
</figure>

Je vous remet une petite couche de photos de Kyoto, la fin de journée puis des sanctuaires pendant la nuit (ou nous avons prié pour une amie qui désire un 2e enfant à un dieu qui était dédié aux… études ! On repassera pour la cohérence)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_94.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_95.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_96.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_97.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_98.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_99.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_100.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_101.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_102.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_103.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_104.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_105.jpg" alt="">
</figure>

Pour finir la soirée, on se retrouve dans un petit restaurant. Au centre de notre table, une plaque chauffante, et nous commandons des okonomiyaki \o/ (C'est trop bon les okonomiyaki, et le nom viens de yaki = grillé et de okono = un peu de tout :D). C'était très bon (c'est toujours bon les okonomiyaki de toute façon), et il est l'heure d'aller dormir au bord de la rivière.


<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_106.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_107.jpg" alt="">

</figure>

Je suis un peu frustrée car je n'ai pas pu suivre certain·e·s de mes ami·e·s à un onsen (des bains publics où l'on est tou·te·s nu·e·s :D ) dans la montagne en plein air (car les bains ne sont pas ouverts aux personnes ayant leurs règles :< ) mais je me promets que ça sera pour une prochaine fois (avant la fin du séjour en tout cas !!!)

Pour conclure cette journée, je vous laisse avec cette photo vue à plusieurs endroits différents, et je vous laisse y réfléchir jusqu'à demain :D

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_11/japon_j11_108.jpg" alt="">
</figure>
