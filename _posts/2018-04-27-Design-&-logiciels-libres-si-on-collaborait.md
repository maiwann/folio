---
layout: post
title: "Designers & Logiciels libres : et si on collaborait ?"
subtitle: ""
date: 2018-04-24 19:09:00 +0200
vignette: null
intro: "Le monde du libre est rempli de communautés de développeurs·euses passionnés. Mais où sont les designers ? Incompréhensions entre les différents corps de métier, méconnaissance de ce qu'est un logiciel libre, légende du designer-artiste qui n'en fait qu'à sa tête, possibilités de contribuer mal adaptées pour des designers, les origines de ce désamour sont nombreuses. Et si on passait à l'étape suivante, celle où devs et designers du libre se rejoignent pour créer des logiciels ensemble, histoire d'en améliorer l'expérience utilisateur ?"
nom_blog: le blog
which_blog: blog_pro
permalink: blog/designers-&-logiciels-libres-si-on-collaborait/
---

> Cet article fait suite à une conférence que j'ai donnée à MiXiT sur le thème "Designers & Logiciels libres : et si on collaborait ?" (la vidéo sera bientôt disponible). Histoire d'approfondir certains points et partager plus facilement l'ensemble des ressources, voici un article complémentaire =)

Je profite de l'intro pour partager le résumé de la conférence :
> Le monde du libre est rempli de communautés de développeurs·euses passionnés. Mais où sont les designers ? Incompréhensions entre les différents corps de métier, méconnaissance de ce qu'est un logiciel libre, légende du designer-artiste qui n'en fait qu'à sa tête, possibilités de contribuer mal adaptées pour des designers, les origines de ce désamour sont nombreuses. Et si on passait à l'étape suivante, celle où devs et designers du libre se rejoignent pour créer des logiciels ensemble, histoire d'en améliorer l'expérience utilisateur ?

Mais pour démarrer sur des bases communes, passons par quelques définitions :

# C'est quoi un logiciel libre ?

D'après Wikipédia :
> Un logiciel libre est un logiciel dont l'utilisation, l'étude, la modification et la duplication par autrui en vue de sa diffusion sont permises, techniquement et légalement, ceci afin de garantir certaines libertés induites, dont le contrôle du programme par l'utilisateur et la possibilité de partage entre individus.


On parle aussi des 4 libertés du logiciel libre :
- Utiliser le programme
- Étudier son fonctionnement et le modifier
- Redistribuer
- Distribuer des versions modifiées

Si vous voulez en savoir plus, je vous recommande chaudement la présentation de [Delphine](https://framapiaf.org/@DelphineM) "L'intérêt du libre, expliqué le dimanche midi en famille" au Capitole du libre ([Regarder la vidéo](https://toulibre.org/pub/2017-11-18-capitole-du-libre/videos/education/malassingne-interet-du-libre-midi-en-famille.mp4) ou [voir ses slides](http://articles.nissone.com/2017/11/interet-libre-explique-dimanche-midi-famille/))

## Et qui fait les logiciels libres ?

Les logiciels libres naissent souvent d'un besoin ou d'une envie d'un développeur·euse, qui commence par développer un bout de logiciel pour y répondre. Lorsqu'il prend un peu d'ampleur et commence à se diffuser, d'autres développeurs·euses peuvent y contribuer et, en général, s'auto-organisent pour le développer de façon communautaire.

Sauf que… et bien sauf que si le logiciel n'est développé que par des développeurs·euses et pour des développeurs·euses, lorsqu'il s'ouvre à d'autres profils d'utilisateurs·ices, son utilisation laisse assez… dubitatif·ve.

> Sur ce point, n'hésitez pas à aller voir la super conférence [Percevoir et communiquer : réalité et fictions personnelles](https://mixitconf.org/2018/percevoir-et-communiquer-realite-et-fictions-personnelles) de Yves Rosseti lorsque la vidéo sera sortie !

Pourquoi ça ? Eh bien parce que les développeurs·euses ont leurs propres priorités (nouvelles fonctionnalités, résoudre les bugs, garder du temps libre) et leurs propres biais (liés au fait qu'ils s'occupent du coté technique du logiciel et qu'ils l'ont initialement développé pour leurs propres besoins, et pour se faire plaisir !). Pourtant, une fois que le logiciel est ouvert à des profils d'utilisateurs·ices plus variés·es, il faut faire l'effort de penser au design du logiciel afin de leur proposer une expérience positive (et ne pas contribuer à la mauvaise réputation qu'ont les logiciels libres auprès du grand public), même si cela signifie remettre en cause pas mal de choses ce qui peut être sacrément inconfortable (mais courage, c'est pour le bien des utilisateurs·ices et la promotion du logiciel libre !).

# Mais en fait, c'est quoi le design ?

Le design n'a pas de définition consensuelle (ça serait trop simple sinon), et chaque designer a sa propre définition de son métier, dépendante de ses compétences, de sa vision, de son éthique… Par exemple pour le design d'interfaces, on peut considérer qu'il est sous-composé par :
- la conception
- le graphisme
- la stratégie
- l'ergonomie
- l'UX · Expérience Utilisateur
- l'identité graphique
- l'illustration…

Mais cette liste n'est pas exhaustive pour autant.

On peut néanmoins s'accorder sur le fait que le design est une **vision globale** d'un produit, service, logiciel… et est trop souvent ramené au seul aspect esthétique d'un produit.

## Utile, utilisable, utilisé

Pour le dire autrement, un logiciel bien designé est un logiciel utile, utilisable & utilisé :
- Utile car il apporte de la valeur aux utilisateurs
- Utilisable car il peut être utilisé sans provoquer de frustration
- Utilisé car les utilisateurs ont envie de… l'utiliser ;)


L'exemple que je trouve parfait pour illustrer cette explication du design c'est VLC.
J'utilise VLC depuis que j'ai 15 ans. Pas parce qu'il est libre. Mais parce qu'il me permet de lancer chaque vidéo que je veux regarder sans aucune frustration. Ca n'a l'air de rien et pourtant tous les autres ont échoué à le faire aussi bien que lui.

Pourtant il n'a pas une interface très esthétique, mais ce n'est pas le plus important. Si votre logiciel est beau mais inutilisable, personne ne l'utilisera. S'il est utilisable mais moche, il fera mauvaise impression ce qui lui fera perdre des utilisateurs, mais il conservera ceux qui auront passé le pas.

Evidemment le mieux serait qu'il soit esthétique et utilisable, mais ce qu'il faut retenir c'est que le plus important, quoi qu'il arrive, c'est **l'utilisabilité.**


# Où sont les designers ?

Mais alors, si le design c'est si important, où sont les designers du libre ? Ceux qui se plaignent de la mauvaise utilisabilité sans pour autant contribuer ?

Si nous nous plaignons et que nous ne contribuons pas, c'est parce que, mine de rien, les designers rencontrent de nombreux freins à leur intégration au monde du libre… Faisons un petit tour des différentes frustrations auxquelles nous faisons face, pour mieux comprendre ce qui nous rebute tant à contribuer. Ensuite je vous donnerai des pistes d'actions à mettre en place pour contourner ces problématiques !

## Méconnaissance de ce qu'est un logiciel libre

Lors de nos études, personne ne nous parle spécifiquement de l'existence des logiciels libres. On entend parler des droits d'auteurs et du risque de se faire voler nos créations, mais pas d'une alternative possible qui seraient de contribuer aux communs (d'ailleurs les communs on ne nous en touche pas non plus un mot).

De plus, alors que nous cherchons à nous entraîner, sur notre temps libre ou pour des projets de fin d'année, nous nous plaignons de ne connaître aucun développeur avec qui co-créer des sites ou logiciels. Quand on sait à quel point le monde du libre a besoin de designers, c'est risible non ?

## Où trouver l'info **facilement** ?

Lorsqu'on a envie de contribuer d'une façon ou d'une autre, il faut trouver où contribuer. Or Internet est grand, très grand, et la multitude de logiciels libres ne nous aident pas, d'autant plus qu'il n'y a pratiquement pas de "liste des besoins" ou de page "comment contribuer" nous expliquant par où commencer. On se sent alors complètement perdus·es pour trouver l'information recherchée.

## Besoins listés de façon inadaptée

Une liste des besoins de contribution, c'est une bonne idée. Mais une fois la liste parcourue, je me suis déjà retrouvée à consulter des demandes étiquetées "design" et correspondant à… de l'illustration pour des stickers (qui est donc un sous-domaine très spécifique et ne fait pas vraiment appel à une vision globale liée au design). Arriver jusque là pour se rendre compte que notre domaine de compétence est incompris ou mélangé à d'autres, ce n'est pas terrible.

## PR welcome

Pour trouver où contribuer, un petit tweet ou pouet (#JoinMastodon) pour proposer son aide et on obtient plusieurs retours de personnes qui ont besoin d'un·e designer !
Par contre, la réponse ressemble souvent à "Hey sur monprojet.com on a des besoins en UX, je te laisse faire un tour et n'hésites pas à faire des issues".

Sauf que, premièrement, parler d'issues ou de PR à un designer, c'est souvent lui parler chinois (car même si on sait parfois  comment fonctionne git, ce n'est pas pour ça qu'on sait l'utiliser), et deuxièmement, si le design est une réflexion globale, ce n'est pas avec des issues qui sont plutôt appropriées pour traiter des problèmes à périmètre limité que l'on va s'en sortir. Donc en général, à ce stade là, soit on est perdus·es car le vocabulaire utilisé n'est pas adapté, soit on s'est enfui en courant.


## Une répartition déséquilibrée

Un autre point qui ne nous met pas en confiance c'est que, face à une communauté déjà constituée et principalement composée de développeurs·euses, les nouvelles venues (surtout si iels ne sont pas développeurs·euses) ne sont pas forcément à l'aise. Il faut avouer que pour certains·es d'entre nous, faire face à toute une culture et une organisation à découvrir et intégrer, d'autant plus lorsqu'on voit les animosités qui perdurent entre les différents corps de métier, ça intimide.

## Voir son travail débattu par des novices

Collaborer c'est bien, mais voir un travail de design critiqué par des personnes qui ne s'y connaissent pas et font une platrée de retours non pertinents, c'est vraiment très agaçant (en plus de faire perdre beaucoup de temps à tout le monde). Ce n'est pas qu'on tienne à faire les divas mais si vous faites appel à quelqu'un dont c'est le métier, c'est important de lâcher prise et de lui faire confiance, et non pas de vous permettre de critiquer sous prétexte que "Tout le monde a des yeux donc tout le monde peut juger de la qualité du design"

Histoire de bien me faire comprendre, j'en rajoute une couche : Je sais que faire des retours démontre de l'intérêt pour le travail du designer, et que le but est d'améliorer davantage la réalisation en faisant appel à l'intelligence collective. C'est pourquoi il est, à mon avis, important que les développeurs·euses fassent attention à minimiser le nombre de retours pour ne pas submerger lae designer, ainsi que de ne pas exiger de lui telle ou telle modification. Coté designer, il reste nécessaire d'être à l'écoute des propositions pour être sûr·e de ne rien avoir oublié, ce qui n'empêche pas de faire preuve de pédagogie pour justifier certains choix qui semblerait incohérents (en cas de doute, le mieux étant toujours de faire des tests utilisateurs =p )


## Le design **par comité**

Le pendant extrême de ce débat sur le travail du designer, c'est le design par comité : la communauté s'empare du design et, itérativement, va le modifier afin de satisfaire le maximum de ses membres. Or, comme un travail de design est (encore une fois) global, avec des décisions issues de réflexions du designer, s'en emparer et y apporter des modifications au bon vouloir de chacun·e c'est le vider de son essence dans l'espoir d'obtenir un consensus (consensus qui irait dans le sens des contributeurs·ices et pas des utilisateurs·ices donc on ne se trouve même plus dans un travail de design). Dans ce contexte, pas la peine de redemander au designer de contribuer à nouveau (n'importe lequel d'entre nous sera parti depuis longtemps).

# Bon alors on fait quoi ?

Bon eh bien c'est pas joyeux tout ça. Pas étonnant qu'on y réfléchisse à deux fois avant de contribuer, vu toutes les frustrations qui peuvent s'accumuler quand on commence à chercher à contribuer (L'expérience utilisateur·ice de contribution à un logiciel libre n'est en effet pas terrible !).

Mais on ne va pas rester sans rien faire ! développeurs·euses du logiciel libre, voici une liste d'actions à mettre en place pour que votre projet soit d'avantage propice à accueillir des contributions de designer ↓

## Étape 0 : Se renseigner

Commencez par vous renseigner sur ce qu'est le design et sur la démarche des designers, histoire de mieux comprendre nos petites lubies…

### Des tas de ressources !

Ça passe par un peu de lecture 📚 ↓

#### Blog

Pour mieux comprendre ce qu'est le design :

- [Non, les designers du web ne sont pas des artistes](https://weblog.redisdead.net/main/post/2015/08/28/Non-les-designers-du-web-ne-sont-pas-des-artistes) par [Hellgy](https://weblog.redisdead.net)
- [Quelques astuces pour gérer vos designers-divas en mode associatif](https://weblog.redisdead.net/main/post/2017/03/30/Quelques-astuces-pour-gerer-vos-designers-divas-en-mode-associatif) par [Hellgy](https://weblog.redisdead.net)
- [Making it pretty will not make it usable](https://blog.stephaniewalter.fr/application-jolie-inutilisable-projet-finit-mal/) par [Stéphanie Walter](https://blog.stephaniewalter.fr/)
- Kit de survie du graphiste : [Qu'est ce que la qualité ?](http://kitdesurvie.metiers-graphiques.fr/articles/qu-est-ce-que-la-qualite)

Pour découvrir l'UX Design :

- [Carine Lallemand](http://uxmind.eu/) : [Méthodes d'évaluation UX](https://uxmind.eu/category/methodes-ux/)
- [Raphaël Yharrassarry](http://blocnotes.iergo.fr/)


#### Livres

Pour celles et ceux qui veulent pousser plus loin :

Mieux collaborer avec les designers :
- [Métier web designer](https://www.eyrolles.com/Audiovisuel/Livre/metier-web-designer-9782212135275) de Mike Monteiro
- [Web designer cherche client idéal](https://www.eyrolles.com/Audiovisuel/Livre/web-designer-cherche-client-ideal-9782212141627) de Mike Monteiro

Pour mieux comprendre certains aspects du design :
- [Design émotionnel](https://www.eyrolles.com/Informatique/Livre/design-emotionnel-9782212133981) de Aarron Walter
- [La phase de recherche en web design](https://www.eyrolles.com/Audiovisuel/Livre/la-phase-de-recherche-en-web-design-9782212141467) de Erika Hall
- [Don't make me think](https://www.placedeslibraires.fr/livre/9782744025525-don-t-make-me-think-optimisez-la-navigation-sur-vos-sites-steve-krug-roger-black/) de Steve Krug

### Étape 1 : Diffuser

Une fois que vous êtes bien renseignés·es, il est temps d'essaimer  ↓

#### Allez dans les écoles de design

Contactez et rencontrez les différentes promos de futurs·es designers histoire de trouver des contributeurs·ices, que se soit maintenant ou plus tard, sur leur temps libre ou pour des travaux liés aux études. Essayez également de voir avec les enseignants si ils sont intéressés par le fait de monter un projet autour de votre logiciel (d'habitude on fait des réalisations qui ne serviront jamais alors pourquoi pas ?)

Je vous recommande d'aller voir :
- les BTS Design Graphique option médias numériques, qui forment spécifiquement des designers d'interface. Le site national du design et arts appliqués les a listés [par ici](http://designetartsappliques.fr/content/btscvmma)
- les DUT MMI (Métiers du Multimédia et de l'Internet) qui sont plus généralistes (on y fait du design, du code et plein d'autres choses). Le blog du MMI a fait [une liste](https://blogdummi.fr/liste-des-dut-mmi/)
- Certaines [licences ou licences pros](https://blogdummi.fr/poursuite-etudes-apres-dut-mmi/) doivent aussi être réceptives mais c'est à voir au cas par cas
- Idem pour les écoles privées, je ne me prononce pas mais pourquoi pas essayer…

#### Organisez des rencontres IRL

Organisez-vous pour vous retrouver et inviter les nouveaux·elles autour d'un thé, café, chocolat, apéro pour faire connaissance, comprendre ce que vous faites et surtout comment vous le faites, comment vous vous organisez… Ça pourrait même vous permettre d'échanger à propos de vos besoins et de créer du lien 👌

#### Formulez vos besoins clairement et publiez-les

Demandez-vous quels sont les problèmes qui pourraient être résolus par un designer et efforcez-vous de les formuler sans énoncer de solution : "Je vais rajouter une nouvelle fonctionnalité et je ne sais pas comment l'intégrer au logiciel en cours", "mes utilisateurs ne trouvent pas les fonctionnalités dont ils ont besoin", "mes utilisateurs ne comprennent pas comment fonctionne mon logiciel"… Puis publiez-les sur une page dédiée histoire que si quelqu'un a envie de contribuer, il sache directement où aller pour commencer. Et n'hésitez pas à demander à des personnes dont c'est le métier si vous avez bien étiquetté vos besoins, afin qu'ils soient associés aux bonnes compétences :)

### Étape 2 : Accueillir

#### Être très très accueillants

Vraiment très très accueillant : quand vous accueillez des gens chez vous pour la première fois, vous ne les faites pas rester sur le pas de la porte le temps de finir de cuisiner le plat principal ! Sur le net c'est pareil, indiquez à la personne à qui elle peut s'adresser pour faire connaissance, qui sont les personnes qui détiennent quelles informations, demandez-lui si elle a besoin de quelque chose en particulier… Et souvenez-vous qu'elle a galéré pour parvenir jusqu'à vous, vous lui devez bien ça :3
Cela passe aussi par la mise en place de modes de discussion appropriés : proscrire les propositions d'issues et ouvrir un framateam pour pouvoir discuter facilement, ça facilite énormément les choses :) Si vous avez un doute, commencez par demander à la personne de quelle façon elle préfère communiquer !

#### Se rendre disponible

Restez disponible (et communiquez sur quand est-ce que vous l'êtes) pour être une personne ressource, le temps que lae nouvel·le arrivant·e se familiarise avec votre fonctionnement, ait les informations de base, sache par où démarrer… Cela lui évitera de se décourager et lui permettra de se sentira soutenue.


#### Communiquer de façon inclusive

Les communautés du logiciel libre sont plutôt composées de devs, avec un profil correspondant plutôt aux hommes, blancs, cisgenres et hétéros (voir [l'enquête de StackOverflow](https://insights.stackoverflow.com/survey/2018/)). Or si vous avez la volonté d'ouvrir votre communauté à des profils plus variés, communiquer de façon inclusive est une obligation pour accueillir les gens dans de bonnes conditions. Cela passe par utiliser un langage adapté aux connaissances de chacun·e (et non pas technique), ainsi qu'à proscrire toutes les blagues sexistes, LGBTphobes ou racistes.


### Étape 3 : Collaborer

Et pour finir, une fois que l'on est bien intégré·e :

#### Former un petit groupe de travail

Pour travailler sans devoir expliquer sans cesse à un grand nombre de personnes ce que l'on fait (et surtout ne pas répondre à chaque question ou retour pour ne pas perdre un temps infini), il est de bon ton de mettre en place un petit groupe de travail, composé de 4/5 personnes par exemple, histoire d'ouvrir le dialogue entre personnes se faisant confiance et ne pas mettre trop le nouveau venu en défaut face à des contributeurs·ices se connaissant déjà bien.

#### Former un binôme

L'idéal, c'est de former un binôme dev-designer pour qu'il soit facile de faire des ping-pong lorsqu'il y a des questions ou incompréhensions. C'est aussi l'occasion d'établir une relation de confiance, ce qui permettra au designer d'avoir plus de poids si il doit justifier ses choix à terme.

#### Parler des problèmes pas des solutions

Une fois que le designer vous a montré les changements à mettre en place, et que malgré la confiance que vous avez en ses compétences, un choix vous semble problématique pour les utilisateurs·ices, n'hésitez pas à formuler votre interrogation en problème plutôt qu'en solution. Cela permettra au designer de vous expliquer ses choix ou de réaliser qu'un aspect de la réalisation lui avait échappé !

#### Et le design par comité ?

Dernier conseil (oui j'en remets une couche mais c'est important) : Arrêtez le design par comité.

# La solution ? La communication

Apaiser les tensions entre les développeurs·euses et les designers, c'est déjà un sacré boulot. Mais contribuer ensemble ne sera pas possible si chaque partie ne mets pas de l'eau dans son vin et ne cherche pas à comprendre les problématiques rencontrées par les autres corps de métier.

# Prologue : Encore un peu de ressources ?

- [Le design dans le libre : pistes de réflexion](https://mariejulien.com/post/2017/02/08/Le-design-dans-le-libre-%3A-pistes-de-r%C3%A9flexion) - @MarieJulien
- [Table ronde design et open source au Reset](https://mariejulien.com/post/2017/02/13/Table-ronde-design-et-open-source-au-Reset) - @MarieJulien
- [Graphisme / UX versus Sécurité : deux mondes opposés ?](http://pixellibre.net/2017/07/graphisme-ux-versus-securite-deux-mondes-opposes/) - @Numendil
- [Lettre pour un logiciel libre](http://www.maiwann.net/lettre-pour-un-logiciel-libre/) - @maiwann

# Remerciements
Merci à celleux de Framasoft qui ont su m'accueillir avec enthousiasme ! (particulièrement @pyg et @pouhiou)

Merci aux mastonautes qui ont répondu à mes questions : @daycode@mastodon.social, @delphin@mastodon.social & @Quadragondin@mamot.fr

Merci à @dascritch pour ses ressources

Merci aux relecteurs : Éric, Fabien, Marien, Théotime =)
