---
layout: post
title: "Zam 1 : Premier contact"
subtitle: ""
date: 2019-11-03 15:00:00 +0200
vignette: null
intro: "Cela fait bientôt un an que je travaille pour Zam. C'est mon premier revenu régulier en tant qu'indépendante, mais c'est surtout un projet et une équipe extrêmement chouette, dont j'ai envie de vous parler depuis le premier jour, notamment car je me dis que c'est important de documenter comment est-ce que je travaille, avec qui, et comment nous travaillons ensemble."
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam1-premier-contact/
---

Cela fait bientôt un an que je travaille pour Zam. C'est mon premier revenu régulier en tant qu'indépendante, mais c'est surtout un projet et une équipe extrêmement chouette, dont j'ai envie de vous parler depuis le premier jour, notamment car je me dis que c'est important de documenter comment est-ce que je travaille, avec qui, et comment nous travaillons ensemble. C'est une histoire un peu longue, qui, je l'espère, sera l'occasion d'écrire plusieurs billets :) (*Spoiler : finalement, j'en ai écris 17 :D*)

C'est parti !

# Zam 1 : Premier contact

## Il était une fois, à SudWeb

Mai 2018

Il fait beau, et je suis à Anduze pour la [8e édition de SudWeb](https://sudweb.fr/2018/) (qui a été une conférence capitale pour beaucoup de moments de ma vie, notamment le fait de devenir freelance et celui de tester l'itinérance… mais je m'égare). J'y re-croise [Raphaël](https://mamot.fr/@rafoo), qui me parle d'une ["startup d'état"](https://beta.gouv.fr/startups/) sur lequel une équipe s'est montée. Or, l'équipe s'aperçoit qu'il y a des enjeux au niveau de la mise en forme du contenu, et Raphaël a pensé à moi pour les aider.

A ce moment là, tout est très vague. Je pense que Raphaël n'est pas bien sur de ce que je fais comme métier, mais qu'il me positionne vaguement entre graphisme et ergonomie, et du coup, même si il ne sait pas précisément comment, ça à l'air de coller par rapport à ses besoins d'avoir un contenu mieux "mis en forme" ou "améliorer la lisibilité de l'application".

Il m'explique aussi que, comme nous n'avons jamais travaillé ensemble, il apprécierait de commencer petit. On se met d'accord pour 2 jours de boulot, je lui dit que j'améliorerai ce qui est possible dans ce temps là, et on se donne rendez-vous pour en reparler plus tard !

Note : J'ai d'ailleurs constaté qu'il y a 2 types de stratégies lorsque je commence à travailler sur un projet : Soit on ne me donne aucune info, et on me laisse définir ce que je trouverais prioritaire avec un oeil neuf, soit, à l'opposé, on me propose de commencer par un tout petit point pour ensuite élargir mon champ d'action. **Le tout toujours facturé au temps passé** ce qui est parfait pour travailler dans de bonnes conditions (et plus je passe du temps en freelance, plus le travail facturé au temps passé me semble indispensable !).


## Pourquoi Zam ? : La visionneuse

Peu après, je suis ajoutée au Slack dédié au projet et récupère les accès à l'application (je parle aléatoirement d'application (web) ou de logiciel). Je prends contact pour la première fois avec l'équipe : [David](https://larlet.fr/david/) et [Ronan](https://ronan.amicel.net/), les développeurs, et Mélodie, chargée du projet (on dit intrapreneur chez les startup d'état, mais peu importe). Elle m'explique à qui et à quoi Zam est destiné :

Avant qu'une loi soit votée, elle est publiée afin que les députés puissent la lire et y proposer des modifications. Ces modification sont ce qu'on appelle **des amendements.** En général, si la loi passe à l'assemblée le mardi, les députés ont le droit de déposer ces amendements jusqu'au vendredi précédent, fin d'après-midi (17h par exemple). Une fois la date de dépôt des amendements passée, c'est aux petites mains du ministère de s'activer : Ces personnes récupèrent les amendements, les lisent, indiquent la position (avis) du gouvernement (favorable, défavorable…) et rédigent une réponse afin que, lorsque læ ministre défendra son projet de loi, iel puisse indiquer sa position vis à vis de l'amendement proposé, argumentaire et chiffres à l'appui. (Si vous voulez en savoir plus sur les amendements : [https://data.senat.fr/aide/notice-explicative-ameli/](https://data.senat.fr/aide/notice-explicative-ameli/) )

On me propose de travailler sur ce que l'équipe appelle **"la visionneuse"**, c'est à dire une application web pour tablette afin que, à l'intérieur de l'assemblée, læ ministre puisse, en temps réel, naviguer à travers les amendements et lire leur contenu ou la réponse qu'on lui propose de donner au micro.

Pourquoi une application numérique ? Eh bien parce que le reste du temps, le "dossier de banc" qui regroupe les amendements imprimés et leur réponse **fait plusieurs centaines de pages,** si bien qu'il est divisé en plusieurs parties pour être transportable et manipulable. De plus, certaines réponses sont mises à jour à la dernière minute, ce qui oblige à naviguer à travers tout ces feuillets pour mettre à jour le dossier. D'ailleurs il y a plusieurs dossiers, un pour læ ministre et un pour chacun de ses conseillers (ce qui fait 3 ou 4 fois plus de papiers imprimés en plusieurs exemplaires à mettre à jour au fur et à mesure…).

En revanche, le choix d'une version numérique implique une attention toute particulière : Il faut que le contenu soit accessible quoi qu'il arrive (si læ ministre ne peut pas répondre aux députés dans l'assemblée, c'est problématique !). Et donc, pas question d'être piégé par le fait de ne pas avoir de réseau.

Un des positionnements initiaux est donc que l'application doit **regrouper son contenu sur une seule page,** qui sera chargée une fois et permettra donc l'accès au contenu sans risque vis à vis d'un réseau potentiellement capricieux.

## Premier contact avec l'appli

Je prend donc connaissance de la version actuelle de l'application. Je n'ai malheureusement plus qu'une capture pour vous la présenter :

<figure>
  <img src="/img/posts/2019_Zam/01/zam_0.png" alt="">
</figure>

Une capture avec [plus de détails par ici](/img/posts/2019_Zam/01/Zam_0bis.png)

Précisions :
- Les + ouvrent un "accordéon" qui permettent d'accéder au contenu de l'amendement et de la réponse rédigée pour le gouvernement.
- Les listes de chiffre, à dérouler à coté de "Article", permettent de naviguer rapidement d'un article à un autre.

Je commence à arpenter en long et en large ce qui existe, et je demande à Mélodie des clarifications et des priorisations pour le contenu :
- A quoi servent les emojis ? Et la couleur de fond de chaque amendement ?
- C'est quoi un amendement gouvernement ?
- Est-ce que la facilité de navigation entre les articles est importante ?
- Qu'est-ce qui est le plus important à mettre en avant ? L'avis du gouvernement (favorable, défavorable…), qui a déposé l'amendement, le corps de l'amendement, la réponse à l'amendement, le numéro de l'amendement…?
- Pourquoi est-ce qu'il y a des amendements qui sont tous regroupés ensemble ? (comme le premier sur la capture d'écran)
- Dans quel ordre sont affichés ces amendements ?

Une fois que j'ai un peu défriché ce qui est important et pour quel contexte, je commence le maquettage.

## On recommence tout ! : Premières maquettes

### Les priorités

Si cette version m'a aidée à comprendre ce qui était important et ce qui l'était moins, je me vois mal itérer à partir de celle-ci pour l'améliorer graphiquement. Je repars donc de 0. Mes priorités sont de réaliser une maquette qui permet, prioritairement :

- Pour chaque amendement: De savoir rapidement qui l'a déposé et quelle est la position du gouvernement
- Un accès très rapide à la réponse d'un amendement,
- Pour chaque réponse : Un confort de lecture optimal pour læ ministre qui aura plusieurs choses à gérer au banc, dont la lecture de la réponse.
- De savoir facilement à quel article se réfère un amendement qu'on a sous les yeux,
- De naviguer facilement au sein du contenu pour passer d'un article à un autre rapidement
- Un accès rapide au contenu d'un amendement,
- Un accès rapide au contenu d'un article.

- Une navigation facilitée entre les articles pour passer rapidement d'un article à l'autre.


Le tout sur une page unique, donc avec de gros soucis de performance, on peut parfois aller jusqu'à plus de 1800 amendements !


### Première version
Après un peu de recherche sur comment les sites mettent en oeuvre de la hiérarchisation d'information grâce à de la typographie, je remonte mes manches et je me lance pour une 1e version !

<figure>
  <img src="/img/posts/2019_Zam/01/zam_1.png" alt="">
</figure>

[Par ici pour voir la page de toute sa hauteur](/img/posts/2019_Zam/01/zam_1bis.png)

Je fais le choix de différencier les typographies : Une typographie serif / à empattements pour ce qui concerne les articles, et une sans-serif / sans empattements, pour les amendements, car c'est sur eux (et surtout leur réponse) que se concentre l'enjeu de lisibilité.

Les liens sont soulignés pour indiquer qu'ils sont cliquables,
Les bandeaux "Article" sont sticky pour que, à tout moment, il suffise de lever les yeux pour connaître le contexte auquel fait référence l'amendement (Lorsque l'on scrolle, on a "Article 3" qui se positionne en haut de l'écran, et qui sera remplacé par "Article 4" lorsque l'on arrivera à sa section)

Pour chaque bloc amendement, on retrouve le minimum d'information pour limiter la charge cognitive : Le numéro, la personne ayant déposé l'amendement, et l'avis du gouvernement. Cette information, très importante, est doublée par un bandeau coloré à gauche pour être facilement lisible (D'ailleurs on m'a remonté des retours d'utilisateurs qui disaient que c'était vraiment chouette ce bandeau !).
Et ensuite, on accède soit au corps de l'amendement, soit à la réponse, légèrement mise en avant car sa taille de police est plus grande, et elle est en gras.

Les liens plient et déplient les textes plus grand. Le texte d'une réponse est affichée dans une taille de police assez grande, dans le but d'être lisible facilement de loin. Une fois un contenu déplié, les liens se transforment en lien "Replier", et j'en positionne un en bas à droite de chaque texte pour ne pas avoir à remonter et que l'utilisateur risque de perdre son contexte.

Le menu de navigation en haut à gauche permet lui de naviguer rapidement au sein des articles.

Exit les petits emojis pouce vers le haut et pouce vers le bas et les couleurs de fond qui communiquent sur l'avis du gouvernement, car pas assez claires (il faut déduire la position à partir de ces indications, je choisis de l'indiquer clairement (en l'écrivant) pour qu'il n'y ai pas de doute, et de doubler avec de la couleur pour une lecture plus rapide une fois l'utilisateur plus habitué).

### Moins de texte !

Cependant, après discussion avec Mélodie, cette maquette répond aux priorisations mais est assez lourde car les liens "lire l'amendement" et "lire la réponse" ajoutent du texte à chaque amendement, alors qu'il y en a déjà beaucoup !

C'est le moment de mon premier défi : Il faut que je réussisse à signifier que, pour chaque amendement, il est possible d'avoir accès au contenu de l'amendement, et à la réponse de l'amendement, et de la façon la plus graphique possible afin de minimiser le texte, surtout que la représentation que je choisirai sera répercutée sur tous les amendements de l'application (donc potentiellement 1800 fois !!).

Je rajoute qu'il est important de hiérarchiser en mettant en avant la réponse de l'amendement, car c'est une action principale, qui doit pouvoir être réalisée rapidement (par exemple, au moment où un député vient de finir de défendre on amendement, il faut attraper la tablette et ne pas perdre de précieuses secondes à chercher le bouton pour déplier la réponse, voire pire, ouvrir le contenu de l'amendement au lieu de sa réponse, le lire et se tromper devant toute l'assemblée !!)

### Des icônes !

<figure>
  <img src="/img/posts/2019_Zam/01/zam_2.png" alt="">
</figure>


Je profite de cette nouvelle version pour améliorer la graphisme afin de mieux délimiter les groupes fonctionnant ensemble, en représentant les amendements sous forme de cartes sur fond gris, et les articles sous forme de bandeau qui se différencient des amendements pour bien marquer un changement de groupe.

Je rajoute ensuite à mes liens dépliables une petite icône représentant un texte, associé au verbe "Lire" à coté pour inciter à l'action.
Afin de bien différencier ce qui est dédié à la lecture du corps de l'amendement, et ce qui déplie la réponse, je réalise une séparation à l'aide d'une petite barre, pour que les boutons soient mieux séparés, chacun faisant référence à ce qui se trouve à sa gauche : le bouton du haut, fait référence au corps de l'amendement, celui du bas, qui fait face à la réponse, déplie…la réponse !


Je tente de mettre un peu en avant le bouton dédié à la réponse sans en faire 2 différents alors qu'ils ont la même fonction… mais le résultat n'est pas convaincant, car j'épaissit à peine le libellé et la bordure… De plus, j'ai peur que comme ces boutons ne sont pas clairement distinguables, on clique sur l'un en le confondant avec l'autre et que cela perde l'utilisateur.

Je change alors de stratégie en différenciant clairement les boutons : Soit je veux voir le corps de l'amendement, et il y a une icône discrète à coté du titre pour le faire (discrète car l'action est secondaire), soit je veux réaliser l'action principale et il y a un bouton dédié et donc plus mis en avant pour le faire.

<figure>
  <img src="/img/posts/2019_Zam/01/zam_3.png" alt="">
  <img src="/img/posts/2019_Zam/01/zam_3bis.png" alt="">
</figure>

Sauf que… personne ne comprend que l'icône "oeil" est cliquable ! Je fais machine arrière et reviens sur l'optique d'avoir des boutons. De plus, David me signale que l'icône "texte" ressemble à l'icône "menu"… je la supprime donc pour éviter les incompréhensions.

### Différencier des boutons qui ont la même fonction…mais pas la même priorité

<figure>
  <img src="/img/posts/2019_Zam/01/zam_4.png" alt="">
</figure>


Cependant j'en reviens encore à ma problématique précédente :
Les boutons se ressemblent, ont le même libellé et sont à peine différents (car j'essaie malgré tout de les différencier)… Pas assez pour que se soit clairement lisible, le risque étant une gêne chez l'utilisateur, qui ne comprendrai pas pourquoi ces boutons semblent identiques et pourtant pas complètement…

Je décide donc d'harmoniser le tout !

<figure>
  <img src="/img/posts/2019_Zam/01/zam_5.png" alt="">
</figure>

Mais j'ai toujours besoin de différencier ces deux actions… Je décide donc d'en changer le libellé, même si il est préférable d'avoir un verbe d'action sur un bouton, car je ne trouve pas de meilleur compromis.

<figure>
  <img src="/img/posts/2019_Zam/01/zam_6.png" alt="">
</figure>

C'est un peu mieux, mais pas suffisant, surtout qu'il faut lire consciemment le libellé du bouton pour savoir si l'on déroule "Texte" ou "Avis". De plus je trouve que les boutons sont ce qui se distingue le plus au sein de l'interface, alors que c'est le texte qui devrait être prioritaire…

<figure>
  <img src="/img/posts/2019_Zam/01/zam_7.png" alt="">
</figure>

En passant de "Avis" à "Réponse" dans le libellé d'un bouton, j'augmente la différence, car ils ne font plus la même largeur et donc sont encore plus facilement dissociables ! Je tiens le bon bout ! Mais les boutons sont encore trop pregnants à mon goût

<figure>
  <img src="/img/posts/2019_Zam/01/zam_8.png" alt="">
</figure>

Dernière modification : Je change l'icône en un oeil moins imposant et tout de suite, le bouton perd en importance, il y a plus de blanc qui circule dans l'interface, ca allège et ça fait du bien !

### Le fonctionnement de ces itérations

On peut croire que, au fil des maquettes, c'est mon oeil critique qui a systématiquement cherché l'amélioration. La vérité c'est que j'ai eu droit à 3 paires d'yeux supplémentaires, qui, bien que non designer, s'efforcaient de verbaliser si parfois elles ressentaient une gêne dans ce que je proposais. Il y a donc eu beaucoup d'aller-retours, de discussions, d'explications et d'argumentation pour dissocier ce qui relevait de l'impression ou craintes personnelles et de ce qui était ressenti par plusieurs personnes.

Je me découvre d'ailleurs souvent embêtée de devoir expliquer mes choix, car j'ai une pensée intuitive plutôt qu'argumentative, et qu'il me faut donc refaire tout le chemin qui m'a mené à mon résultat actuel en construisant une argumentation afin d'expliquer pourquoi j'estime que c'est la meilleure décision face à la problématique… Ou parfois reconnaître tout bêtement que je m'étais bien trompée ! (L'exemple de l'icône oeil utilisée seule est un excellent exemple de plantage, elle était si peu affordante que personne, alors qu'ils savaient que la fonctionnalité existait, n'a compris qu'elle se trouvait derrière&nbsp;!)


## La suite ?

C'est tout pour ce premier billet !
Si vous en voulez encore, le suivant parlera de quelquechose de plus costaud : [Le répondeur](/blog/zam2-le-repondeur/), encore plus de complexité pour rendre l'interface simple et satisfaire les besoins utilisateurs !
