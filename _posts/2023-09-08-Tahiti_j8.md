---
layout: post
title: "Tahiti - jour 8"
subtitle: ""
date: 2023-09-08 09:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/tahiti_j8/
---

> J'AI VU UNE ******

Le matin, nous filons au lycée professionnel où enseignait mon papa. Nous y avions des souvenirs d'enfant et j'avais très envie de les confronter à la réalité d'adulte. Notamment le souvenir d'un amusement avec les autres enfants de prof, celle de sauter du haut d'une marche du forum extérieur.
Pour pouvoir entrer au lycée, on s'est inscrits pour se faire coiffer au salon de coiffure des élèves du lycée pro :)

Eh bien non seulement la personne qui m'a coiffé était sympa, mais elle m'a fait le seul brushing de ma vie qui ne m'a pas brûlé, in-cro-yable. Et pour la marche, elle existe belle et bien et se situe à presque 1m50 de haut o_o Même avec ma taille d'adulte je n'aurai pas envie de la sauter ! Nous étions aventurières enfants (et nos parents assez détendus visiblement).

Repas rapide avant de filer au club de plongée. Là aussi rien n'a changé, je reconnais l'entrée, la salle de restauration, le club, le banc vert, le rangement des gilets mais reconnait que celui des palmes à changé, bref je suis assez incollable :p Il y a même un ami de mes parents qui est toujours le directeur du club, lui aussi n'a pas changé et me dit même que je ressemble à mon père (remarque que je pensais ne plus entendre, comme quoi :p)

On me prépare mes affaires, un shorty (une combi courte), un gilet (pour fixer la bouteille dessus), un détendeur, une ceinture de plombs avec 4 kg (pour que je coule !), j'ai mes palmes et un masque, on est bon. On me donne un moniteur, je serai avec Roch. On embarque sur le bateau (là aussi toujours le même), je reconnais l'échelle, la disposition du bateau, tout est familier. Roch est bien sympa et ne cesse de valoriser mes connaissances pendant le brief. Me dis que j'ai l'incroyable compétence de dissocier respiration par le nez et respiration par la bouche (si toi aussi tu ne pensais pas que c'était une compétence, tape dans tes mains), bref il est cool avec moi et ça me met en confiance.

On démarre la plongée, et après quelques réglages de masque on commence à descendre. Petite panique passagère, entre le masque qui s'enfonce et qui glisse, le détendeur qu'il faut garder en bouche, l'absence de contrôle sur ma flottaison (le moniteur dégonfle ou regonfle le gilet selon si il veut me faire descendre ou remonter), je suis assez destabilisée. Mais une fois ce moment passé, je profite. Deux bémol : le masque est un peu abimé et du coup je ne vois pas hyper bien les poissons, et je trouve qu'il n'y en a pas beaucoup, des poissons. Peut-être dû à un effondrement de biodiversité ? Peut-être juste à des souvenirs d'enfant exagérés ? En tout cas, je suis ravie car je retrouve une anémone qui était au même endroit il y a 20 ans, et le moniteur me laisse la toucher (c'est trop rigolo).

On continue le tour et tombons à un moment sur une grosse coquille enfichée dans le corail. Roch me la montre, j'essaie de comprendre ce que c'est. La coquille remue, je suppose qu'un gros poisson va sortir de dessous, peut-être une murène ? A moins que…

Ca remue, ça remue, et la tortue fait demi-tour. Une tortue !!!! Quelle chance :$ Elle repart sur la droite et après que nous ne la distinguions plus, mon moniteur me fait aller sur la gauche. Frustration de ne pas suivre la tortue ! Mais quel bonheur de l'avoir vue…

Suite et fin de la plongée, on continue à voir des tas de petits poissons, des cochers, des petits perroquets, des tas dont j'ai perdu le nom (mais que je vous retrouverai), puis on repasse devant l'anémone que je retourne toucher, elle s'accroche tellement à mes doigts qu'en essayant de les enlever je la décolle un peu du rocher !!! hiiii !!

Bon l'anémone s'en sortira sans moi, et nous on finit la plongée qui a duré 40 minutes en tout, trop chouette =)

<!-- Je suis aussi descendue jusqu'à 9m80 mais on n'a pas le droit de le dire :p -->

On retourne à quai, on nettoie tout dans les bacs d'eau douce, je vais récupérer mon diplôme et l'on repart.

On file jusqu'à la Pointe Vénus dans l'espoir d'y voir le coucher de soleil (je ne comprends pas qu'on n'ai pas passé notre temps à faire ça quand on habitait là), il est magnifique bien orangé à coté de Moorea. On rentre en passant acheter une pizza à l'ananas, et puis on fait les bagages pour le bateau où on ne veut pas tout emmener. Heureusement qu'on a fait le tri car un bébé margouillat aurait embarqué avec nous sinon !

Les voisins chantent encore mais il est l'heure pour moi de fermer les écoutilles. A partir de demain, c'est voilier, grosse inconnue sur l'électricité à bord (surement inexistante ?). Il parait qu'on va mettre 20 à 30h pour rejoindre la première île, on va essayer d'aller à Huahine, Tahaa, Tetiaroa et Maupiti. On verra bien !

Nana =)
