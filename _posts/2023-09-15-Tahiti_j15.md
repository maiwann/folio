---
layout: post
title: "Tahiti - jour 15"
subtitle: "Voilier - jour 7"
date: 2023-09-15 09:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/tahiti_j15/
---

J'ai trop mal dormi, je me lève pour regarder les couleurs du début du jour avec ma soeur, et chance, on voit tout plein de raies depuis le bateau, elles sortent leurs ailerons et tournent un peu près du ponton, chance !!


Une fois le ballet passé, je retourne me coucher pendant que d'autres essaient de voir les raies en PMT. J'ai pris trop cher les jours précédents, et ai trop donné émotionnellement, je suis exténuée (et totalement deg de ne pas être en état d'y aller mais bon (en vrai je suis énervée vontre elleux :p))

Je me réveille lorsqu'ils reviennent, pas de raies. On se donne comme mission d'aller acheter les billets pour le ferry, car impossible de réaliser l'achat en ligne. On marche, on marche, je perds le peu d'énergie que j'ai récupéré (haha), on réussit à acheter nos billets, on repart. Je ne vous l'ai pas dit mais ici il y a des crabs par-tout, et les crabes ici à Tahiti ils se cachent… dans leur trou ! Donc on croise plein de crabes qui filent dans leur trou, c'est rigolo :3

De retour au bateau, on a tellement faim qu'on mange. Puis on va faire un tour au ponton pour voir les raies mantas, mais… on ne voit rien.

On enchaîne avec un passage au jardin de corail. On y a vu beaucoup (trop) de rori (des concombres de mer), des tas d'anémones avec des poissons très énervés qui nous attaquaient si on voulait les toucher, des oursins, des tonnes de bénitiers très très gros, des petits poissons clowns, un beau banc de poisson, des poissons perroquets, des poissons patriotes (ils ne s'appellent pas comme ça mais ils ont un drapeau bleau blanc rouge juste derrière leurs nageoires, trop rigolo :D ), et plein d'autres !

Et … un requin pointe noire !! Ça n'est pas spécialement dur à voir mais après toutes les choses super qu'on a vues, ça aurait été dommage de ne pas en voir un :)

Le retour se fait frigorifiés, le soleil du matin a disparu. On ne se change pas en arrivant, car on veut encore tenter notre chance pour les raies. Une heure plus tard donc, alors qu'on aperçoit les ailerons d'une raie au loin, on se jette à l'eau alors qu'on se gèle pour les apercevoir.

Mais c'est raté, on n'a rien vu du tout :) On rentre donc gelés, vite séchés et habillés pour la dernière soirée à bord.
