---
layout: post
title: "Première réunion partisane"
subtitle: ""
date: 2022-03-23 21:00:00 +0200
vignette: null
intro: "Je suis allée à une rencontre du groupe local de La France Insoumise, parce que pourquoi pas !"
nom_blog: le blog
which_blog: blog_perso
permalink: blog/premiere-reunion-partisane/
---

Je sors de ma première réunion partisane de toute ma vie, une de la France Insoumise de Dinan.

Pour l'historique, je me suis inscrite il y a maintenant quelques mois sur le "réseau social militant" de la France Insoumise : [Action Populaire](https://actionpopulaire.fr/)

Le site est très bien fait (merci [Agathe (lien Twitter)](https://twitter.com/MenicaFolden) ) (et il a de nombreuses ressemblances avec Mobilizon) et apporte plein de petites façons de contribuer (être référente d'immeuble, proposer de prendre des procurations… avec des fiches pratiques sur [https://infos.actionpopulaire.fr/](https://infos.actionpopulaire.fr/) ). Mais il permet aussi de voir ce qui se passe près de chez soi, et justement alors que je venais de m'abonner au groupe local dinannais, juste pour voir… ils se retrouvaient ce mercredi aprem !

Alors j'y vais… j'y vais pas…

Mon angoisse sociale était forte, mais j'avais trop peur d'avoir des regrets. Alors j'y suis allée en me disant que si ils étaient trop pénibles ou insistants, eh bien je filerai !

Un gros point positif : Quand je me suis inscrite au groupe, une dame m'a écrit pour me souhaiter la bienvenue :) Déjà ce n'était pas exclusivement masculin, youpi !

Donc je me suis rendue au point de rendez-vous, un petit bar à Dinan. Et là, surprise quand j'arrive : Les gens sont vieux ! Alors rien de grave, mais j'avais en tête que les militant·es de LFI étaient plutôt jeunes, grave jeunes :) Et là je me suis fait traiter de p'tite jeune, c'était marrant !

Bilan de la soirée : Iels ont pas mal parlé du gros rassemblement de Paris, de la frustration énorme de ne pas voir suffisamment de répercution dans la presse (alors que franchement si c'est pas Mélenchon qui gagne, ça va être chaud pour les servies publics d'information qu'il reste !), du fait d'être délégué pour les bureaux de vote (alors je sais paaaas ce que c'est mais [il y a une vidéo](https://melenchon2022.fr/assesseurs-delegues/) pour expliquer la différence avec assesseur·euse… bien fait ce site je vous dit !!)

Et enfin des actions. Il y en a qui vont coller des affiches, d'autres qui vont tracter sur le marché, d'autres qui vont faire du porte-à-porte, et d'autres enfin qui vont "boîter" c'est à dire mettre des tracts dans les boîtes aux lettres.

Alors moi j'hésitais entre l'échéance qui est quand même très bientôt, et donc l'envie de faire des choses, et mon flip total d'interagir avec des gens.

Mais comme rien ne m'a été demandé, je me suis décidée à prendre des tracts pour les mettre dans les boites au lettres, et à aller tracter demain au marché !!

On verra bien ce que ça donnera !!

Bonus : Il y en a plusieurs qui pensent que si Mélenchon passe au second tour, ça sera gagné pour lui ! Alors la position m'a sacrément étonnée, mais leur point de vue se tenait : un débat face à Mélenchon pourrait être fatal à Macron, et iels pensent qu'il sera mangé tout cuit ! Youpiii !

Bonus 2 : La parole tournait pas mal, c'était un peu cacophonique parfois mais il n'y a pas eu de mec relou qui gardait la parole et ça, ça m'a fait bien plaisir =)
