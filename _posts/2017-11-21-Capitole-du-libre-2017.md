---
layout: post
title: Libristes de tous les pays, rassemblez-vous !
subtitle: "Ce que j'ai vu au Capitole du Libre 2017"
date: 2017-11-21 11:47:00 +0200
vignette: null
intro: "Ce que j'ai vu au Capitole du Libre 2017"
nom_blog: le blog
which_blog: blog_pro
permalink: /capitole-du-libre-2017/
---

Après une semaine entourée de gens fantastiques dans les Pyrénées, et avec trop peu d'heures de sommeil dans les pattes, je me suis rendue au Capitole du Libre 2017 (abrégé CDL ensuite), organisé par Toulibre à Toulouse. L'évènement amène des speakers de toute la France (et même plus loin encore !) et est une vraie mine d'or pour tous celleux qui s'intéressent au logiciel libre, aux communs et à la vie privée.

Petit tour d'horizon de ce que j'y ai vu et entendu :

# Samedi matin : L'intérêt du libre, expliqué le dimanche en famille.

J'ai démarré le CDL par la conférence de [Delphine](https://framapiaf.org/@DelphineM), que j'espérais accessible et riche d'astuces pour vulgariser le logiciel libre à des néophytes. Je n'ai pas été déçue, elle a expliqué très clairement ce qu'était le logiciel en général, la différence entre logiciel libre et privateur, avec les *4 libertés défendues par la communauté du libre* (que je ne connaissais pas, je suis d'autant plus ravie !):
- La liberté *d'utiliser le programme*
- La liberté d'*étudier son fonctionnement*
- La liberté de le *redistribuer*
- La liberté de *distribuer des services modifiés.*

Elle a ensuite abordé l'ensemble des avantages et des freins à l'adoption du libre, de façon très pointue (Fiabilité, Sécurité, Interopérabilité, Autonomie, Indépendance, Innovation, Créativité, Personnalisation, État d'esprit et Prix), nous a au passage rappellé le modèle économique des plus grosses boites mondiales : la *revente des données personnelles*, ce qui prouve à celleux qui en doutent que nos données valent de l'or !

Enfin, Delphine conclut en nous proposant des façons d'agir, de participer et de transmettre, en mettant bien l'accent sur le fait que cela doit se faire *à notre rythme*.

Pour ne rien gâcher, elle a mis [ses slides en ligne](http://articles.nissone.com/2017/11/interet-libre-explique-dimanche-midi-famille/). C'était un moment très riche qui montre bien que ce n'est pas parceque l'on vulgarise un sujet qu'on ne peut pas être précise et dense tout en étant claire ! Je vous recommande de la regarder avant votre prochain dîner de famille ! (lorsque le CDL l'aura mise en ligne :) )

# Samedi aprem : Du repos et du Pouhiou

Après avoir profité du soleil pendant la plenière, je me décide à aller voir [Pouhiou](framapiaf.org/@Pouhiou) pour sa conf' "Why so serious ?".

Il nous a parlé de sa place à Framasoft, et de la bienveillance qui y règne. Apparemment, chez Framasoft, la seule condition pour se sentir bien c'est d'avoir un sens de l'auto-dérision bien ancré ! Il a aussi expliqué que *si être perfectionniste c'était important, cela devait rester dans un périmètre restreint* : pour soi-même ou seulement entre membres. Je le rejoins là-dessus, il n'y a absolument *aucun intérêt à forcer ou rendre honteux·ses* des personnes qui ont pour seule volonté de bien faire ou de mieux faire (ou pas d'ailleurs).

En résumé, framasoft c'est une bande de potes qui boit des bières ou du thé en se retrouvant, qui [prévoit de conquérir le monde](http://www.dailymotion.com/video/x2qxb1i) en sachant pertinemment qu'iels n'y arriveront pas, et qui prévoit d'envoyer des chatons pour motiver les contributeur·ice·s :D

# Samedi soir : Une table ronde et des révélations

Pour cloturer la première journée, une plénière-table ronde a été organisée, sur le thème : *La place du logiciel libre dans la société.* Si je ne connaissais pas grand monde sur scène, je peux néanmoins assurer que le monde des hommes blancs n'a pas manqué de représentants =/

Pour vous raconter un peu mieux ce qu'il s'est passé, j'ai fait une sélection des pépites entendues sur scène:

- *"Le logiciel libre, c'est de l'open-source & des valeurs éthiques et sociales"*

J'aime beaucoup cette définition de [@pyg](https://framapiaf.org/@pyg) qui citait [lunar](https://mastodon.potager.org/@lunar) en disant que si les drônes qui bombardent des enfants sont sous Linux, cela nous fait une belle jambe. Et je ne peux qu'aller dans son sens, les licences libres c'est bien, mais si les GAFAM (Google, Apple, Facebook, Amazon, Microsoft) font du logiciel ouvert, ça ne nous avance pas beaucoup !

- *"VLC, c'est bien car personne ne sait que c'est libre"*

Mais OUI !!! Pourquoi est-ce que VLC est si utilisé ? Car lorsque je veux lancer ma vidéo, que j'ai le choix entre windows media player, Quicktime et VLC, et que c'est VLC qui me permet de lancer ma vidéo sans me poser de questions, c'est lui que je choisis ! On envisage enfin de *parler des besoins des utilisateurs !*

- *"Est-ce que la rentabilité doit-être la mesure de toute chose ?"*

Nope, au cas où vous en douteriez ;)

- *"Il ne faut pas atteindre une masse critique du logiciel libre, il faut mettre en place un réseau d'initiatives"*

Effectivement, Framasoft ne cesse de répéter que même si ils trouvent un écho dans un grand nombre de personnes, ils se refusent à devenir "le Google du libre", c'est à dire à concentrer un maximum d'utilisateurs. Et je ne peux que les appuyer (vous aurez sans doute compris que j'apprécie pas mal framasoft !)

- *"Je veux que les gens aient une expérience positive du logiciel libre, donc je ne les force pas !"*

On en revient à *la bienveillance et à la volonté de laisser chacun aller à son rythme,* sans pression.

- *"Proposer un choix éclairé, et ne pas stigmatiser"*

Pour prolonger le point précédent, c'est en effet important que chacun sache quels sont les tenants et les aboutissants de ses actions, mais de ne pas vouloir propager la honte sur la personne qui ne serait pas "une bonne libriste". Après tout, c'est pour cela que l'enjeu est si important, il est vraiment difficile de se passer à 100% des GAFAM et autres logiciels propriétaires dans notre vie quotidienne.

- *"Il faudrait que les développeurs parlent aux utilisateurs"*

Ah bon ?? Je suis assez effarée de celle-là, qui explique tout de même à quel point il est important que les designers s'intègrent aux processus de création d'un logiciel libre : Déjà, on réalise qu'il *faudrait* s'intéresser aux utilisateurs et nous sommes en 2017 !
Ensuite, comme le disait [Natouille](https://mastodon.partipirate.org/@Natouille) il ne suffit pas qu'un développeur aille voir un utilisateur, car nous sommes tous sujets au biais de confirmation consistant à rechercher la personne qui nous donnera raison. Les UX designers sont habitués à y faire attention, et je ne crois pas qu'un dev, d'autant plus si il est très impliqué dans le développement de son projet, réussira à y être attentif (c'est aussi la force de faire entrer une personne extérieure). Bref, pas mal de travail à ce niveau là, mais ça donne des idées de conférences à donner !

# Samedi nuit : Mieux accueillir les contributeurs

Après la table ronde j'ai eu la chance de pouvoir assister à une conversation passionante entre [@pyg](https://framapiaf.org/@pyg), [@pouhiou](https://framapiaf.org/@Pouhiou) et [@Zatalyz](https://framapiaf.org/@Zatalyz) sur *l'importance d'accueillir à bras ouverts les contributeur·ice·s* en créant une personne "pom-pom girl" qui serait *une ressource pour aiguiller les bonnes volontés vers la meilleure façon pour eux de s'intégrer et de contribuer facilement selon ce qu'iels souhaitent.* Je dois avouer que l'idée me plait et même me rassure : Quand on veut contribuer et qu'on entends "Ben tu n'as qu'à trouver un projet qui t'intéresse", qu'on nous envoie chercher parmi la grande jungle des projets open-source, sans aucune connaissance des besoins ou de la place qu'on pourrait nous faire, *le résultat amène beaucoup de désarroi et un abandon de l'envie de contribuer* (C'est du vécu). Et lorsqu'on voit le besoin de contribution des projets libres, c'est quand même hallucinant !

# Dimanche matin : Privacy by design !

Après quelques heures de sommeil, c'est reparti pour une journée de libre !

Je me retrouve dans la conférence de [m4d_z](https://twitter.com/m4d_z) qui parlait de privacy by design au sens "conception" du terme, et qui s'adressait plus précisément aux personnes techniques pour leur expliquer comment elles pouvaient faire pour *gérer au mieux les données que leurs utilisateurs leur confiait.* La conférence a été très accessible néanmoins, j'ai appris ce que mettait en place le [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) (Réglement Général sur la Protection des Données) qui s'appliquera en mai 2018, mais aussi eu des billes afin de mieux concevoir en prenant en compte dès le début la vie privée des utilisateurs. Bien que je soit personnellement sensibilisée au sujet, une petite mise à jour ne fais pas de mal, je retiendrai donc qu'il faut :
- Penser à mettre *par défaut* et sans plug-in *une protection maximum*
- *Minimiser le traitement et l'utilisation des données*
- *Laisser le choix* aux utilisateurs de régler leurs préférences (en leur expliquant clairement et simplement les tenants et aboutissants de leur choix afin qu'ils aient *un choix éclairé*)
- *Récolter un minimum de données* (tout simplement !)

En résumé, il est important de *concevoir en gardant en tête la protection de la vie privée*, mais cela ne pourra se faire qu'*en expliquant à l'utilisateur les risques auxquels il est exposés lors de son utilisation d'un service.*

M4d_z a lui aussi [mis ses slides en ligne](http://talks.m4dz.net/privacy-by-design/) !


# Dimanche aprem : Contributopia, inspirer les possibles !

Comme je n'arrivais pas bien à saisir ce qu'était Contributopia, la nouvelle feuille de route de Framasoft, je me suis rendue à la conférence ["Contributopia, dégoogliser ne suffit pas"](https://asso.framasoft.org/nextcloud/index.php/s/3IO3XlxhBdmNFvW?path=%2Fcontributopia) de [@pyg](https://framapiaf.org/@pyg), qui avait promis une conférence dense… Il faut dire que nous avons été servi·e·s, pyg a tenu l'audience pendant plus d'1h40, en rapellant l'origine du combat de Framasoft, les différentes dominations des GAFAM (*Technique, Économique et Culturelle*), la notion de *capitalisme de surveillance puisque toutes nos actions sont traçables* (via notre smartphone, le paiement sans contact, bientôt la voiture…) ce qui a *un impact sur la portée de nos démocraties* puisqu'elle est laissée entre les mains d'acteurs privés via la manipulation par la pub, l'économie de l'attention et même les smarts city (j'apprends au passage qu'au Canada, un quartier entier est geré par Google… ça ne me plait pas beaucoup !)

Enfin, il revient sur Dégooglisons qui, malgré le nombre très impressionants de services lancés en 3 ans, n'est une réussite que sur le plan de l'open-source. *Les valeurs éthiques du monde du libre souffrent, et les contributeur·ice·s se font rares même sur des projets dont le succès est indiscutable.*

Contributopia est donc l'occasion de relancer la communauté sur un terrain différent afin de:
- Promouvoir *le web comme un commun*
- *Proposer des alternatives concrètes*
- *Mixer les métiers* (youpiiiiii \o/)
- Montrer *comment contribuer*
- *Rendre le libre plus accessible*

Autant vous dire que je suis *très enthousiaste* face à cette feuille de route, qui pourrait notamment inciter les non-techs / hommes blancs à contribuer plus facilement ! Décidémment, Framasoft fait preuve *d'un esprit d'ouverture* extrêmement agréable qui *m'incite personnellement à contribuer pour réaliser de belles choses !*

# Pour conclure

Ce Capitole Du Libre a été pour moi placé sous *le signe de la bienveillance et de l'écoute, afin de faciliter le rassemblement.* Je ne sais pas si c'est parceque j'ai passé une grande partie de mon temps avec les membres de Framasoft, mais il faut dire qu'il n'est que temps pour les techs libristes de laisser de la place dans leurs rangs à d'autres familles de métier !
On se rend compte qu'il reste du chemin puisque l'idée de faire *avec les utilisateurs* n'est pas encore une évidence… Mais cela m'enthousiasme aussi que je puisse contribuer en injectant à mon niveau un peu de cette culture de conception centrée utilisateur !

Merci à l'orga du Capitole du Libre pour ce moment ! *Prenez soin de vous, la route est longue mais la voie est libre !*
