---
layout: post
title: "Japon Jour 13"
subtitle: "Tradition, lapins & onsen"
date: 2018-05-30 15:00:00 +0200
vignette: null
intro: "Tradition, lapins & onsen"
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/japon_jour13/
---

La nuit a été dure, au sens premier, car les futons n'étaient pas bien épais et les tatamis bien denses et fermes. Je me sens comme une petite mamie au réveil !

Ce matin, c'est petit déjeuner du coté du vieux Kyoto ! Il parait qu'il y a un Starbuck intégré dans de vieux batiments (avec des tatamis au sol) et on a bien envie de voir ça. Pour rigoler, on dit que lorsqu'on arrivera au pied de la montagne, on y sera (Ha ha ha ha). On croise des canards qui pioncent sur l'eau… et encore de belles ruelles !

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_1.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_2.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_3.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_4.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_5.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_6.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_7.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_8.jpg" alt="">
</figure>

D'ailleurs si quelqu'un sait ce que c'est que ces tampons que je trouve sur toutes les maisons je suis preneuse !

Une fois que j'ai bu un frappuccino au Matcha (c'est une tuerie ce truc, je vais acheter du matcha rien que pour en refaire !!), nous nous dirigeons vers une pagode. En gros, aujourd'hui c'est circuit est de Kyoto, du Sud au Nord, pour arriver jusqu'au temple d'argent. La route est parsemée de temples bouddhistes et quelques sanctuaires shinto, ça tombe bien on a pas fait grand chose de bouddhiste jusqu'à présent !

La pagode donc, que nous avons vu de loin et qui ne nous… a pas émues ^.^ On passe donc notre chemin pour continuer notre route :)


<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_9.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_10.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_11.jpg" alt="">
</figure>

On sent bien que le quartier est touristique, mais on tombe quand même (enfin !) sur de jolis magasins avec de belles choses à voir. Bon on tombe sur un magasin Ghibli avec un beau robot du Chateau dans le ciel que j'ai très envie de reproduire dans mon jardin, ok ce n'est pas très traditionnel. Mais aussi, en entrant dans une boutique de vaisselle, sur un perroquet, à qui je dit Bonjour en japonais et qui me réponds !!!!!!! Bon il est ensuite parti à faire des cris stridents, mais je suis contente, c'est la première fois que j'interagis avec un perroquet et c'est vraiment fou de l'entendre me répondre "Konichiwa" !

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_12.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_13.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_14.jpg" alt="">
</figure>

Nous entrons dans un "parc" composé de temples (bouddhistes donc) et de sanctuaires shinto. Nous sommes alignées sur la non-envie de rentrer dans des temples pour voir des Buddha, mais plutôt de rester à profiter de la végétation ou de la spiritualité shinto.

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_15.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_16.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_17.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_18.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_19.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_20.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_21.jpg" alt="">
</figure>

Quitte à snober les temples bouddhistes, nous retraçons notre itinéraire pour voir les sanctuaires shintos, et justement, il y en a un immense sur la route !! Nous avançons tout droit quand soudian nous découvrons un immense Tori :

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_22.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_23.jpg" alt="">
</figure>

Voilà, en gros il y avait sobrement une 2x2 voies qui passait dessous ! Je suis exaltée de voir un Tori si immense et me demande bien de quand il date ! Nous nous dirigeons vers lui, et en passant près d'une boutique de poterie j'ai un coup de coeur monstrueux pour une série de 12 coupelles représentant les 12 animaux du zodiaque. Ils sont tous magnifiques et j'hésite longuement à les acheter, car comme ils sont peints à la main ils sont assez chers. Mais j'essaie de désencombrer mon chez-moi et il faut avouer que je n'ai pas l'utilité de ces coupelles. Je renonce à les acheter malgré leur beauté (et ne me suis pas permise de les prendre en photo).

Enfin bon le coeur en peine (au moins !) nous passons sous le Tori géant et découvrons un petit marché devant le temple ! Décidemment cette journée me ravit. J'y achète du thé pour mes desserts futurs ;)

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_24.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_25.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_26.jpg" alt="">
</figure>

Par contre, en rentrant dans le sanctuaire, désillusion. Il est immense en effet, mais le sol n'est recouvert que de graviers poussiéreux, et ça ne donne pas du tout (du tout) envie d'y rester. Par contre les toits et l'architecture sont magnifiques (et c'est rouge !) (j'aime bien le rouge).

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_27.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_28.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_29.jpg" alt="">
</figure>

Nous continuons notre route et tombons sur un tout petit sanctuaire, où se déroule un mariage. Nous allons voir et oh surprise, l'esprit de ce sanctuaire est représenté par un lapin (trop mignon !!!!!!). Du coup, il y a des lapins **partout**, ils sont trop chous !! Je prends mon courage à deux mains pour demander quels sont les aspirations du dieu-lapin, et la pretresse (?) me donne un papier où j'apprends qu'il veille sur la fertilité (youpi), le mariage (re-youpi) et la "bonne direction" (???). Après avoir cherché il apparait que "La bonne direction" serait vis à vis des directions annuelles de "l'énergie", et le dieu s'assurerait qu'elle ne nous soit pas défavorable (voilà en gros c'est un peu flou mais c'est contre la poisse de l'année).

De toute façon, l'important c'est qu'ils étaient trop mignons ces lapins !!!

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_30.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_31.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_32.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_33.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_34.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_35.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_36.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_37.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_38.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_39.jpg" alt="">
</figure>

J'accroche une prière (que j'ai écrite en japonais mais j'ai peur qu'elle soit dans un japonais si médiocre que les kamis me taperont sur les doigts). De toute façon je ramène une plaque pour l'accrocher devant chez moi, je ferai attention de faire une phrase plus cohérente avec internet sous la main !

Nous repartons pour atteindre le Chemin des philosophes, qui était notre but de la journée (qui est bien bien avancée !). C'est un petit chemin sympa mais on sent que c'est très touristique (aux nombreuses boutiques et aux prix des bouteilles dans les distributeurs !!). On remonte et arrivons jusqu'au temple d'argent qui venait juste de fermer ! Pas grave, de toute façon le temple des lapins c'était très sympa :3

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_40.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_41.jpg" alt="">
</figure>

J'aime bien cette plaque en forme de dent pour signifier que c'est un dentiste ! x)

Nous retournons manger chez Coco curry (qui va devenir notre chaine chouchou) et ensuite nous mettons en jambe pour aller à un Onsen (les bains publics japonais !!) \o/ J'avais hate d'y aller mais était toujours trop rincée le soir pour avoir le courage de ressortir.

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_13/japon_j13_42.jpg" alt="">
</figure>

Nous voilà en route, plus ou moins à l'aise avec l'idée de finir toutes nues les unes devant les autres. En gros, un onsen est un lieu qu'on peut qualifier de bains publics et qui pour moi se rapproche pas mal du concept de thermes (mais en tout petit). On rentre, on se répartit par genre, on se déshabille complètement dans des vestiaires communs, puis on se savonne/douche avant de rentrer dans des bains CHAUDS qui peuvent être de différentes natures.

Donc nous rentrons, enlevons nos chaussures, les rangeons dans un casier, comprenons qu'il faut acheter un ticket à la borne à l'entrée, allons remettre nos chaussures, nous rendons compte qu'il y a une borne à l'intérieur aussi… On donne le ticket à la personne, rentrons dans le vestiaire ou plusieurs japonaises sont déjà toutes nues. Ca met dans l'ambiance ! Du coup on ne se débine pas, on se déshabille et je trouve ça assez marrant en fait ! Ca me rapelle les cours de nus de la fac d'arts plastiques, en fait si le corps n'est pas érotisé il y a rien de problématique a être tout nu près de quelqu'un d'autre (surtout s'il l'est aussi, ça met sur un pied d'égalité.) Nous nous dirigeons vers les bains, nous lavons avec nos savonettes et une petite bassine (c'est comme dans les mangas c'est rigolo) puis je rentre dans le premier bain. C'est chaud, vraiment, vraiment chaud, mais j'ai trop envie de tester pour m'arrêter là. Une amie me signifie qu'elle sent comme des décharges dans son corps, et ça me fait rire, je lui explique que c'est sans doute ses muscles qui se détendent quand, soudain, je ressens aussi des picotements extrêmement désagréables sur ma peau. Je sors en vitesse du bain, que nous supposons à sortes d'impulsions électriques ?? Enfin en tout cas c'était très désagréable !!

Je rentre dans un second bain, vert !, tout aussi chaud, et c'est agréable. En même temps, j'ai la tête qui commence à tourner à cause de la chaleur. Je croise le regard d'une japonaise qui me souris gentiment, je crois qu'elle compatit x)

Je sors à grand peine du bain, me lave à l'eau froide, et tente un second bain, plus chaud, je rentre dedans mais n'y reste pas longtemps j'ai la tête qui tourne trop. J'en fais un 3e, à remous celui-là, et nettement plus froid (d'ailleurs celui par lequel les japonaises, entrées après nous, ont commencé, coincidence ?). Mais la chaleur m'est trop montée à la tête et il faut que je sorte ! La prochaine fois je ferai un tour des bains pour connaitre les moins chauds et commencer par ceux là, histoire de tester progressivement la chaleur et éviter le malaise !!

Enfin je sors du lieu, me place devant un ventilateur pour apprécier la fraicheur !! Puis c'est l'heure de se rhabiller et repartir, dommage car c'était vraiment sympa (mais impossible de tenir avec la chaleur).

Nous rentrons à la guesthouse, et c'est notre dernier jour à Kyoto, demain ce sera Nara et le temple d'Inari où j'espère trouver des statuettes des animaux du zodiaque !! Il y a prévu un peu de pluie mais ça va le faire ! A demain ! :)
