---
layout: post
title: "Pourquoi les logos des logiciels libres sont-ils si moches&nbsp;?"
subtitle: ""
date: 2018-08-28 20:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/pourquoi-les-logos-des-logiciels-libres-sont-ils-si-moches/
---

Ce matin, j'ai vu ce billet passer sur Mastodon&nbsp;: [Pourquoi les logos des logiciels libres et open source sont-ils si moches&nbsp;?](https://motherboard.vice.com/fr/article/wjk95n/pourquoi-les-logos-des-logiciels-libres-et-open-source-sont-ils-si-moches) publié sur le site Motherboard (que je ne connais ni d'Eve ni d'Adam, mes remarques porteront donc exclusivement sur le contenu de cet article). Le sujet étant une de mes marottes du moment, je l'ai lu et j'ai fini passablement énervée. Je vais donc profiter de cette énergie pour aborder le sujet et vous raconter pourquoi, d'après moi, les logos de logiciels libres et open source sont moches, et surtout pourquoi il faut arrêter de se trouver des excuses et commencer enfin à se bouger un peu pour changer ça.

# D'abord, l'article&nbsp;:

L'article cite 8 raisons, listées parfois avec sérieux, parfois avec humour, pour expliquer ce déficit de logos bien foutus. Reprenons-les ensemble&nbsp;:

## La licence libre ne paie pas

Elle ne paie pas non plus les développeurs qui contribuent il me semble. Cet argument m'agace profondément, car posé tel quel, il donne simplement l'impression que les designers et designeuses ne sont interessées que par le fric et pas par le fait de contribuer aux communs, ce qui est une absurdité (mais que j'ai déjà entendue, comme si notre proximité avec le marketing faisait de nous des assoiffé·e·s d'argent). En revanche, le métier de designer est bien moins plebiscité que celui de développeur, avec tous les inconvénients que cela amène&nbsp;: Nous sommes moins recherché·e·s par les boites, moins bien payé·e·s, et avons donc moins de temps libre pour *contribuer* par exemple. Si on creuse un peu, c'est donc un argument qui s'entend (mais le formuler différemment éviterait pas mal de quiproquos)

## Surprise&nbsp;: les développeurs ne sont pas graphistes
> Notons aussi qu’ils ont sans doute autre chose à faire&nbsp;: développer un logiciel correct demande beaucoup de temps et d'énergie.

Jusque là, chacun·e son taf, pas de problème avec ça (au contraire !)

## Plus grave&nbsp;: les développeurs n’aiment pas les gens

Et voilà mon point principal de contrariété.

*"Les Développeurs N'aiment Pas Les Gens"*

Comme c'est simple de ne pas aimer les gens. Ca évite de devoir s'adapter à de nouvelles personnes différentes. *En fait, ça évite de se remettre en question.* En même temps c'est inconfortable de changer, de se voir remis en cause, que se soit dans ses choix ou personnellement. Et je suis à l'aise avec le fait que vous n'ayez pas envie de le faire. Mais franchement, ça n'a rien à voir avec le fait de ne pas aimer les gens.

Soyons francs plutôt que de faire semblant&nbsp;: L'entre-soi c'est confortable, et je comprends qu'on n'ai pas envie de faire autrement. Arrêtons simplement les excuses à deux sous, qui contournent ce qui se passe et dispensent de se remettre en cause. Vous ne voulez pas d'autres types de personnes, de compétences ou que sais-je, soit. Mais c'est un choix qui est fait, pas une disposition préalable au métier de dev qui fait qu'il est impossible de cotoyer d'autres personnes, à cause de je ne sais quelle malédiction divine.

Après tout, il y a quand même un sacré regroupement de devs libristes, donc ne pas aimer les gens ça à l'air de ne fonctionner que pour les gens *différents*.

## DESIGN = CAPITALISME

> Parce qu'il est perçu comme un glissement vers une logique commerciale, le design inspire la méfiance chez les développeurs de FLOSS.

Si les produits propriétaires, alors qu'ils enferment les utilisateurs·ices, sont plus utilisés que leurs alternatives libres, c'est parce que leurs développeurs ont bien compris, eux, qu'un bon design (c'est à dire faire en sorte que le produit soit agréable à utiliser et à regarder), est nécessaire pour susciter l'adoption. Dommage qu'on en soit encore là.

## C'est la beauté intérieure qui compte

> Dans la mouvance FLOSS, tout effort graphique risque d’être perçu comme une distraction qui menace la fonctionnalité d’un programme.

Si il fallait encore trouver des explications du "Pourquoi on n'arrive pas à faire venir des designers", tout est cristallisé ici. Le design et le dev ne sont pas à opposer mais visiblement, cette querelle de clochers est encore d'actualité pour certains…

## Les développeurs sont rigolos

> « Les hackers adorent les blagues finaudes » explique Jose Marchesi, le créateur de GNU Recutils. « C’est tout naturellement que ce côté joueur se retrouve dans les logos. » […] Bon nombre de développeurs épris d'humour affublent leur programme d’un nom et d’un logo-jeu de mots de ce genre.

Je ne vois pas le rapport, on peut avoir un logo qui contribue à un jeu de mot et qui reste… un bon logo.

## C’est presque une tradition

> L’autre pilier du mouvement FLOSS, le noyau de système d’exploitation Linux de Linus Torvald, est représenté par un manchot dodu appelé Tux. L’animal, choisi par Torvald lui-même à l’aube des années 2000, a d’abord été considéré comme faible et disgrâcieux par une partie de la communauté. Les frondeurs, qui lui préférait des « bêtes nobles et fortes comme des requins ou des aigles », ont fini par déclencher la colère de Torvald.

Kamoulox (Je ne vois pas le rapport avec le schmilblick en tout cas)

## Peut-être que c’est fait exprès

> Certains événements pourraient faire croire que la communauté des FLOSS tient à rester dans son ghetto graphique. En 2017, The Document Foundation, l’organisation en charge de LibreOffice, a demandé aux internautes d’élire une mascotte pour le logiciel. Le vote a tourné à la catastrophe au deuxième tour.

Peut-être parce que… le processus de vote par une communauté n'est pas adapté pour prendre des décisions de design&nbsp;? Si vous aviez demandé à des designers, on vous aurait prévenu.

> Et si les mascottes, les logos et les interfaces moches étaient trop profondément inscrits dans les gènes des FLOSS pour être remplacés&nbsp;? Les logiciels libres et open source sont ainsi, un point c’est tout&nbsp;: autrement, ça ne cadrerait pas. « Je trouve que l'amateurisme de ces logos est presque rassurant et assez représentatif du logiciel libre (pas de budget, des gens passionnés qui sont experts dans leur domaine et qui se foutent de tout l'aspect image ou marketing) » remarque Gautier Gevrey.

Si le but c'est de produire un logiciel amateur, pas de problème. Mais alors allons-y franchement, proposons un logiciel bourré de bugs, histoire d'être cohérents.

> Il faut le reconnaître, la famille FLOSS compte quelques beautés « conformes ». Firefox, Ubuntu, Wordpress, et, heu... Les logos de LibreOffice et du mouvement open source lui-même ne sont pas si moches. Ces remarques sont subjectives, c'est vrai.

Un bon logo, ça n'a rien de subjectif. Mais pour le savoir, il faut avoir appris à analyser ce qui fait un bon logo et ce qui le distingue d'un mauvais.

# Je récapitule&nbsp;:

Apparemment, les développeurs aiment bien les logos moches, ça a tout le temps été comme ça et en même temps c'est marrant. De toute façon c'est l'intérieur qui compte, le design c'est juste un truc de marketeux pour vendre plus. Et puis le truc des devs, c'est de faire du code et pi c'est tout. Faire du design ils savent pas faire (et ont pas envie d'apprendre) et parler à d'autres gens (des gens *différents*) non plus. Et en plus il y a pas de sous pour payer les designers, donc c'est foutu d'avance.

# Soyons sérieux deux minutes

Vous trouvez ce portrait caricatural&nbsp;? Moi aussi et pourtant, plus j'essaie de contribuer au monde du libre, plus je me retrouve confrontée à cette façon de penser archaïque et dépassée.

Voilà donc une liste de choses à faire pour ne plus avoir de logos moches&nbsp;:

- *Trouvez des designers / graphistes qui ont envie de vous en faire un.*

Oui mais je trouve pas de designers / graphistes pour mon projet libre ! Comment faire&nbsp;?&nbsp;:

- Arrêtez d'opposer dev et design. Cela facilitera grandement la collaboration, vous verrez.

- Arrêtez de faire du graphisme / design alors que vous n'en avez pas les compétences. Vous avez quelquechose à faire qui nécessite ces compétences&nbsp;? Vous n'avez pas de designer / graphiste sous la main&nbsp;? *Débrouillez vous pour trouver des personnes compétentes et ne le faites pas à votre sauce.*

- Arrêtez de faire comme si c'était une malédiction. Cette absence de designers est symptomatique d'une absence d'ouverture à des personnes différentes, que se soit d'un point de vue identitaire ou des compétences, du monde du libre. Ne pas faire d'efforts pour sortir de l'entre-soi, c'est un choix. A vous de voir si vous choisissez de continuer dans cette voie ou si vous souhaitez faire différemment, mais arrêtez de vous trouver des excuses.

# Pour conclure

Cela ne fait pas longtemps que je mets de l'énergie à contribuer au monde du libre et je suis déjà éreintée par la quantité d'opposition que je rencontre chaque jour. J'en viens à penser que si les développeurs libristes ne se remettent pas en cause, il n'y a aucune chance qu'il soit intéressant de contribuer à leur projet, car cela serait *trop couteux en temps ou en énergie pour moi*. Cela explique sans doute pourquoi je ne cotoie quasiment que des développeurs qui, d'une façon ou d'une autre, remettent en cause leur façon de faire, que se soit vis à vis de la contribution ou sur d'autres aspects (consommation de viande, sexisme, LGBTphobie…).

Merci à eux.
