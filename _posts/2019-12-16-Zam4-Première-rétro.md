---
layout: post
title: "Zam 4: Première rétro"
subtitle: ""
date: 2019-12-16 15:00:00 +0200
vignette: null
intro: "Où je vous raconte ce qu'est une rétro, et à quel point ça a été simple à faire pour moi (nope)"
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam-premiere-retro/
---

# Première rétro

Où je vous raconte ce qu'est une rétro, et à quel point ça a été simple à faire pour moi (nope)

## C'est quoi une rétro ?

Rétro, le petit nom pour rétrospective, c'est une bonne pratique qui permet de faire un bilan après X temps, souvent une itération, de ce qu'il y a améliorer, conserver dans le fonctionnement d'une équipe… Sauf qu'à Zam on a pas vraiment d'itération, ou alors elles sont tellement courtes qu'on pourrait passer notre temps à faire des rétros si on en faisait à chaque fois !

J'en faisais déjà lorsque j'étais salariée, mais le contexte joue énormément sur ce qu'il peut se dire au sein d'une rétrospective. L'idéal, c'est de pouvoir tout se dire, mais selon les enjeux de chacun, ce n'est pas si facile…

## Comment j'ai abordé ma première rétro-Zam

Avec angoisse !

Pour plusieurs raisons :
- Déjà parce que j'ai rejoint une équipe qui avait l'air de bien fonctionner ensemble, et je me sens très en-dessous du niveau général : Iels comprennent vite les tenants et aboutissants de chaque nouveau problème, et j'ai souvent du mal à suivre, je demande de réexpliquer plusieurs fois (ou parfois n'ose pas demander…même si je me soigne !). Bref, je me sens à la traîne et j'ai peur qu'on me le reproche ou tout du moins que le sujet soit abordé.

- Mes propositions sont requestionnées et creusées, systématiquement, par les membres de l'équipe. C'est éreintant car, je réalise que je ne fais pas les choses parfaitement du premier coup (rien de surprenant finalement, mais la prise de conscience n'est jamais facile), et qu'en plus, les questionnements de ces personnes sont pertinents, ce qui est é-pui-sant car il faut sans cesse expliquer mes choix, et donc que je me souvienne de pourquoi j'ai fait tel ou tel choix (et parfois pas consciemment en plus !), et dans le même temps réussir à reconnaître quand il faut que je tienne sur mes positions, ou quand ma justification est bancale. Ca fait beaucoup de travail sur l'égo et sur mes compétences, de remise en cause… Ce n'est pas facile !

- Mes maquettes ne sont pas implémentées parfaitement, ce que je comprends complètement, cependant dans le flux et en travaillant à distance, je n'arrive pas à savoir comment faire pour, lorsque j'ai une liste de 15 détails à améliorer, les communiquer aux développeurs… Par écrit cela me semblerait vraiment trop fastidieux de tout détailler, et j'ai peur qu'on me trouve pointilleuse… Du coup je me tais, et ça crée de la frustration pour moi !

- Et enfin, je suis tentée de postuler sur un autre projet, ce qui signifierai ne plus pouvoir travailler avec l'équipe Zam… Le projet me tente énormément, mais je ne suis pas sure d'être prise, et je ne sais pas si il vaut mieux les mettre au courant, quitte à ce que se soit pour rien, ou le taire et prendre le risque qu'ils l'apprennent par une autre voie…

En résumé, je ne suis pas tout à fait sereine, mais je me dis que c'est aussi le jeu de la rétrospective, pour m'améliorer et pour améliorer le travail d'équipe. Alors j'y vais !

## Le déroulé de la rétrospective

Comme j'ai besoin de me sentir à l'aise, je me cale en tailleur sur mon lit, et mon chat me rejoint sur mes genoux. Parfait, au moins j'ai un peu de soutien moral, se sera plus simple.

Tout le monde se connecte en visio, et Raphaël nous propose une rétrospective [au format Conseil](http://ut7.fr/blog/2015/11/18/animer-vos-retrospectives-avec-le-conseil.html).

On commence en se donnant, dès qu'on le souhaite, **des appréciations** (sous la forme "Prénom + Je t'apprécie pour *chose concrète que tu as faite*"). Un peu plus tard, on rajoute la possibilité de formuler **des plaintes ou craintes**, accompagnées de **recommandations pour soi-même**. On rajoute ensuite **les mystères** (choses pas claires), **les rumeurs** qui sont sensés être complètement fausses, **les infos** et pour finir **les souhaits et espoirs** (formulés positivement).

## Ce qui s'est passé pour moi

Déjà j'ai vu que tout le monde arrivait à la rétro et avait l'air sûr que se serait un moment chouette. Moi qui essayait désespéremment de me détendre, ça m'a fait lâcher pas mal de stress que j'avais sur les épaules !

La rétro a ensuite démarrée, et j'ai envie de vous détailler 3 grands moments pour moi :

### L'appréciation qui touche juste

Comme énoncé au début, j'étais assez stressée à l'idée que l'un des membres de l'équipe me fasse remarquer que je ne m'étais pas faite au rythme du projet assez rapidement (pour ne pas les ralentir).

Bref, au début de la rétro, je ne sais plus bien pourquoi, mais je sais que l'un des développeurs (David ou Ronan donc) m'a dit, à un moment, qu'il était très content de la façon dont ça se passait niveau design.

Ca m'a soulagée d'un poids ! J'avais tellement l'impression de ralentir l'équipe et que mon travail était remis en question que j'avais du mal à me sentir robuste et utile au sein d'une équipe si chouette !

J'ai compris à ce moment là que les questionnements vis à vis de mes maquettes étaient des interrogations sincères, dans le but de bien vérifier que le produit allait être amélioré. Or, dans mes maquettes, surtout dans les premières, j'ai changé / remis en place beaucoup de choses, ce qui génèrait forcément beaucoup de questions. Mais j'étais tellement habituée à être confrontée à des personnes qui n'avait pour but que leur égo personnel, sans prendre en compte ma démarche, que j'étais par défaut pas mal sur la défensive.

Ca m'a permise de faire tomber la garde, et c'était vraiment bienvenu. Tout le reste de la rétro s'est passé de façon encore plus détendue pour moi !

### La remarque qui secoue

Reguaillardie par ce compliment, j'ai osé prendre la parole pour aborder le sujet de "Comment je fais parce que j'ai 40 mini-trucs à vous faire corriger et je me sens dépassée". Rien que d'y repenser, je souris car les réactions ont été tellement simples que je me suis sentie un peu… stupide de m'être tant montée la tête et de ne pas avoir osé en parler (mais c'est le jeu que voulez-vous !).

L'échange, très court, pourrait se résumer à une phrase qui a été "Il faut nous dire les choses et on les corrigera… Mais c'est sur que si on ne le sait pas on ne peut pas les changer :)".

Et bim… J'avoue que cette remarque m'a d'abord un peu vexée ("Oui ben si il pense que c'est facile !"). Mais l'atmosphère était tellement pleine de gentillesse (la vraie, pas la "gentillesse travaillée de façade"), que je me suis efforcée de ne pas écouter mes propres peurs. Je me suis promis de leur en re-parler plus tard pour voir de quelle façon nous pourrions trouver une solution intéressante qui nous conviendrait à tou·te·s.

### La rumeur qui trahit…ah ben non

Pleine de confiance au fur et à mesure des échanges positifs de la rétro, je profite de ce qui semble être la fin pour annoncer, en "rumeur", que je pourrai candidater à un autre projet… qui demande à être à plein temps, bien que je compte négocier afin de pouvoir continuer à travailler avec l'équipe.

Et si je m'attendais à beaucoup de choses, plutôt de l'ordre du champ lexical de la déception, je n'attendais pas à "Maïtané, je souhaite que tu trouves une solution à ce dilemme qui te conviendra".

Je me suis sentie tellement acceptée et écoutée que tout le reste de mes peurs ont disparues à ce moment là. Le reste de l'équipe a opiné du chef autour de cette phrase, et j'ai pleinement ressenti que je pouvais faire confiance à cette équipe.

Ca a été un moment comme je n'en avais jamais vécu et je vous souhaite d'en vivre un identique dans votre vie professionnelle <3

## Et maintenant ?

La rétrospective terminée, je vous amène maintenant vers la solution que nous avons trouvée pour améliorer cette "liste de petits détails de design" : [Le pair !](/blog/zam5-le-pair/)
