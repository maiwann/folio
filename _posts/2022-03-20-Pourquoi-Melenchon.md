---
layout: post
title: "Pourquoi j'irai voter Jean-Luc Mélenchon"
subtitle: ""
date: 2022-03-20 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blog/pourquoi-melenchon/
---

Le 10 avril, j'irai voter Mélenchon.
J'irai parce que je suis usée de ce monde absurde, où sans cesse c'est l'argent et le pouvoir qui dictent leur loi.

J'irai parce que je crois que nous faisons face à un choix entre deux mondes.

Un monde qui [ferme des lits en temps de pandémie](https://www.bfmtv.com/sante/fermetures-de-lits-dans-les-hopitaux-qu-est-ce-qui-a-conduit-a-cette-situation_AV-202110290281.html), qui privilégie les riches et les puissants en [supprimant leurs impôts](https://www.20minutes.fr/economie/3148415-20211014-trois-ans-apres-suppression-isf-peine-montrer-effets-positifs-economie-francaise), qui méprise et [opprime les manifestants](https://reporterre.net/Face-aux-Gilets-jaunes-l-escalade-des-violences-policieres) en les violentant, qui plutôt que d'écouter la [Convention Citoyenne pour le Climat](https://www.149propositions.fr/) laisse [Total faire 14 milliards de bénéfices](https://www.francetvinfo.fr/economie/entreprises/totalenergies-annonce-un-benefice-gigantesque-de-14-milliards-d-euros-pour-2021_4953384.html) tandis que le litre d'essence monte à 2€ et qui ne prends pas de mesures face [aux 10 millions de pauvres du pays](https://www.20minutes.fr/societe/3142667-20211007-presidentielle-2022-10-millions-personnes-pauvres-france-comme-affirme-jean-luc-melenchon) (c'est **une personne sur 6 !!!**).

Et un monde qui donne la priorité à la justice, la [justice sociale](https://laec.fr/partie/4/humaniser-les-personnes-et-la-societe) ET [environnementale](https://laec.fr/chapitre/4/les-grands-defis-de-la-bifurcation-ecologique).

Parce que je ne pense pas que l'une puisse aller sans l'autre. C'est la [retraite à 60 ans](https://laec.fr/section/44/garantir-une-retraite-digne?q=retraites,retraite) au lieu de 65, [les professeurs](https://laec.fr/section/65/reconstruire-une-ecole-globale-pour-legalite-et-lemancipation?q=d%27indice,indice,point) et [soignants](https://laec.fr/section/32/faire-passer-la-sante-dabord-et-reconstruire-les-etablissements-de-sante-publics-notamment-les-hopitaux?q=soignants,soigner,soins) enfin augmentés, [le SMIC à 1400€](https://laec.fr/section/30/creer-un-etat-durgence-sociale?q=SMIC) pour qu'il n'y ai plus de travailleurs pauvres comme c'est le cas actuellement, [des logements disponibles](https://laec.fr/section/54/garantir-le-droit-au-logement?q=logement,log%C3%A9es) pour que les personnes sortent de la rue ou des appartements pourris dans lesquels ils ne peuvent pas se chauffer, [un revenu minimal pour que les étudiants](https://laec.fr/section/53/construire-lautonomie-des-jeunes?q=%C3%A9tudiants) puissent étudier sans avoir à travailler à coté et se loger et nourrir décemment, appliquer les idées de la [Convention Citoyenne pour le Climat](https://melenchon2022.fr/programme/comparateur/la-convention-citoyenne-pour-le-climat-et-lavenir-en-commun/)

Ce monde là, c'est celui porté par la gauche, par la France Insoumise, par Jean-Luc Mélenchon.


Le pire n'est pas à venir. Il est maintenant.

Ne pas oser aller manifester, choisir un emploi pour ne pas se retrouver à l'aide alimentaire, et ne même pas croire au fait d'avoir une retraite, c'est maintenant.

Je me bats depuis longtemps maintenant pour un numérique plus respectueux de nos libertés, un monde du travail plus respectueux des salariés et des précaires, et pour une société plus attentive aux violences faites aux femmes et aux enfants.

Je ne cesserai pas de le faire, mais j'étouffe sous le poids de ce travail, impacté jour après jour par les nouvelles mesures oppressantes prises contre nous.

Aidez-moi à respirer, aidez-nous à avoir une société qui prend soin de chacun·e d'entre nous, venez voter Mélenchon et parlez-en autour de vous !

Rendez-vous le 10 avril ♥️
