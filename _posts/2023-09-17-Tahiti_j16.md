---
layout: post
title: "Tahiti - jour 16"
subtitle:
date: 2023-09-17 09:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/tahiti_j16/
---

Aujourd'hui c'est une journé transit :
- On part de Tahaa en voilier pour aller à Raiatea
- à Raiatea on prend le ferry pour aller jusqu'à Tahiti
- à Tahiti, quelqu'un va chercher nos valises et la voiure pendant que d'autres attendent au port
- puis on prend le ferry jusqu'à Moorea
- et le taxi jusqu'au logement où l'on restera la semaine.

Le temps est beau, le dernier trajet en voilier est agréable, on part à 5h30 pour être à l'heure au ferry.

Je n'ai pas décoléré de la discussion d'il y a 2 jours sur le covid, et ça me bouffe. Je vais avoir besoin de revenir dessus pour clarifier les choses. Joie.

Sur le trajet en voilier, ma mère démarre une discussion. Elle s'est excusée de "ne pas m'avoir donné de tendresse quand j'étais petite". Elle poursuit en explicitant pourquoi (spoiler : ses parents) mais je l'arrête : je n'ai plus aucune capacité d'écoute, et je sais déjà tout ce qu'elle va me dire. Je lui précise que c'est aussi de ça dont j'ai besoin, écoute et reconnaissance, à la place de réponse type "Je ne suis pas d'accord" à des choses que je lui dis.

S'ensuit une grande discussion sur un exemple concret : le covid. J'explique, je détaille, le covid c'est grave, je ne veux pas l'attraper, on ne fait pas prendre de risques au gens, on ne vient pas les voir si on a croisé quelqu'un de positif, non on n'éradiquera pas le covid, oui il y a des réponses collectives, non on est pas parti pour les appliquer, ça ne signifie pas que je vais accepter de tomber malade tranquillement, donc il faut que les personnes autour de moi fassent un minimum attention. "J'avais pas pensé à tout ça" m'a t-elle répondu songeuse.

Voilà ce que ça donne des adultes qui n'écoutent pas : des "Je ne suis pas d'accord" qui te défoncent, et ensuite après un peu d'écoute, une réalisation qu'en fait ils n'ont pas compris ce que tu demandais.

On arrive au port, on prend le ferry (il y a plein de gens qui ont le t-shirt avec un logo de la compagnie, je me demande toujours pourquoi), on rentre à l'intérieur et j'ai pris pour la première fois un cachet magique pour le mal de mer car Josselin nous a dit qu'on allait galérer pour le retour. Cachet, comme annoncé, ultra efficace car pas une goutte de mal de mer malgré les impressionnants sauts du ferry.

Dans les salles, la télé commune diffuse une émission type plateau télé sur un sujet, en tahitien. On n'entends rien, de toute façon je ne parle pas la langue, mais je comprends que ça parle d'accès au foncier dans le coin. Les participants ont tous des tenues avec des motifs polynésiens, soit maori, soit de belles chemises ou robes à fleurs colorées. Quand je pense à nos costumes-cravates métropolitains, j'ai la flemme. A un moment je reconnais le député NUPES, le plus jeune de l'assemblée. Ca me fait plaisir :)

Une fois arrivés on attend sur le quai.

Je me sens hyper mal à ressasser comment expliquer de la façon la plus claire possible ma position sur le covid à l'amoureux de ma mère. Je n'ai pas l'espace d'expliquer à cet homme à quel point il n'y connait rien, à quel point il fait des approximations, à quel point il dit n'importe quoi. Je veux juste annoncer ma position, qui est non négociable.

Je réussis à la résumer en 2 points :
- Si on croise quelqu'un qui a le covid, la bonne chose à faire c'est de s'isoler
- Si on ne veut/peux pas s'isoler, le minimum c'est d'appeler les personnes que l'on va voir pour savoir comment elles souhaitent s'organiser pour se sentir suffisamment protégées.

J'ai eu le droit à plusieurs bribes de réponses :
- "Ben bien sûr si je vois quelqu'un de positif je vais pas voir des gens ensuite" (This is the MDR time puisque je rappelle qu'à Noël 2021 c'est précisément ce qui ne s'est PAS passé)
puis…
- "Moi je pense que c'est se mettre beaucoup de contraintes au lieu de se concentrer sur des problèmes plus importants"

Ce à quoi j'ai répondu "Je ne te demande pas d'être d'accord, ce que je dis c'est que si l'on se voit, ces 2 points sont non négociables. Sinon, on ne se voit pas."

Mais MERDE à la fin, passer un coup de fil à des personnes c'est trop compliqué ? Ces gens ne sont pas prêts pour le changement climatique -_-

Bref, le 2e passage en ferry se passe, on arrive à Moora, prend un taxi, taxi qui prend une néo-zélendaise en stop (cool) mais qui réalise à la fin de la course qu'il n'a pas clarifié le mode de (non)paiement avec elle (pas cool). Vous me direz ce que vous en pensez, pour moi si on prend quelqu'un en stop, on la fait pas payer, s'tout. Et il lui a été demandé de régler 1 tiers de la course, et j'ai pas trouvé ça cool, mais il m'a été répliqué que c'était au chauffeur de taxi de gérer le problème avant puisque c'est lui qui l'a prise en stop. Puisque ça râle dès que je donne mon avis, je vais arrêter de le donner :)

On va manger au restaurant en face qui est au bord de l'eau, on voit des poissons et même des requins pointes noires qui passent :) yeah :)

3 caresses à un chat, je rentre, un long appel avec l'amoureux (enfin, vive le wifi et l'életricité stable) où je m'étends sur les journées passées et leur infinie difficulté…Je pleure beaucoup bien sur.

Puis au dodo dans le nouveau lit, où des petites fourmis aiment bien faire un bout de trajet.
