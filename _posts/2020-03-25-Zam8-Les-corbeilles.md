---
layout: post
title: "Zam 8: Les corbeilles"
subtitle: ""
date: 2020-03-25 10:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam8-les-corbeilles/
---

# Les corbeilles

## Le besoin

De nombreuses demandes nous ont amené·es à repenser la conception au sein du répondeur de Zam, afin de remplir les attentes des utilisateurs. Les besoins prenaient différentes formes selon les personnes, mais je les résumerai comme ceci :

> Être rassuré·e quand au fait de ne pas "rater" la rédaction d'une réponse à un amendement (peu probable mais très problématique) ou sa relecture (d'avantage probable).

## Comment ont fait les autres ?

Zam n'est pas le premier à s'attaquer à la rédaction des amendements. Mais nous pensons que, si les autres ont échoués à réaliser un projet utilisé par tou·te·s, c'est notamment car ils se sont retrouvés bloqués sur la conception du "circuit de validation".

En effet, pour chaque projet de loi, l'organisation des validations (c'est à dire qu'un responsable dise "Ok je trouve que la réponse de cet amendement est correctement rédigée") est différente. Il n'y a pas le même nombre de niveaux de validation, pas les mêmes boucles de validation…

Vouloir reproduire ce circuit  :
- ne peut pas être fait de façon "automatique" (en imaginant par exemple rentrer dans la machine un organigramme "modèle" de validation qui serait suivi systématiquement),
- demanderait beaucoup de temps et une connaissance très aboutie des différents organigrammes aux coordinateurs, si on imagine leur proposer un outil leur demandant d'insérer à la main dans Zam l'organigramme de validation de chaque nouveau projet de loi (Au secours !! x.x)

Car, selon les bureaux, le nombre de validations peut changer. Mais il faut aussi penser qu'il peut y avoir des projets de loi inter-ministériels, ce qui multiplie d'autant les façons de faire différentes, et donc les possibilités qu'un organigramme inséré dans Zam ne soit pas pertinent.

Bref, réaliser un circuit de validation strict dans Zam s'annonçait long et fastidieux, aussi bien pour les utilisateurs que pour nous les concepteurs. Nous n'avions pas envie d'aller dans ce sens là, ni pour la complexité rajoutée, ni pour le fait que cela mettrait au centre de l'outil une organisation rigidifiée, alors que nous avions surtout envie de permettre la souplesse et l'auto-organisation des agents, pour qu'ils adaptent l'outil à leurs besoins.

## Alors, comment on fait ? : Les corbeilles

Après en avoir parlé plusieurs fois entre nous, nous avons commencé à esquisser le concept de "corbeilles", comme des corbeilles accueillant des dossiers papier, dans lesquelles chacun·e déposerait des amendements. Il pourrait par exemple y avoir une corbeille étiquettée "Bureau 5B", une seconde "Bureau 5A", "En attente de validation"…

Et de ce fait, chaque utilisateur pourrait se connecter et demander à voir une corbeille donnée. Cette souplesse permettrait également que, si l'un·e des responsables est absente lors de la rédaction du projet de loi, sa collègue puisse prendre la main sur les amendements qu'on aurait déposé dans sa corbeille !

Une fois qu'on en a discuté plusieurs fois, j'ai l'idée dans ma tête, bien à plat (enfin c'est ce que je crois), et je commence donc à maquetter le tout !

<figure>
  <img src="/img/posts/2019_Zam/08/zam_1.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/08/zam_2.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/08/zam_3.png" alt="">
</figure>

Vous pouvez voir que le format change ! Les amendements sont sous forme de blocs, ce qui me permet plus facilement d'y caler un objet un peu long par exemple.

Ca fait plaisir de concevoir une nouvelle façon de faire. Une fois mes maquettes assez avancées, je les montre à l'équipe pour ne pas passer trop de temps sur des détails qui pourraient être remis en question.

## L'équipe et les maquettes

On s'appelle et je partage mon écran. Je fais un tour des maquettes en expliquant les différents comportements attendus. Après mon tour de présentation, les questions commencent à arriver :

- Il faut permettre à un utilisateur de naviguer entre les corbeilles, comment l'indiquer aisément ?
- Certains amendements dépendent de plusieurs bureaux. Est-ce qu'on peut les mettre dans plusieurs corbeilles ?
- Que se passe-t-il si personne ne surveille une corbeille ? Est-ce que les amendements vont s'y entasser ?
- Qu'est-ce qui se passe si quelqu'un change un amendement de corbeille sans prévenir ?

Les questions sont nombreuses et nous avons la sensation, compliquée à admettre, surtout pour moi puisque j'ai passé du temps sur les maquettes, que le nouveau concept complexifie le logiciel plus qu'autre chose… A force de poser des questions, nous réalisons que nous avons besoin d'approfondir le sujet à fond, en temps réel, et que le mieux pour ça c'est de se retrouver pour un atelier de conception.

Nous nous organisons donc pour nous voir sur Paris, à la boutique, les locaux d'ut7 où travaille Raphaël. David ne sera pas présent physiquement car le trajet Montréal-Paris n'était pas trop trop envisageable :p Mais nous ferons en sorte de l'appeler lorsque le fuseau horaire le permettra.

## La suite !

Je vous donne rendez-vous pour le prochain article qui parlera de cet [atelier de conception :)](/blog/zam9-les-zamettes/)



## Les prochains articles :

- [Atelier de conception / Les zamettes](/blog/zam9-les-zamettes/)
- [Nouvelle version : Les tables !](/blog/zam10-les-tables/)
- [Le rôle de Mélo & les démos](/blog/zam11-melo-et-les-demos/)
- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
