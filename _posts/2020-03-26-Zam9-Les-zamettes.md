---
layout: post
title: "Zam 9: Atelier de conception · Les zamettes"
subtitle: ""
date: 2020-03-26 10:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam9-les-zamettes/
---

# Zam 9: Atelier de conception · Les zamettes

Après avoir réalisé que le concept des corbeilles n'était pas complètement au point, nous avons programmé un atelier pour avancer sur le sujet…

Je prends mon sac à dos et je me déplace à Paris pour retrouver Raphaël, Mélo et Ronan, à la boutique d'ut7. J'adore cet endroit et ces personnes, je suis donc super motivée pour que l'on trouve ensemble la solution à :

> Mais comment est-ce qu'on conçoit quelquechose de simple à utiliser pour permettre aux utilisateur·ices d'être rassuré·e quand au fait de ne pas "rater" la rédaction (peu probable) ou la relecture (d'avantage probable) d'une réponse à un amendement.

On commence la journée en listant les sujets à traiter. On veut partir en :

- Sachant quel truc on veut concevoir
- Ayant les idées claires sur le 1er bout à implémenter
- Partageant la roadmap du futur de Zam

<figure>
  <img src="/img/posts/2019_Zam/09/0.jpg" alt="">
</figure>

La roadmap a déjà été travaillée, Mélo nous la présente (à Ronan et moi) pour que nous visualisions tous quelles sont les prochaines grosses étapes : Après le flux de validation, nous nous attaquerons certainement à l'authentification, mais on y est pas encore !

## Débuter la conception : Les questions

On se pose un moment pour mettre à plat toutes les nouvelles questions que les maquettes ont fait remonter. On en profite pour renommer les "corbeilles" en "zamettes" histoire de laisser libre cours à notre imagination sur ce concept.

On liste :
- Une personne travaille dans un seul "bureau" au sens de l'organigramme ? Ou plusieurs ?
- Quand plusieurs personnes ont leur avis a donner sur un amendement, comment on gère les affectations à ces personnes ?
- Quand je suis connecté, est-ce que je vois par défaut ce qui se passe dans une zamette ? Dans plusieurs ? Lesquelles ? Est-ce que c'est quelque-chose de prédéfini ou un abonnement que je réalise consciemment ?
- Qui crée les zamettes ?
- Un amendement peut-il être dans plusieurs zamettes ?

<figure>
  <img src="/img/posts/2019_Zam/09/1.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/09/2.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/09/3.png" alt="">
</figure>

C'est plus sympa avec les schémas de Ronan !

Au fur et à mesure des discussions, on retrace les enchainements d'actions, mais tout n'est pas encore clair…



## Ensuite, ce qu'on garde

On commence tout de même à tirer des concepts généraux :

On a 2 types de comportement :
- Le premier, surnommé "Tinder" : On prends un amendement, on le traite, on le fait passer au suivant et on n'a plus besoin de le voir. C'est le rôle des rédacteurs et de leurs valideurs.
- Le second, surnommé "Kanban" : Le besoin de surveiller l'avancement de l'ensemble des amendements. C'est le rôle des coordinateurs.

<figure>
  <img src="/img/posts/2019_Zam/09/4.jpg" alt="">
</figure>

On se concentre sur ces concepts et on tente de faire simple :

- Un amendement ne peux être qu'à un endroit à la fois, dans une "zamette".
- Il n'y a qu'une zamette par personne. Chacun a sa zamette personnelle pour prendre ses amendements ou qu'on les lui dépose, comme sur un coin de bureau. C'est ce qui servira notre "concept Tinder" évoqué plus haut. Quand la personne a rédigé son amendement, elle le met dans la zamette de celui qui doit le relire, et ainsi de suite.
Exit le concept des zamettes pour plusieurs personnes, auxquelles il faudrait s'abonner… trop compliqué !
- Il existe un second endroit, qui, comme une sorte d'annuaire, liste les amendements et la zamette dans laquelle il se trouvent à un instant T, ainsi que son niveau de remplissage (avec un avis, avec un avis et une réponse…). C'est le "Kanban" évoqué au-dessus.

## Adieu Zamettes !

Plus tard, Mélo et Raphaël on réalisé ensemble un second atelier pour approfondir les concepts. Il en est ressorti que les "zamettes" s'appelleraient dorénavant des **tables**.

Chacun aura donc sa table personnelle, sur laquelle il sera possible de déposer des amendements. Une fois un amendement rédigé, on pourra le faire suivre à la personne en charge de relecture, et ainsi de suite…

Un amendement ne peut être qu'à un seul endroit à la fois, et ne peux être modifié que par la personne qui l'a sur sa table.

Par contre, on peut voler un amendement à quelqu'un, si on estime cela pertinent ! Cela évite les blocages éventuels pour les amendements qui ne seraient pas chez la bonne personne…


Il n'y a plus qu'à revoir les maquettes pour mettre en place ces concepts !


## Le déroulement de la journée

Je n'en ai pas parlé davantage, mais David nous a rejoint en début d'après-midi via feu appear.in (aujourd'hui whereby) pour participer. Il raconte très bien dans [son article de blog](https://larlet.fr/david/blog/2018/penser-distance/) comment on a réussi à se débrouiller pour qu'il puisse participer le plus possible malgré la distance et alors qu'on collait des post-it de partout !


Pour la suite, je vous invite à voir les maquettes dans le prochain article, afin de voir [les tables prendre forme !](/blog/zam10-les-tables/)


## Les prochains articles :

- [Nouvelle version : Les tables !](/blog/zam10-les-tables/)
- [Le rôle de Mélo & les démos](/blog/zam11-melo-et-les-demos/)
- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
