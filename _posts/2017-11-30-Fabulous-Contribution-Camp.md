---
layout: post
title: Fabulous Contribution Camp
subtitle: ""
date: 2017-11-30 13:20:00 +0200
vignette: null
intro: "Pyg le disait lors du Capitole du Libre, la majorité des projets de logiciel libre sont soutenu par très peu (voire pas) de contributeur·ice·s. C'est assez inquiétant, et en même temps pour avoir déjà échoué personnellement, parfois on a l'envie de contribuer et on y arrive pas. Ce qui est quand même assez comique lorsqu'on voit à quel point le logiciel libre a besoin encore et toujours de personnes volontaires !"
nom_blog: le blog
which_blog: blog_pro
permalink: /fabulous-contribution-camp/
---

Pour finir ce mois de novembre en beauté, j'ai été invitée par Framasoft à participer au Fabulous Contribution Camp qu'ils co-organisaient avec La Quadrature du Net. Je suis donc partie à Lyon pour rencontrer des contributeur·ice·s de tous horizons (et pas seulement de la technique !) pour parler des outils et de comment aider celleux qui le souhaitent à contribuer plus simplement.

# Un petit état des lieux

Pyg le disait lors du Capitole du Libre (dans la conférence Contributopia que vous pouvez [retrouver ici](https://asso.framasoft.org/nextcloud/index.php/s/3IO3XlxhBdmNFvW?path=%2Fcontributopia)), la majorité des projets de logiciel libre sont soutenu par très peu (voire pas) de contributeur·ice·s. L'exemple le plus marquant est celui des pads (un alternative à GoogleDoc permettant d'écrire collaborativement en ligne): Sur l'année dernière, il n'y a pas une seule personne ayant réalisé plus de 50 contributions, alors que le logiciel est très populaire !

C'est assez inquiétant, et en même temps pour avoir déjà échoué personnellement, parfois *On a l'envie de contribuer et on y arrive pas*. Ce qui est quand même assez comique lorsqu'on voit à quel point le logiciel libre a besoin encore et toujours de personnes volontaires !

J'avais donc hâte de me joindre à des groupes de réflexion sur le sujet afin d'identifier les problématiques et envisager des pistes de solution tout au long du FCC. Autant vous dire que tout le monde s'est transformé·e·s en designer pour concevoir *une meilleure expérience contributeur·ice* (Peut-on envisager de dire des CX Designer qui au lieu de signifier Customer eXperience sera notre Contributor eXperience Designer ? ^.^ )

# FCC c'est parti !

Le FCC avait lieu à [Grrrnd Zero](https://www.grrrndzero.org/), un lieu au caractère très affirmé, où les murs sont recouverts d'illustrations, près d'un immense chantier participatif destiné à devenir à la fois salle de concert, d'enregistrement et atelier de bricolage/dessin… Nous nous sommes fait accueillir avec un petit déjeuner fabuleux, cette première journée démarrait vraiment bien !

<figure>
  <img src="/img/posts/2017_11_30_FCC/petit_dej.jpg" alt="">
</figure>


Après un icebreaker rigolo nous demandant de nous regrouper d'abord par provenance géographique (mais comment font les nomades ?), ensuite par coiffure, ensuite par système d'exploitation préféré (oui on était un peu chez les geeks quand même) et enfin par notre engagement associatif principal, nous nous sommes lancés sur des ateliers collaboratifs, animés par les enthousiastes [Meli](https://twitter.com/melo_meli) et [lunar](https://mastodon.potager.org/@lunar) !

## Failosophy : Identifier les problèmes

Pour démarrer, nous nous sommes regroupés par 4 et avons raconté chacun une expérience marquante où *notre envie de contribuer s'est soldée par un fail.* Nous notions les différentes problématiques rencontrées sur des post-it avant de toutes les regrouper sur un grand mur pour avoir une vue d'ensemble des cas les plus fréquents. Ensuite, nous avons collaborativement trié les post-it ce qui prend "étrangement toujours 8 minutes peu importe le nombre de post-its ou de personne" d'après lunar… Il faut dire qu'il avait raison ;)

De grandes catégories regroupent les post-its et font apparaître des motifs récurrents. Ceux que je retiens (car ils me font écho) :
- La difficulté d'identifier un interlocuteur référent pour s'intégrer au projet
- La peur de demander "Comment faire"
- La peur de ne pas être assez doué·e / de dire des "choses bêtes"
- Ne pas savoir par où commencer
- La peur de ne pas s'intégrer à la communauté
- Le manque de temps
- La difficulté de déléguer à d'autres un projet qu'on a toujours porté
- Le fait de ne pas avoir réussi à créer une communauté autour d'un projet
- Ne pas connaitre la ligne de route d'un projet
- Ne pas savoir comment exprimer les besoins de contribution pour que les intéressé·e·s les comprennent

J'ai la sensation qu'un grand nombre de ces problèmes peuvent être résolus en:
- Listant clairement les besoins de contributions, avec les compétences nécessaires, via une interface plus facile d'accès qu'un github (qui est réellement anxiogène pour les non-développeur·euse·s)
- Désignant une personne accueillante comme point d'entrée pour les nouveaux·lles arrivant·e·s, à laquelle il sera facile de s'adresser, et qui saura aiguiller chacun·e vers des tâches simples pour découvrir le projet.

Cela se recoupe avec les discussions du Capitole du Libre, ce qui donne la sensation que nous sommes sur la bonne voie et que les hypothèses que j'ai entendues la semaine dernière sont des solutions à des problèmes récurrents pour les contributeur·ice·s !

## Étude de cas : 6 projets appellant à la contribution

Dans la phase suivante, pour s'imprégner un peu de ce qui se fait parmis les projets nécessitant contribution, nous avons eu le droit à la présentation de:

- Exodus, une plateforme qui analyse les applications Android et liste les logiciels traquant notre activité qu'elle y trouve.

- La Revue de Presse de la Quadrature du Net, permettant de soumettre des articles à la revue de presse de façon collaborative.

- Diaspora, un réseau social décentralisé assez proche de la configuration initiale de facebook.

- FAI Maison, un fournisseur d'accès associatif et nantais, qui propose le prix libre (ou contribution consciente)

- Le PiPhone, une plateforme de La Quadrature du Net permettant d'avoir accès à des numéros de députés afin de les appeler pour les encourager à soutenir (ou non) une loi, en proposant un set d'arguments et un retour d'expérience après l'appel.

- FramaForm, une des alternative de [Dégooglisons Internet](http://degooglisons-internet.org/) lancée par Framasoft, que Pyg a développé seul (!) en 15 jours (!!!). Avec le souci notamment que le logiciel permet plein de choses différentes mais que les options sont difficiles à trouver.

Le tour d'horizon permet de voir que si chaque projet recherche à fédérer une communauté de contribution, ils sont tous très différents et n'intéressent sans doute pas du tout les même personnes. *Les présentations étaient elles aussi très variées selon la personne,* de celui qui démarre par l'aspect technique à celui qui met tout de suite en avant les problématiques d'ergonomie. Bien sur, la présentation varie énormément selon les affinités et les problématiques rencontrées par chacun·e. Peut-être qu'avoir une description homogène (qui ne parte dans l'aspect technique que face à des personnes orientées technique) puis des pistes selon les compétences / envies de la personne en face serait à envisager ? (c'est un point que je laisse en suspens, mais il faut avouer qu'à force je suis rebutée par ceux qui m'expliquent immédiatement en quelle technologie est leur application alors que je suis très interessée par celleux qui ont des problématiques de conception ;) )

## Icebreaker rigolo

Petite recette: Prenez une feuille et écrivez en gros au marqueur ce dont vous avez envie de parler / un truc à apprendre à quelqu'un, montrez la feuille à tout le monde et formez un binôme avec une personne qui est interessé·e par votre thème et a un thème qui vous intéresse (ou pas) !


## Grodébat I

Après manger (des lasagnes végétariennes à tomber <3) nous avons repris certaines catégories de problématiques listées le matin et nous sommes séparés en petits groupes pour déterminer:

- C'est quoi le problème ?
- Dans l'idéal, avec une baguette magique, qu'est-ce qu'on fait ?
- Quelles solutions concrètes pour y arriver ?

Nous sommes arrivés à tout un tas de solutions pour nous mettre le pied à l'étrier:

### Vision :
Créer un manifeste et créer des rencontres mêlant communauté de contributeur·ice·s et utilisateur·ice·s

### Accès au savoir & Appropriation :
Formuler des avantages concurrentiels pour les marchés publics et rendre transparents les logiciels utilisés dans le service public

### Sortir de l'entre-soi :
Trouver des métaphores pour expliquer sans être technique, lister les communautés avec des objectifs similaires mais des besoins différents.

### Répartition des tâches :
Lister l'ensemble des tâches à faire et leur avancement.

### Expression des besoins :
Lister & Publier les besoins de contribution en petites tâches, Créer un bouton "J'ai un problème" qui s'adresse à un humain (facilitateur ou UX designer)

### Rapports de pouvoir :
Mettre et afficher un système de résolution des conflits.

### Gestion du temps (et de l'argent) :
Mesurer le temps mis pour une tâche pour le prioriser dans le futur et compléter le financement par le don.

### Accueil :
Avoir un·e référent·e du projet, co-rédiger une charte d'accueil "Comment accueillir les personnes".

Toutes ces solutions qui sont autant de petits pas à mettre en oeuvre m'ont beaucoup enthousiasmée : *j'ai l'impression que le chemin est tout tracé pour réaliser de belles choses ensemble !* Nous avons d'ailleurs enchaîné avec de petits groupes de travail concrets permettant de débuter la rédaction d'une charte, designer un bouton "J'ai un problème" ou encore réfléchir à une plateforme listant les besoins de contribution.

<figure>
  <img src="/img/posts/2017_11_30_FCC/grodebat.jpg" alt="Deux post-it: Créer un bouton J'ai un problème qui s'adresse à un humain, UX Designer ou Animateur, et lister / publier les besoins de contribution en petites tâches">
  <figcaption> Simple et à portée très efficace (on espère) </figcaption>
</figure>

Tout ça en une seule journée autant vous dire que nous étions lessivé·e·s à la fin, mais après un bon repas (préparé par les cuistots de Grrrnd Zero) il était temps de se coucher pour être fraî·che·s le lendemain !


# C'est reparti pour un tour !

C'est avec une fraicheur toute relative que nous avons entamé la seconde journée du FCC. La fatigue est là, mais la bonne humeur et l'enthousiasme général donne vraiment envie de finir le week-end en beauté. Nous entamons un tour de météo avec pas mal de personne dans le brouillard du matin, mais très vite nous faisons chauffer les méninges collectivement.

## Contributeur·ice·s où êtes-vous ?

Nous démarrons par une séance de "Boule de neige" où nous sommes invité·e·s à *lister les endroits où, demain, nous pourrions aller pour parler du logiciel libre et trouver de nouvelles personnes pour contribuer !* Sur un autre tas, nous écrivons plutôt ce qui nous empêche de le faire actuellement.

Je liste personnellement mes précédentes écoles de design qui sont, à mon avis, des nids à designers motivé·e·s mais pour qui le monde du libre est inexistant (on a tendance à nous parler principalement de droit d'auteur et nous n'entendons jamais parler de licences libres). Pour y aller cependant, il me faut plus de connaissances sur les licences et une liste de projet dont ils pourraient s'emparer afin de commencer à contribuer rapidement.

A la fin nous collons tout nos post-it sur les murs et, comme a dit lunar: *"Si chacun ramène 3 personnes, la prochaine fois on est 300."*. Il n'y a plus qu'à !

<figure>
  <img src="/img/posts/2017_11_30_FCC/cherchons_du_monde.jpg" alt="">
  <figcaption> Ca en fait du monde ! </figcaption>
</figure>

## Gros Débat II : Le retour

Nous enchaînons sur des groupes de travail thématiques. Je rejoins celui qui aborde le sujet du financement, et y apprends sans surprise que les subventions demandent un investissement énorme (environ 1/3 du temps total de travail passé pour les demander) et que, si le fonctionnement grâce aux dons permet beaucoup plus de liberté, il est soumis à des règles strictes, notamment au fait qu'on ne peut pas donner de contrepartie en échange d'un don, qui doit être fait uniquement pour soutenir le travail passé et à venir de l'association (dans les grandes lignes).
Le monde des financements a l'air très nébuleux, ce qui nous fait envisager de rechercher des contributeur·ice·s s'y connaissant en financement comme l'on en rechercherait en design ou en admin sys !

Les autres groupes ont pour leur part envisagé :

### Accueil :
Montrer de l'intérêt aux personnes qui arrivent, lister les besoins afin qu'iels puissent s'en emparer.

### Discutons ensemble :
Diversifier les compétences en listant les besoins (les solutions se regroupent, chouette !), ne pas imposer des outils libres aux futurs utilisateur·ice·s mais partir de leurs problèmes plutôt (on dirait bien une démarche de conception centrée utilisateur·ice \o/ )

### Rencontrer les utilisateur·ice·s :
Avoir des relais au sein de l'association ou se greffer à une rencontre existante afin de faciliter les échanges, demander à des UX Designers de se joindre aux rencontres et motiver certains devs à venir voir les tests utilisateurs.

### Comment on garde le lien post-FCC :
Avoir une plateforme ou l'on puisse mettre photo, un article de blog (collaboratif ?) récapitulant le week-end, et au-delà, une plateforme listant les besoins de contribution (oui, encore, chouette !)


## La machine infernale !

Après le repas composé de pizzas maison (c'est vraiment sympa Grrrnd Zero), nous nous lançons dans la machine infernale: Une première personne se place et commence à enchainer un son et un geste, de façon répétitive. Une seconde personne la rejoint et va elle aussi réaliser son + geste, et chacun va à son tour rejoindre la machine infernale, en répétant indéfiniment son geste et son son jusqu'à ce que…la machine s'emballe ! C'était un petit jeu très chouette et revigorant après la phase digestive !

## Gros Débat III

Pour ce troisième groupe de réflexion, je rejoins pyg qui a envie de réunir des associations afin de les informer sur le logiciel libre, qui leur plait pour des raisons évidentes de prix, mais aussi de valeurs ! Cependant le numérique est un domaine assez vaste et pour des assos qui ne s'y connaissent pas, il y a besoin de faire de l'éducation aux usages et à ce que cela engendre d'utiliser tel ou telle logiciel ou application.

Le groupe fourmille d'idée, je retiens pêle-mêle:
- Il y a un besoin de sensibilisation, de démonstration des possibilités des logiciels libres, et d'accompagnement à la transition,
- Les associations ont besoin d'indépendance, de communiquer et de collaborer
- Les mettre en relation vis à vis de leurs besoins peut permettre une mise en commun de financement pour développer des logiciels ouverts et adaptés
- Il faut aller voir les assos pour savoir ce dont elles ont besoin et ce qu'elles recherchent
- Selon les retours, décider du format, plutôt conférence organisée ou Summer Camp très ouvert ?

Moi qui recherche à participer à des choses qui ont du sens, me voilà pile au bon endroit et j'en suis très heureuse ^.^

## Ca sent la fin !

Enfin, pour finaliser le FCC, nous avons 4 catégories de post-it, à remplir puis à afficher au mur:

- ❤️ Ce que nous avons fait ce week-end et qui nous a plu !
- 🌟 Ce que j'ai envie de faire dès demain pour contribuer.
- ➡ Ce que je fais dans les prochains mois.
- 😊 Ce que j'ai envie que quelqu'un d'autre ici fasse (et chacun pouvait récupérer un post-it de ce type pour s'y engager)

Voici les miens :

<figure>
  <img src="/img/posts/2017_11_30_FCC/post_it_bilan.JPG" alt="J'ai gagné plein de motivation, rencontré des personnes volontaires, parlé d'UX. Dans les prochains jours, je vais écrire un article de blog FCC, Rédiger des designs pattern sur la contribution dans le libre, choper les contacts. Dans les prochains mois, je vais aller chercher des designers et leur parler de libre, trouver une façon de gagner ma vie en contribuant, contribuer avec + d'UX pour les framatrucs et essaimer sur l'UX et les besoins utilisateurs. Je vais donner un coup de main pour élaborer l'agrégateur de besoin des assos">
  <figcaption> J'ai du pain sur la planche !! ^.^ </figcaption>
</figure>


# Merci le FCC !

Ce Fabulous Contribution Camp a été une très bonne expérience : J'ai pu parler UX en long, en large et en travers, à des personnes qui se sont montrées à l'écoute de ce que j'avais à leur raconter (et ça ça fait plaisir !). Les différentes solutions énoncées, avec des pistes concrètes de réalisation pour se lancer rapidement me donnent des ailes, et j'espère que cette motivation ne va pas retomber de sitôt !

Je vous tiendrai au courant de mes avancées dans le monde du libre et de la contribution, en tout cas je remercie Framasoft de m'avoir permis de venir partager un week-end à Lyon avec eux à parler et rêver du futur du numérique !

Pour finir, voici les sketchnotes de ce week-end collaboratif. Prenez soin de vous :)

<figure>
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_1.JPG" alt="">
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_2.JPG" alt="">
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_3.JPG" alt="">
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_4.JPG" alt="">
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_5.JPG" alt="">
  <img src="/img/posts/2017_11_30_FCC/2017_11_25_6.JPG" alt="">
</figure>
