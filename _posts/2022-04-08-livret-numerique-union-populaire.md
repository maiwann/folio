---
layout: post
title: "Analyse éclair du livret Numérique de l'Union populaire"
subtitle: ""
date: 2022-04-08 14:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/livret-numerique-union-pop/
---

Billet de blog reprenant un thread-analyse éclair du [livret "Numérique" de l'Union populaire](https://melenchon2022.fr/livrets-thematiques/numerique/)


Je suis ravie de voir la sortie du [livret de l'Union Populaire dédié au numérique.](https://melenchon2022.fr/livrets-thematiques/numerique/)

Analyse-éclair avec l'intersection de mon engagement contre les oppression systémiques et pour une éducation aux enjeux du numérique (à [@framasoft](https://framasoft.org/) notamment)


> Partie 1 : Équilibre difficile à trouver entre les possibilités extraordinaires du numériques et son accaparement par quelques grandes entreprises qui pose de nombreux problèmes

C'est bien expliqué, clair, réussi.
Sont mentionnés : les GAFAM, la surveillance de masse, l'intelligence artificielle sans être techno-béat, la transparence des algorithmes (vous savez ces algos qui favorisent la droite et cie), les 13 millions de français exclus du numérique, que du bon

Mon dada personnel, à l'intersection entre numérique et santé au travail : l'automatisation de tâches pénibles pour travailler moins.

Alors que le discours pour des ergonomes a été pendant des années de "ne pas aller sur les projets d'automatisation" parce que ça supprime des emplois. Mais qu'on les supprime ces emplois, qu'on soit pas payé·es à faire des trucs que des machines peuvent faire !!
MAIS qu'on en profite pour réquisitionner les profits, l'idée c'est qu'on travaille tou·te·s moins, pas + de profit pour les patrons ✊

Ah et y a même respect de la vie privée, nickel.

On passe au 2 :
> Notre projet : Le numérique en commun

Déjà y a "commun" ça démarre bien

Point 2 :
- **Assurer l’égalité d’accès au numérique :** Très bien, ça concerne quand même 1 français sur 5 (13 millions je me répète)
- **Garantir la liberté d’opinion et d’expression des citoyen⋅nes #CensureGAFAM :** Très bien pour éviter les suppressions de contenu des copines féministes parce que tu as écris "comment faire pour que les hommes arrête de violer"
- **Assurer notre souveraineté numérique et notre maîtrise de toute la chaîne de l’information : infrastructures, équipements, logiciels libres et données :** Allez ça parle de logiciel libre et d'arrêter de dépendre de logiciels proprios américains, on adore !
- **Protéger les travailleur·ses des transformations du monde du travail engendrées par le numérique et du contrôle permanent de leur activité par des algorithmes :** OUI les travailleur·euses du clic qui font de la modération puis doivent être arrêté·es parce que la violence du taf !!
- **Lutter contre la surconsommation numérique, en phase avec les objectifs de bifurcation écologique et de sobriété énergétique :** Yes qu'on arrête de parler des mails et qu'on parle un peu des écrans publicitaires géants qui prennent bcp + de ressources rares pour rien !!

On passe au point 3, mais pour l'instant y a que du bon !

> Nos propositions : un numérique au service de l’intérêt général humain

> 3.1 Un numérique émancipateur pour toutes et tous

Droit d'accès internet pour tou·te·s, mise en place de la fibre, neutralité du net.
Que du bon !

Hiiiii y a même une partie communs culturel avec une médiathèque de ce qui s'est élevé dans le domaine public ! ❤️

Ohlala accéder gratos aux articles de recherche MAIS C'EST OUI !!

Faire du numérique une opportunité pour la démocratie : Du libre, des données ouvertes, de la collaboration avec les citoyens, c'est oui (même si c'est vague vu la diversité des choses à faire, je comprends)

> Développer la médiation numérique et garantir les alternatives physiques :

Là c'est top : 1, épauler réellement les médiateurices du numérique pour pas que se soit des personnes peu formées avec un CDD de juste 2 ans qui récupèrent les pots cassés de la dématérialisation

2, faire en sorte que "si tu veux pas, ben tu fais pas" en ayant des guichets physiques !! Parce que si les gens veulent pas remplir leur fiche d'impôt par internet ILS ONT LE DROIT !

3, vite, 100% accessibilité

4, co-conception (là encore c'est vague mais bon mood)

La partie "Réguler les usages du numérique " c'est moins mon truc, même si la partie "Loot box" fait écho à des problématiques d'économie de l'attention donc je comprends l'idée

(Oh la vache mais c'est long ce truc je suis qu'à la moitié !)
(Y a du boulot !)

Donc !

> 3.2 Garantir la souveraineté numérique de la France

(a priori moi la souveraineté, je m'en fiche, juste qu'on sache ce qu'il y a à l'intérieur de nos logiciels et qu'on en dépende pas #interopérabilité)

Point GAFAM = tuyaux de la NSA, c'est bon ça !

Lutte contre les brevets, c'est toujours bien aussi :D

Avoir des pros en IA pour conseiller les décideurs, ça sera pas du luxe (et si on pouvait arrêter avec les bullshiteurs pour qui l'IA c'est une religion c'est encore mieux)

Stockage des données en France, notamment santé et éducation, excellent point !!

Soutenir les projets de "cloud" décentralisé, associatif, bon point aussi

Bon après y a des trucs je sais pas me positionner, genre :
- Le cloud souverain, bon on l'a déjà entendu celle là, quelle différence majeure avec OVH par exemple ? (ptet c'est le privé ?)

- Créer des centres de calcul haute performance régionaux : pareil pas d'avis

Une diplomatie française du numérique à rayonnement mondial : Ca j'ai pas trop d'avis SAUF : "Offrir l’asile et proposer la nationalité française aux lanceurs d’alerte Edward Snowden et Julian Assange"

OUI, @Snowden Welcome !

> 3.3 Protéger la vie privée,

là je vais avoir des choses à dire :
- Protéger la vie privée et empêcher la prédation des données personnelles
Chiffrement, stopper le profilage publicitaire (TROP BIEN), former les pros de santé a pas filer nos données (avec doctolib en monopole ça fera du bien !!), filer des moyens à la CNIL, que du bon !

Ensuite, ça parle régulation : Impôts (bonjour, vous payez, merci bien), loi anti-monopole, EXIGER L'INTEROPERABILITÉ (mais c'est le feu !!!!), et la fin des partenariats État GAFAM je dis OUI !
👏👏👏👏

> encourager le développement de filières d’excellence, comme dans le jeu vidéo et le logiciel libre.

Logiciel libre everywhere !

> Développer l’éducation au numérique ouvert

♥️

> Garantir les droits des travailleur·ses à face à la transformation numérique

Allez hop la partie santé au travail !
> Exiger que l’employeur fournisse le matériel nécessaire au télétravail

(parfait, oui les copains profs ça vous parle peut-être ?! )

> Garantir les droits des travailleur·ses du numérique

> Établir la présomption légale de salariat pour tou·tes les travailleur·ses de plateformes et auto-entrepreneur·es

= Deliveroo, Uber et cie, pas merci, et au revoir !

> Donner aux travailleur·ses des plateformes un droit de contestation et de révision des systèmes algorithmiques qui organisent leur activité.

Ca aussi trop bien, transparence des algorithmes again !

Et enfin : 3.5 la sobriété numérique :

Je vous met que les trucs qui m'enthousiasment (ou ceux qui seraient critiquables ++)

> Interdire les usages inutiles comme les panneaux publicitaires numériques

(on adore)

> Réguler les usages énergivores des serveurs, comme le minage des cryptomonnaies

Yes, merci aurevoir

> Agir sur les modèles économiques des GAFAM reposant sur la captation de l’attention et la consommation sans limite de médias (ex : interdiction de la lecture automatique par défaut des vidéos, transparence sur les algorithmes)

Mais OUI ! Ciao la captation d'attention

Y a une partie développer un numérique sobre,ça va dans le bon sens mais franchement je m'y connais pas assez sur si c'est réaliste ou pas
Y a la partie "référentiel d'éco-conception" où il faudra former en masse les personnes parceque juste appliquer un référentiel ça suffit pas

Développer la réparation et le réemploi :
Augmenter les durées de garanties, téléphones reconditionnés, structurer une filière du reconditionnement… tout ça c'est pas ma compétence direct mais ça me semble cool.

## 🔖 CONCLUSION
Le programme est ambitieux, rien qui me fait tiquer comme étant contre productif, quelques trucs dont je vois pas l'intérêt.

Beaucoup de choses qui seraient génialissimes !! Priorité au logiciel libre, transparence des algorithmes, travailleurs du numérique salariés et qui auraient voix au chapitre, aller vers une interopérabilité des GAFAM, tout en voulant diminuer l'économie de l'attention et la captation de données à des fins publicitaires, protéger les données de santé, former au numérique, accessibilité, inclusion…

Je ne peux que vous encourager à aller le lire, c'est pas très long et très clair : [https://melenchon2022.fr/livrets-thematiques/numerique/](https://melenchon2022.fr/livrets-thematiques/numerique/)

Moi ça me conforte : Dimanche ce sera [@JLMelenchon 🗳](https://melenchon2022.fr/)
