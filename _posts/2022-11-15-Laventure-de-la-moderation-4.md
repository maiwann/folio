---
layout: post
title: "Avantages et limites"
subtitle: "L'aventure de la modération : Chapitre 4"
date: 2022-11-15 13:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/laventuredelamoderation4/
---

Alors maintenant que je vous ai dit tout ça, faisons un petit bilan des avantages et surtout des limites de ce système !

Si vous avez raté les articles précédents, vous pouvez ls retrouver par ici :
- Chapitre 1 : [Un contexte compliqué](https://www.maiwann.net/blog/laventuredelamoderation1/)
- Chapitre 2 : [La découverte de la modération](https://www.maiwann.net/blog/laventuredelamoderation2/)
- Chapitre 3 : [La pratique de la modération](https://www.maiwann.net/blog/laventuredelamoderation3/)


## Les avantages

Je vais être courte parce que mon point n'est pas ici d'encenser Mastodon, donc je ne creuserai pas trop les avantages. Cependant je ne peux pas m'empêcher de remettre en avant deux points majeurs :

Indéniablement, [on bloque très vite les fachos.](https://mstdn.social/@nassiraelmoaddem/109298566075349373) C'est quand même, à mon avis, le grand intérêt, qui n'est pas assez connu au-delà de Mastodon. De ce fait nous avons une exigence collective très faible par rapport à la modération : Quand Twitter annonce qu'il ne peut pas faire mieux, on le croit sur parole alors qu'une alternative sans moyens d'envergure y arrive… Journalistes spécialisés si vous lisez ce message !

Ensuite, le fonctionnement est beaucoup plus démocratique car : ce sont des humains que vous choisissez qui font la modération. Et ça, ça change beaucoup de choses. Déjà parce qu'on est dans un fonctionnement beaucoup plus… artisanal de la modération, donc potentiellement avec plus de proximité, dans l'idéal on peut discuter / appeller à l'aide ses modérateurices… et même selon les endroits, proposer de participer à la modération et ça, ça compte pour se rendre compte du boulot que c'est, et participer aux décisions importantes !


## Les limites

Comme rien n'est parfait, et que nous sommes sur une alternative qui évolue tout le temps, quelques points qui restent des limites malgré le modèle anti-capitaliste de Mastodon :

### Temps de traitement d'un signalement

Il y a un difficile équilibre entre le temps de traitement d'un signalement et le temps d'entendre plusieurs avis du collectif de modération. Je pense qu'il est sain de prendre le temps, mais il y a des situations où l'un·e des modérateurices peut considérer qu'il y a "urgence" et dans ce cas, attendre 48h c'est beaucoup (pour protéger une personne par exemple). C'est un point que nous n'avons pas encore creusé à Framasoft (comment gérer le dilemme rapidité d'action VS disponibilité du collectif), mais que je me note de rajouter en discussion pour nos prochaines retrouvailles !

Encore une fois je parle là des cas difficiles à départager. Si il s'agit de pédopornographie, les salariés de Framasoft sont déjà habitués à devoir réagir rapidement pour supprimer le contenu, donc je ne traite pas ce sujet ici car il n'est pas spécifique.

### Beaucoup d'utilisateurices, beaucoup de problèmes

Framasoft est une association très connue, et nos utilisateurices nous choisissent souvent car iels ont confiance en nous (merci !)
Mais du coup, cela entraîne une responsabilité compliquée à gérer : Plus de monde chez nous, ça nous fait plus de travail, de modération notamment.

Aussi, dans un cadre plus large qui était celui de la [déframasoftisation d'Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/), nous avons fermé les inscriptions, ce qui nous a permis d'alléger la charge de notre coté.

Et comme tous les Mastodon sont interconnectés, il est possible d'aller s'inscrire ailleurs pour suivre ce qui se passe chez nous, donc c'est un fonctionnement technique qui nous permet de mieux vivre le travail de modération… youpi !

### Éviter la "spécialisation" des modérateurices

Lors de la mise en place d'une équipe de modération à Framasoft, il a fallu faire un petit temps de formation-découverte à l'interface de modération de Mastodon.

Or on a vu apparaître assez rapidement une "spécialisation" entre celleux qui "savaient" utiliser l'interface, et celleux qui n'osaient pas car ne "savaient pas" l'utiliser.

Pourtant il y a beaucoup de valeur à ce que la connaissance autour des outils circule auprès de tou·tes celleux que cela peut intéresser :
- Pour qu'il y ait de plus en plus de modérateurices, ce qui répartit le temps dédié à la modération,
- Pour que la discussion soit ouverte à des personnes qui n'ont pas la tête plongée dans la modération, ça permet d'entendre d'autres paroles, c'est appréciable,

Pour résoudre ce problème, nous avons organisé des visios de modération !
C'est une pratique que nous avons dans [ma coopérative](https://lechappeebelle.team/) : faire des tâches chiantes, c'est quand même bien plus sympa ensemble !
Alors quand l'un de nous disait "bon, là, il y a quand même beaucoup de signalements qui s'accumulent" je répondais parfois "ça vous dit on fait une visio pour les traiter ensemble ?!"

Nous n'avions pas besoin d'être beaucoup, à 2, 3 ou 4 c'était déjà bien plus sympa, même les contenus agressifs sont plus faciles à traiter quand les copains sont là pour faire des blagues ou râler avec toi ! Nous lancions un partage d'écran, idéalement d'une personne pas hyper à l'aise pour l'accompagner, et nous traitions les signalements.

Autre effet bénéfique : **la boucle de "je demande leur avis aux copaines, j'attends, je traite le signalement" est raccourcie** car nous pouvons collectivement débattre en direct du problème. C'est vraiment une façon très sympa de faire de la modération !

## Les "On va voir" et autres "On peut pas savoir"

Enfin, si tout cela est possible actuellement, une part de moi me demande si ce n'est pas dû au fait que Mastodon passe encore "sous les radars".
C'est un sentiment que j'ai tendance à minimiser vu que cela ne nous a pas empêché d'avoir des hordes de comptes d'extrêmes droites qui se ramenaient. Donc une part de moi pense que le réseau (qui a déjà 6 ans donc on est loin du petit truc tout nouveau qui a 6 mois) a un fonctionnement déjà résilient.
Et une autre partie de moi sait qu'elle n'est pas voyante, donc on verra bien dans le futur !

Par contre je voudrais insister sur le fait qu'on ne peut pas savoir. Tous les articles qui vous expliqueront que Mastodon ne peut pas fonctionner parce que *"intégrer un argument d'autorité sur le fait que ce n'est pas assez gros"* ne sont pas mieux au courant que les utilisateurices et administrateurices depuis des années. Et je n'ai pour l'instant vu aucun argument pertinent qui aurait tendance à montrer que le réseau et ses modérateurices ne peut pas supporter une taille critique.

Note : Rajouter une partie "Mastodon n'est pas safe par nature, évidemment" ?

## Comme d'hab, le secret : il faut prendre soin

Cette conclusion ne va étonner personne : La solution pour une bonne modération, c'est de prendre soin.

Prendre soin des utilisateurices en n'encourageant pas les discours haineux (techniquement et socialement), en ne les propageant pas (techniquement et socialement).

Prendre soin des structures qui proposent des petits bouts de réseaux en leur évitant d'avoir trop de pouvoir et donc trop de responsabilités sur les épaules (et trop de coûts).

Prendre soin des modérateurices en les soutenant via un collectif aimant et disponible pour leur faire des chocolats chauds et des câlins.

Cher·e modérateurice qui me lis, tu fais un boulot pas facile, et grâce à toi le réseau est plus beau chaque jour. Merci pour ton travail, j'espère que tu as un collectif qui te soutiens, et que se soit le cas ou non, pense à avoir sous la main le numéro d'un·e psychologue, au cas où un jour tu tombes sur quelque chose de vraiment difficile pour toi/vous.

Coeur sur vous <3

## Et pour finir ?

Je n'ai pas pu m'empêcher de conclure avec un billet dédié au contraste entre mon expérience de la modération et ce qui se passe sur Twitter. Voici donc un billet spécialement dédié à l'oiseau bleu : [Et Twitter alors ?](https://www.maiwann.net/blog/laventuredelamoderation5/)
