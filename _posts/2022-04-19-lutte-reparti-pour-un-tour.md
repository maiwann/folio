---
layout: post
title: "Lutte : C'est reparti pour un tour !"
subtitle: ""
date: 2022-04-19 10:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blog/lutte-reparti-pour-un-tour/
---

Puisqu'on est reparti·es pour un tour, j'ai tenté de trouver des choses facilement activables et d'autres très importantes à faire pour être plus robuste dans les temps qui s'annoncent :

En vrac :

- S'aimer et faire du lien autour de nous. Le capitalisme et le libéralisme essaient de détruire les liens entre les individus, ne leur donnons pas trop se plaisir.

- Préserver votre santé mentale et physique, priorité absolue. Prenez des arrêts de travail (si on vous en file pas, un nouveau médecin), cherchez un nouveau job, commencez une thérapie (une TCC par ex), éloignez vous des gens qui vous prennent de l'énergie absurdément

- reposez-vous, la lutte ça vient après sinon on va tou·te·s exploser en plein vol

- Si vous avez de l'argent, donnez =) Donnez aux assos qui construisent un à-coté ou qui luttent de façon acharnée contre l'existant

- Idem mais pour les médias, abonnez-vous si vous en avez les moyens à des médias alternatifs qui vont pouvoir nous donner une poche d'air (parce que Bolloré et cie vont pas arrêter d'acheter des médias). Moi je pense m'abonner à Les Jours en plus d'Arrêts sur Images et Médiapart qu'on a déjà (et ptet donner à blast aussi j'ai pas regardé) [Ajout du futur : Abonnée à blast !]

- Adhérez à des collectifs avec qui vous vous sentez connecté·es, syndiquez-vous (je viens d'ouvrir la page de [Solidaires Informatique](https://solidairesinformatique.org/)

- donnez aux assos des plus précarisés, anti-racistes, LGBTI+, anti-capitalistes, va falloir faire face 💪

- ptet qu'un premier truc ça peut être : trouvez vous un médecin généraliste cool, comme ça vous aurez de l'aide si dans les 5 prochaines années c'est dur, pour savoir où aller en confiance. Et pour avoir subi la défiance médicale, je sais que ça change tout d'avoir avec toi un·e allié·e qui prend sérieusement en compte ton état

- Idem si vous avez envie depuis des années de commencer une thérapie mais que vous osez pas, moi j'ai fait des progrès hallucinant grâce à l'ICV et je suis une grande fan de ce cabinet que j'ai découvert trop tard : [Centre Bertha Pappenhein, soins engagés pour les femmes et les enfants](https://centre-bertha-pappenheim.fr/) et qui détaille [par exemple l'ICV](https://centre-bertha-pappenheim.fr/2022/01/14/lintegration-du-cycle-de-la-vie/)

- Ca peut être le moment de chercher le nom d'un·e ou deux psys, voire d'appeler ou d'envoyer un mail pour demander un premier rdv 🤗 (on déteste tous le téléphone ! :p)

Courage <3 

Note : Je pense que c'est une bonne nouvelle qu'on soient aussi nombreuses, même les + vieux à avoir une gueule de bois électorale. Le vieux monde s'accroche parce qu'il agonise, mais nous on sait construire celui de demain !
