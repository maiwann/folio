---
layout: post
title: "Nomadisme et questionnement"
subtitle: ""
date: 2018-09-03 21:40:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: none
permalink: blog/nomadisme-et-questionnement/
---

> J'ai jeté pêle-mêle mes idées et mes angoisses sur mon clavier et j'en ai fait ce billet, à peine relu. Il cristallise pas mal de choses et si vous deviez y réagir, j'aurai vraiment besoin de franchise et de bienveillance.
Pour le contexte: je suis arrivée hier par le train de 22h16, c'est la fin de mon itinérance démarrée le 17 juillet et conclue le 02 septembre.

J'écoute du Maitre Gims et je me pose des questions

Drôle de journée

Le retour de l'itinérance / Le rendez-vous avec la psy / Se nourrir sans en avoir aucune envie / Retrouver le chat et ses inconvénients / Se lever le matin avec le spleen / Découvrir qu'on a mégenré quelqu'un et qu'on s'en est même pas rendue compte / Chercher de l'empathie sans le formuler et ne pas en trouver / Trouver de l'empathie alors qu'on ne s'y attendait pas / Prendre la mesure de sa culpabilité, énorme / Se poser des questions sur l'itinérance

C'était bien l'itinérance. Genre *vraiment* bien. Bouger. Découvrir des lieux. Des personnes qui me donnent accès à leur chez eux. Qui me parlent honnêtement. Qui me livrent de l'intimité. Et puis partir. Pour en retrouver d'autres. Encore et encore et encore.

Et finir l'itinérance avec une discussion qui crispe.
" Oui mais bon, le faire un peu, là, ça va… Mais bon les gens, ils vont pas non plus passer leur temps à t'accueillir"

Entendre "tu vas les gêner ces gens, être de trop."
Entendre "tu vas pas non plus profiter indéfiniment"

Profiter
Profiter
Profiter

Gêner
Gêner
Gêner

La peur infinie, qui me submerge entièrement : Celle de gêner. Aucune idée de si c'est parce que je suis une meuf, habituée à ne pas devoir prendre trop de place. Si c'est un truc social, j'ai jamais été à l'aise en groupe, et ai rapidement appris à détecter qu'on me repoussait gentiment mais fermement, parce que j'étais trop bizarre ou différente.

Et chez moi, on ne parle pas. En même temps, chez moi, on accepte à bras ouverts n'importe qui. Jamais entendu ma mère râler contre le fait d'accueillir quelqu'un, que se soit ses amis, mes amis, mes petits amis, ponctuellement ou à plus long terme.

De quoi j'ai peur ?

De gêner. De ne pas le réaliser. De me faire rejeter. Je crois que j'ai peur de ça en fait, du rejet, qu'on n'ose pas me dire que je gêne, et que le rejet soit du coup encore plus fort.

Ca fait une grosse prise de tête pour rien. Mais ça fait du bien de l'écrire quand même.

La surprise du jour, c'est d'en avoir parlé avec ma mère. Qui trouve quand même que de ne pas avoir de chez soi c'est un peu bizarre. Ou compliqué. Qui reconnait qu'elle est très attachée aux choses. Qu'elle aimerait pouvoir me léguer des choses, me faire des cadeaux de ses voyages, pour que je puisse m'en souvenir ou en parler plus tard. Qui trouve que ça manquerait de réciprocité de ne pas pouvoir accueillir les gens chez moi. Et en même temps ne pas la sentir à l'aise avec ce terme. Et puis, l'entendre dire que dans 2 ans c'est la retraite, et qu'elle va partir elle aussi. Mais ne sais pas où. "Je veux pas me fixer, j'ai envie de bouger tu sais". Ben oui je sais. Et de me demander dans la foulée si j'ai une idée de ce qui me pousse à bouger comme ça. Si c'est pas aussi parce que mes parents ont fait que bouger.

Ptet ben que oui. Jamais enchaînée à un endroit, depuis ma naissance. Impossible de trouver un chez-moi où je me sente vraiment bien.

Et si tout ça faisait sens ? Si le fait d'être freelance me permettait de bouger comme je l'entendais, sans demander la permission à quiconque ? Si ne jamais s'être sentie chez soi nulle part voulait juste dire que s'enraciner quelque part ce n'était pas comme ça que ça marchait pour moi ?

Et si j'étais faite pour ça ?

C'est flippant

De tout remettre en cause comme ça, c'est flippant.

Ma mère est revenue à des choses très matérielles. Ce qui était marrant, c'était qu'elle se demandait si j'allais vraiment bazarder la totalité de mes possessions, avant de préciser "même si tu as une place chez moi pour ça, bien sur". Et ensuite, me dire qu'à terme ça lui semblait compliqué, l'itinérance. A terme ça veut dire si j'ai un gosse, un jour.

On manque de modèles quand même. Ce qui est marrant, c'est qu'à aucun moment l'âge ne lui a semblé être un point problématique. Les chiens ne font pas des chats à mon avis. Mais j'ai eu la chance d'avoir plusieurs modèles inspirants qui cheminent à leur façon, alors qu'elle n'a apparemment que moi pour se projeter. Pas simple cette histoire.

J'ai *tellement peur* de le faire pour de vrai. J'aurai l'impression de me lacher dans un grand bain plein de… requins ? (ce qui est marrant c'est que ça me fait franchement pas flipper les requins x) )

J'ai peur que ce que j'ai vécu cet été ne soit possible que parce que c'était éphémère. Et le pire, c'est que je n'y pensais même pas, jusqu'à ce qu'on me le dise.

"Oui mais bon, le faire un peu, là, ça va… Mais bon les gens, ils vont pas non plus passer leur temps à t'accueillir"

Ça les embêterait de me voir plus souvent ? Est-ce que 3 nuits tous les 9 mois c'est trop ? tous les ans ? tous les 2 ans alors ? Quand les personnes me disent "Tu reviens quand tu veux" est-ce que je peux vraiment les prendre au pied de la lettre ? Ou est-ce qu'elles me le disent poliment et légèrement, et l'oublient aussitôt ?

Est-ce que j'y attache trop d'importance ? #Zébrée

J'essaie de me détacher mais c'est pas facile.

Est-ce que je peux poser la question franchement aux personnes ? Est-ce que ça ne va pas les mettre complètement mal à l'aise ? Est-ce que je suis prête à entendre la réponse ? Est-ce que celleux qui vont me répondre ne vont pas juste le faire avec gentillesse pour ne pas me blesser, et s'oublier un peu dans l'espoir de m'apporter du soin ?

Ce qui est marrant (non) c'est que j'ai posé la question pour rire. "Et si je restais 2 mois ?"

J'ai exagéré exprès pour avoir une réaction. Quand j'y pense, c'est bien nase ça aussi. Et si je posais la question *simplement* ?

Je crois que j'ai trop peur d'entendre la réponse. Il faut peut-être que j'arrête de me raconter des histoires.

Je me dis que de toute façon, même en passant son temps en Airbnb ca vaudrait quand même le coup.

Alors il n'y a rien à perdre non ?

Mais c'était tellement précieux de passer du temps avec chacun.e que j'ai peur de perdre ça.

Est-ce que l'itinérance sans intimité ça vaut le coup ?

Je pense pas.

Est-ce que j'ai pas juste peur de me sentir seule ?

Je crois pas.

Aaaaaaaaah c'est siiiiii compliqué !!!!

Je me sens coincée.

Je vais commencer par me débarrasser de mes possessions. Je me sens trop encombrée, ça m'alourdit et m'empêche d'être tranquille.

M'améliorer à comprendre ce qu'il se passe dans ma tête.

Ptet faire de la self-défense pour se sentir plus à l'aise de croiser des gars.

Ce qui est marrant, c'est que la compatibilité être enceinte / itinérante est une des premières questions que j'ai eues.

Il y a des signes quand même.

Ptet que Thomas avait raison. Je ne m'arrêterai plus jamais.

Je me sens comme un lion en cage. Peut-être parce que c'est le premier jour.

Ou peut-être parce qu'au fur et à mesure du temps, je vais étouffer la part de moi qui veut bouger. L'anesthésier en me disant "bientôt".

Je ne crois pas que je réussirai à la tuer. Et vivre l'itinérance à moitié, avec une date de fin et un retour au bercail ça ne me donne pas envie.

Je lisais sur Mastodon les phrases qui décrivent le contentement de retrouver son chez-soi une fois le voyage terminé. Ca ne fait pas sens pour moi.

Est-ce que je suis mécontente de l'avoir retrouvé ? Est-ce que je n'ai pas de chez-moi ?

J'en sais rien mais mon lit me donne moins envie que tous les canapés sur lesquels j'ai dormi.

Il y a des signes j'vous dit.

J'ai déjà envie de repartir.
