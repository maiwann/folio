---
layout: post
title: "Et Twitter alors ?"
subtitle: "L'aventure de la modération : Chapitre 5"
date: 2022-11-15 14:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/laventuredelamoderation5/
---


Pour conclure cette série de textes sur la modération, et maintenant que j'ai grandement détaillé comment il est possible, sur Mastodon, d'avoir une modération qui fait le taf, je voulais revenir sur pourquoi, à mon avis, il **est impossible pour les grandes entreprises capitalistes de faire correctement de la modération (et pourquoi ça ne sert à rien de leur demander de faire mieux !).**

C'est le coeur de cette série car je suis très frustrée de voir systématiquement une bonne analyse faite sur les problèmes des outils, qui se casse la figure au moment d'évoquer les solutions. Alors c'est parti !

NB : Je fais cette analyse sans considérer qui est le PDG au moment où je publie car je pense que ça ne change pas grand chose sur le fond, au pire le nouveau sera plus transparent sur sa (non-)politique de modération.

## Twitter ne fera pas mieux car…

### Twitter ne veut pas dépenser d'argent pour construire des équipes de modération

Vous le savez sans doute, c'est passé dans de nombreux articles comme celui de Numérama : [ Twitter assume n’avoir quasiment pas de modérateurs humains ](https://www.numerama.com/tech/786817-twitter-assume-navoir-quasiment-pas-de-moderateurs-humains.html)

> Twitter emploie 1 867 modérateurs dans le monde.

Pour 400 millions d'utilisateurices, ça fait 1 modérateurice pour 200 000 comptes. Donc évidemment la modération ne peut pas être suffisante et encore, on ne sait pas comment les effectifs de modération sont répartis selon les langues. Il est évident qu'il est extrêmement difficile de modérer, alors modérer dans une langue qu'on ne maitrise pas c'est mission impossible !

Je rajoute rapidement qu'en plus, contrairement à ce que j'évoquais dans mes articles précédents sur "comment prendre soin" et construire un collectif de modération, il n'y a absolument aucune notion de soin et de collectif lorsque l'on est modérateurice chez Twitter. Il y a au contraire des conditions de travail délétères qui amplifient le problème : interdiction de parler à ses collègues, de raconter à l'extérieur ce à quoi on a été confronté, pression temporelle intense donc pas possible de récupérer après un moment violent.

Bref, Twitter veut économiser de l'argent, et le fait notamment sur le dos de ses modérateurices qui sont envoyées au massacre psychologique de la modération, et donc au détriment de ses utilisateurices les plus fragiles

### Twitter préfère les robots

Face aux critiques qui lui sont faites, Twitter répond qu'il compte sur l'automatisation des robots pour faire ce travail pénible.

Je pense que cette posture ne tient pas pour plusieurs raisons :
1. C'est technosolutionniste que de penser que des robots peuvent répondre à un problème social (et donc à mon avis voué à l'échec)
2. Les robots sont alimentés par ce que nous leur donnons, et donc remplis de biais qu'ils vont répercuter dans leur politique de modération. Dans un collectif de modérateurices, ces biais sont atténués par les débats et discussions. Je ne crois pas que les robots soient très portés sur la discussion de ce qui est juste ou non.
3. Le contexte est primordial en modération, et un robot ne peut pas être assez "intelligent" pour comprendre ce contexte. Pour exactement le même contenu, selon d'où parle la personne, la réponse de modération ne sera pas la même. Vous imaginez bien qu'entre une femme qui dit subir du sexisme ou un homme, on n'est pas sur la même action à réaliser, l'une subit une oppression systémique tandis que l'autre récupère maladroitement ou stratégiquement un mot en le détournant.


Les robots ne sont donc pas une solution sur laquelle on peut compter, mais sont une bonne façon de détourner la discussion du sujet "Pourquoi n'avez-vous pas plus de modérateurices". Il vaut mieux répondre "Le boulot est trop ingrat, on préfère que se soit des robots, on les améliore chaque jour" plutôt que "on ne veut pas mettre d'argent à payer des modérateurices et puis quoi encore ?". (Quoique en ce moment, il y a une bonne clarification des postes considérés comme utiles chez Twitter, et la modération n'en fait pas partie !)

On pourra me dire : Oui mais un robot, c'est fiable car ça prend toujours les mêmes décisions !

Et c'est peut-être vrai, mais pourquoi avons-nous besoin d'une modération qui ne bouge pas dans le temps ? Comment s'améliorer, comment faire évoluer nos pratiques si toute la connaissance est refilée aux robots ?

Nous avons besoin de remettre de l'humain dans nos médias sociaux, et déléguer aux robots ne fait que contourner le problème.

### Twitter a besoin que les gens soient blessés

Enfin, de par le fonctionnement même de Twitter, le réseau social a besoin pour son modèle économique de gens qui souffrent, qui sont malheureux, blessés, en colère, pour gagner plus d'argent.

C'est le principe de l'économie de l'attention : Plus vous restez sur un média social, plus vous partagez vos données, regardez des pubs, interagissez et faites rester les autres.

Et pour vous faire rester, rien de tel que de vous confronter à ce qui vous énerve, ce qui vous fait vous sentir mal, ce qui vous fait réagir.

Pour cela, les comptes d'extrême-droite sont de l'or en barre : Ils passent leur temps à dire des saloperies, et à un moment l'une d'elle va forcément vous toucher plus particulièrement qu'une autre, soit parce que vous êtes concerné·e, soit parce que quelqu'un de votre entourage l'est.

Ensuite on cite le tweet concerné en disant qu'il faut surtout pas écouter ce genre de personne, on déconstruit l'argumentaire en un fil de 8 tweets, tout cela augmente la visibilité du contenu initial et donc d'idées d'extrême-droite, personne n'a changé d'avis à la fin mais vous vous sentez sans doute encore moins bien qu'au début, ou plus énervé·e… et chouette pour Twitter, parce qu'être en colère ou triste ça rend davantage sensible à la publicité.

Vous l'aurez compris, il y a donc par nature un problème de fond : Si Twitter vire tous les comptes d'extrême-droite, son chiffre d'affaire va chuter. Alors il peut promettre la main sur le coeur qu'il compte le faire, les yeux dans les yeux, et vous avez le droit de le croire.

Pour moi les résultats sont là, rien ne se passe, et il n'y a pas de raison que ça s'arrange avec le temps (au contraire !)

## Oui mais… et la violence à laquelle sont soumis les modérateurices dans tout ça ?

Les entreprises capitalistes du numérique entretiennent savamment la croyance que puisqu'il y a de la violence dans le monde, il n'y a rien à faire, quelqu'un doit bien se farcir le taf ingrat de subir cette violence transposée sur les réseaux sociaux, et ces personnes, se sont les modérateurices.

Je ne suis pas d'accord.

Je pense que nous pouvons collectivement améliorer de beaucoup la situation en priorisant des technologies qui prennent soin des humains, et qu'il est immoral de perpétuellement déléguer le travail de soin (des enfants, de la maison, des espaces en ligne) à des personnes qui sont toujours plus mal payées, plus précaires, plus minorisées, à l'autre bout du monde…

Nous pouvons avoir des réseaux qui fonctionnent correctement, mes articles sur Mastodon en font l'exemple.

Est-ce que c'est parfait ? Non, mais au moins, la modération est gérée à petite échelle, sans la déléguer à des personnes inconnues qu'on rémunère de façon honteuse pour faire le "sale travail" en leur détruisant la santé.

Et si à terme Mastodon grossit tellement qu'il sera soumis aux contenus les plus atroces, et que cela rendra la modération impossible ou complètement délétère pour les modérateurices, eh bien il sera temps de l'améliorer ou de tout fermer. Parce que nous pouvons aussi faire le choix de ne pas utiliser un outil qui abîme certain·es d'entre nous plutôt que de fermer les yeux sur cette violence.

Peut-être que le fonctionnement fondamental des réseaux sociaux a finalement des effets délétères intrinsèques, mais pourquoi se limiter à ça ? Pourquoi ne pas discuter collectivement de comment faire mieux ?

Et sur un autre registre, il est aussi possible de mettre de l'énergie en dehors de la technologie, sur un retour à plus de démocratie, plus de débat apaisé. Parce que peut-être que si chacun·e retrouve une confiance sur le fait que son avis et ses besoins seront entendus, il sera possible de retrouver collectivement plus de sérénité, et cela se ressentira sûrement dans nos intéractions numériques.

[Un autre monde (numérique) est possible,](https://framasoft.org/fr/) il nous reste à le construire, pour contribuer à une société empreinte de justice sociale où le numérique permet aux humain·es de s’émanciper, à contre-courant des imaginaires du capitalisme (de surveillance).
