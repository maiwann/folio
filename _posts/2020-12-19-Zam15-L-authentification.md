---
layout: post
title: "Zam 15: L'authentification"
subtitle: ""
date: 2020-12-19 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam15-l-authentification/
---

# Zam 15: L'authentification

## Tout d'abord, il n'y avait rien…

Aux débuts de Zam, il n'existait qu'un seul couple identifiant et mot de passe, qui permettait à chacun, avec les mêmes droits, d'accéder à l'application. Cela simplifiait grandement les choses et nous n'avions de toute façon pas besoin de savoir qui y faisait quoi.


## L'identification

Puis, lorsque les tables sont arrivées, il nous ai apparu que, pour donner un amendement à quelqu'un, il fallait pouvoir le retrouver (et éviter les transferts à des homonymes). Donc nous avons demandé à chacun lors de sa première connexion, de nous donner un mail, que nous ne vérifions pas mais qui servait d'identifiant unique, puis de nous donner un nom a utiliser.

## L'authentification

Zam ayant du succès, une généralisation de l'application est envisagée auprès de tous les ministères (alors que jusqu'à présent il fallait systématiquement passer par nous et nous ouvrions l'accès selon nos moyens et disponibilités). Cependant, il n'était pas entendable de diffuser Zam si l'accès n'étais pas plus restreint. On nous demandait à présent de vérifier que la personne qui voulait se connecter était légitime pour le faire.

Mais qui décide ce qui est légitime ? Les participants à un projet de loi peuvent être nombreux, et devoir vérifier manuellement que chaque personne qui demande un accès est légitime nous paraissait vain. Comment quelques personnes pourraient savoir, lors d'un projet interministériel, si l'ensemble des demandes d'accès sont légitimes ?

Ce que nous avons préféré faire, c'est procéder de la façon la plus ouverte possible : Tous les utilisateurs de Zam ne se connaissent pas, cependant les unités de travail, elles, se connaitront. Nous avons donc mis en place un système d'invitation, où il est possible à chacun, ayant déjà accès à Zam sur un projet de loi, d'inviter l'un de ses collègues en entrant son mail. Ainsi les invitations se feront de connaissances en connaissance, et l'accès reste limité.

## Vous prendre bien un petit mot de passe ?

Mais une fois que l'on obtient une invitation, comment se connecter ?

Déjà, précisons que nous limitons la connexion à des personnes ayant des adresses mail en gouv.fr, afin que des mails personnels ne soient pas utilisés (et nous limitons la fuite de données vers des entreprises… je pense à toi Gmail !).

Ensuite, nous étions frileux à l'idée de demander un énième mot de passe, qui sera sans doute soit peu complexe, soit utilisé ailleurs… Nous avons donc choisi d'envoyer un mail de connexion, comme par exemple utilisé dans Slack (qui appelle ça un "lien magique"). Ensuite, nous plaçons un cookie pour que la personne puisse se reconnecter facilement (et n'ai pas besoin de demander un lien d'accès à chaque fois qu'elle réouvre son navigateur) et le tour est joué !

Une fois connecté, nous demandons comme précedemment à la personne comment la nommer, et elle peut travailler sur Zam !


Le souci avec cette nouvelle fonctionnalité, c'est qu'elle nous empêche de créer des tables "factices" qui étaient utilisées pour **ranger les identiques** ou **attester d'un niveau de validation**. Cela amenait une régression dans les régulations possibles par nos utilisateurs, et cela nous posait problème. Nous avons donc cherché comment rajouter cette fonctionnalité malgré l'authentification. Je détaillerai ce point dans l'article suivant !



## Les prochains articles :

- Les boîtes
- Drum
