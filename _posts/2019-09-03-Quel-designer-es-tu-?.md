---
layout: post
title: "Quel designer es-tu ?"
subtitle: ""
date: 2019-09-03 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/quel-designer-es-tu/
---

Je viens de retomber sur des personnes qui, lorsque j'ai proposé de contribuer, m'ont répondu "Chouette ! On est nuls en logos". Mais le souci c'est que moi aussi :D Mais je suis super forte ailleurs.

Du coup je me prépare cette petite fiche à remplir sur l'ensemble des compétences potentiellement possédées par un designer (mais pas toutes en même temps hein !) et je vous la joins ici pour que vous puissiez en discuter avec les futurs designers avec qui vous collaborerez dans vos projets libres et mieux comprendre ce qu'iels aiment faire (et n'aiment pas !!)

Pour chaque item, je vous invite à remplacer chaque puce par un élément de la légende (une version vierge en bas de l'article) :

- ??? : Je ne sais pas faire
- ? : Ca me parle un peu
- … : J'apprends sur le sujet
- ✔ : Je sais faire
- ♡ : J'adore faire ça

## UX · Recherche Utilisateur
- ♡ Parler aux utilisateurs pour comprendre leur·s problème·s
- ♡ Réaliser des tests du site / logiciel avec les utilisateurs

## UX · Architecture d'information
- ✔ Améliorer l'organisation du contenu au sein d'une page / d'un écran
- ♡ Améliorer l'organisation globale du site / logiciel

## UX · Ergonomie
- ✔ Évaluer l'utilisabilité d'une interface

- ♡ Faire des schémas de l'interface
- ♡ Faire des schémas de l'interface pour un écran de téléphone
- ♡ Faire des schémas de l'interface adaptables sur différentes tailles d'écran

## UI · Graphisme
- ♡ Faire des maquettes graphiques de l'interface
- ♡ Faire des maquettes graphiques de l'interface pour un écran de téléphone
- ♡ Faire des maquettes graphiques de l'interface adaptables sur différentes tailles d'écran

## UX · Accessibilité
- ♡ Réaliser des interfaces accessibles

## UX · Contenu
- ? Écrire des contenus textuels

## Graphisme · Illustration
- … Réaliser des icônes
- ? Réaliser des illustrations
- … Réaliser des animations simples d'éléments de l'interface

## Graphisme · Identité
- ✔ Évaluer un logo / une identité graphique
- ? Réaliser un logo
- ??? Réaliser une identité graphique

## Intégration
- ? Coder en HTML / CSS


Si il en manque, n'hésitez pas à me faire signe =)


## Version vierge
À diffuser tant que vous voulez :

`UX · Recherche Utilisateur
- Parler aux utilisateurs pour comprendre leur·s problème·s
- Réaliser des tests du site / logiciel avec les utilisateurs

UX · Architecture d'information
- Améliorer l'organisation du contenu au sein d'une page / d'un écran
- Améliorer l'organisation globale du site / logiciel

UX · Ergonomie
- Évaluer l'utilisabilité d'une interface

- Faire des schémas de l'interface
- Faire des schémas de l'interface pour un écran de téléphone
- Faire des schémas de l'interface adaptables sur différentes tailles d'écran

UI · Graphisme
- Faire des maquettes graphiques de l'interface
- Faire des maquettes graphiques de l'interface pour un écran de téléphone
- Faire des maquettes graphiques de l'interface adaptables sur différentes tailles d'écran

UX · Accessibilité
- Réaliser des interfaces accessibles

UX · Contenu
- Écrire des contenus textuels

Graphisme · Illustration
- Réaliser des icônes
- Réaliser des illustrations
- Réaliser des animations simples d'éléments de l'interface

Graphisme · Identité
- Évaluer un logo / une identité graphique
- Réaliser un logo
- Réaliser une identité graphique

Intégration
- Coder en HTML / CSS`


Et pour celleux qui sont arrivé·es jusque là : [Cadeau !](https://invidio.us/watch?v=z_M8U3UjwbQ)
