---
layout: post
title: Un peu d'hygiène numérique pour ceux qui n'y connaissent rien
subtitle: ""
date: 2017-10-28 00:15:00 +0200
vignette: null
intro: "Aujourd'hui c'est Cryptoparty ! "
nom_blog: le blog
which_blog: blog_pro
permalink: /un-peu-d-hygiene-numerique/
---

Aujourd'hui c'est Cryptoparty ! A la médiathèque José Cabanis de Toulouse, je prendrai la parole pendant 5 petites minutes pour parler de ce que j'ai mis en place pour commencer à améliorer mon hygiène numérique, aka avoir (enfin !) plusieurs mots de passe et me libérer de Google !

Un article plus long est à venir mais en attendant pour les slides c'est <a href="/docs/posts/2017_10_28/un_peu_d_hygiene_numerique.pdf" class="lien_inline"> par ici ! </a>
