---
layout: post
title: "Zam 7: Les identiques"
subtitle: ""
date: 2020-03-24 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam7-les-identiques/
---

# Les identiques

Après ma [première observation](/blog/zam6-premiere-observation/) durant la soirée PLFSS, nous nous accordons avec l'équipe : la priorité est de mettre en place un système facilitant la gestion des identiques.

## Rappel du contexte

Lors de mon observation au ministère de la santé et sécurité sociale, j'ai observé qu'une grande partie du temps des rédacteurs consistait à :

- Lire un nouvel amendement
- Essayer de se souvenir si ils avaient déjà lu un amendement identique ou non
- Si c'était le cas, retrouver via la recherche de quel amendement il s'agit
- Copier la réponse de cet autre amendement et la coller dans le nouveau.

Pour donner une idée, au bout d'une heure et demie de travail, une des personnes m'a signifié que, depuis le début, elle était "sur 3 réponses différentes" (comprendre 3 groupes d'amendements identiques). Donc elle a passé tout ce temps à faire de *très* nombreux allers-retours.

## Faciliter la recopie

Nous avons donc mis en place un élement afin de permettre aux utilisateurs de recopier la réponse d'un amendement à partir d'un autre. Nous l'avons nommé "Remplir à partir de", ajouté une liste déroulante pour que l'utilisateur puisse sélectionner dans la liste un amendement qu'il avait déjà rempli, et remplir l'amendement dans lequel il se trouve avec la même réponse.

Techniquement rapide à mettre en place, nous étions très contents de notre nouvelle solution, et nous étions persuadés qu'elle permettait enfin de gagner du temps à nos utilisateurs !

## Sauf que…

Eh bien sauf que cette fonctionnalité était très peu utilisée ! J'avance un peu dans le temps pour vous raconter que, lors de mes observations suivantes, je n'ai vu personne utiliser le bloc "Remplir à partir de…", et personne ne le trouvait pertinent lorsque je les interrogeais !

A la place, ils utilisaient un papier pour noter les numéros des amendements, et se déplaçaient dans Zam pour aller copier / coller la réponse, pour chaque amendement !

## Mais pourquoi ?!

Nous avons mis un peu de temps à réaliser que c'est un autre choix de conception qui provoque cette non-utilisation de la fonctionnalité.

Zam est en fait sous-divisé en lectures (Une lecture pour le PLFSS commission, une autre pour l'assemblée, une 3e pour le Sénat…) et non pas en projet de loi (PLFSS, Projet de Loi Finances…).

Or souvent, lorsque les amendements sont identiques, ils le sont souvent vis à vis d'un amendement rempli lors d'une lecture ultérieure !

Et notre bloc "Remplir à partir de" ne permettait pas de remplir à partir d'un amendement d'une autre lecture, simplement car techniquement ce n'éait pas prévu pour fonctionner comme cela !

## Et du coup…

Le temps de se rendre compte que la fonctionnalité était peu utilisée, et que pour qu'elle soit opérationnelle au mieux il fallait revoir l'ensemble de l'architecture, nous avons repoussé sa refonte plus loin dans le temps.

Nous étions en effet sollicité·es sur d'autres aspects que nous voulions prioriser dans une première reconception : L'ensemble des utilisateurs étaient assez ennuyés à l'idée de rater un amendement, que se soit pour la rédaction initiale coté rédacteur, ou vis à vis de la relecture (du coté de leurs supérieurs hiérarchiques ou des coordinateurs).

Nous avons donc priorisé ce sujet qui nous était remonté systématiquement, pour montrer que nous étions conscient que la fiabilité des résultats exportés de Zam (et donc sa pérennité et son utilisation à plus large échelle) était liée à ce **circuit de validation.**


## Les prochains articles :

- [Les corbeilles](/blog/zam8-les-corbeilles/)
- [Atelier de conception / Les zamettes](/blog/zam9-les-zamettes/)
- [Nouvelle version : Les tables !](/blog/zam10-les-tables/)
- [Le rôle de Mélo & les démos](/blog/zam11-melo-et-les-demos/)
- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
