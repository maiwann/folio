---
layout: post
title: "Téléphone & Vie privée"
subtitle: ""
date: 2019-11-05 15:00:00 +0200
vignette: null
intro: " "
nom_blog: le blog
which_blog: blog_pro
permalink: blog/telephone-et-vie-privee/
---

# Téléphone & Vie privée

Au cours des discussions avec des personnes non-informaticiennes, que se soit lors [des Contrib'ateliers](https://contribateliers.org/) (à Toulouse tous les premiers samedi du mois, et ailleurs en France !) ou à Quimper lors d'Entrée Libre, le sujet du téléphone portable est très présent. Comment échapper à Google ou à Apple alors que se sont eux qui proposent la majorité des systèmes logiciels des smartphones que nous avons dans nos poches ?

Cet article récapitule les différentes étapes que je propose de suivre lors des ateliers que je propose de manière informelle sur ce sujet. Il est **à destination des personnes non-informaticiennes, qui ont un téléphone portable sous Android** (donc pas un iPhone, même si une bonne partie de ces conseils sont sûrement applicables, comme je n'ai pas d'iPhone je ne peux pas m'en assurer !), et je choisis de privilégier la facilité d'accès plutôt que la précision des termes informatiques =)

Note : Je déroulerai un atelier lors du Capitole du Libre 2019 qui suivra à peu près ce déroulé : [Smartphone VS Les forces du mal](https://capitoledulibre.org/programme/#smartphone-vs-les-forces-du-mal)


## Pour démarrer : Qu'est-ce qui se passe dans mon téléphone portable ?

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/logo_exodus.png" alt="">
</figure>

Je vous propose d'installer l'application [Exodus Privacy](https://play.google.com/store/apps/details?id=org.eu.exodus_privacy.exodusprivacy&hl=en_US), comme n'importe quelle application (il faut aller dans le Google Play Store pour cela, et chercher "Exodus Privacy"). Une fois installée et lancée, Exodus fait un petit tour des applications présentes sur votre téléphone, et vous liste le nombre d'autorisations demandées, ou de pisteurs contenus.

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/exodus_screen.png" alt="">
</figure>

### C'est quoi une autorisation ?

Je cite [la définition donnée par Exodus Privacy](https://reports.exodus-privacy.eu.org/fr/info/permissions/) :

> Elles peuvent concerner des fonctionnalités ou des informations diverses, comme l'accès à votre géolocalisation, à vos contacts, vos fichiers, votre micro, votre vibreur, votre appareil photo...

Donc si votre application de GPS vous demande votre géolocalisation, c'est normal a priori :) Mais une application lampe de poche, c'est un peu plus étrange… Il faut se poser la question à chaque installation d'application, pour vérifier si l'application abuse sans ce qu'elle demande ou si elle reste raisonnable.

Pour information, il est possible dans certaines versions récentes d'aller, dans les paramètres du téléphone, modifier les autorisations données à une application pour éviter qu'elles puissent être trop curieuses.

### C'est quoi un pisteur ?

Idem ici, je [cite Exodus Privacy](https://reports.exodus-privacy.eu.org/fr/info/trackers/) :

> Un pisteur est un « morceau » de logiciel en charge de collecter des informations sur la personne qui utilise une application, ou bien sur les usages ou l'environnement de cette personne

Vous aurez plus d'informations détaillées sur ce qu'est un pisteur en allant voir [l'article dédié](https://reports.exodus-privacy.eu.org/fr/info/trackers/) ou en [regardant les vidéos](https://peertube.tamanoir.foucry.net/video-channels/2ab4458d-0b3c-485a-aeaf-792cd0842bc8/videos) d'Exodus Privacy sur le sujet.

Un pisteur est là pour collecter de l'information sur vous. Et plus il y a de pisteurs, plus ça sent mauvais, car ces informations sont souvent collectées puis redistibuées à ceux qui ont réalisé le pisteur.


Maintenant que vous avez une vue d'ensemble de ce qui se passe dans votre téléphone, et de à quel point vos applications sont bavardes, nous allons explorer les solutions qui s'offrent à nous ! Cela va vous demander de vous pencher sur chaque application que vous avez et de vous reposer la question de "pourquoi" vous l'avez installée. C'est parti !

## Astuce n°0 : Désinstaller et désactiver en masse

Je suis obligée d'en parler, car c'est toujours utile de le rappeller : Pensez en amont à vous demander si l'application que vous avez vous est bien utile, si passer par l'ordinateur ne suffirait pas… bref si vous ne pouvez pas alléger votre téléphone en supprimant tout simplement cette application de jeu que vous n'utilisez presque jamais, ou cette application de mail qui finalement vous est peu utile car vous préférez passez par votre ordinateur pour les lire…

Il y a aussi certaines applications pré-installées sur nos téléphones qui ne nous servent pas mais qui sont impossibles à désinstaller. Dans ce cas, allez dans les paramètres de votre téléphone, section application, et enlevez le maximum de droits à cette application : désactivez-la, enlevez lui ses permissions si possible, regroupez là dans un dossier avec toutes les applications qui sont dans ce cas.

## Astuce n°1 : Ne plus passer par l'application !

Peu de personnes le savent, mais il arrive souvent que pour certaines cas, le site internet fasse tout aussi bien le travail qu'une application, mais en étant moins bavard ! C'est le cas pour les journaux, les radios, la météo… Pas besoin d'application pour avoir l'information !

### Comment on fait ?

Je vous recommande d'installer Firefox, une application qui permet d'accéder à internet et qui fait l'effort d'être respectueuse de votre vie privée. C'est ensuite comme sur un ordinateur, vous ouvrez firefox et vous pouvez aller sur le site qui vous intéresse :)

#### Bonus 1 : Une icône comme une application !

Un des aspects pratique des applications, c'est l'icône de démarrage rapide pour éviter d'avoir à faire x fois une recherche pour retomber sur l'écran désiré. **Mais il est possible d'avoir cette petite icône sans installer d'application !**

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/logo_firefox.png" alt="">
</figure>

Sur Firefox, admettons que vous vouliez avoir accès à France Culture rapidement, il vous suffit d'aller une première fois sur le site de France Culture, puis d'appuyer sur l'icône de maison avec un petit + , à droite sur la barre du haut, pour l'ajouter à votre écran de téléphone ! Confirmez ensuite en appuyant sur le bouton bleu "Ajouter à l'écran d'accueil" et hop ! C'est magique et plus besoin d'application !

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/screen_firefox.png" alt="">
</figure>


#### Bonus 2 : Installer un bloqueur de pub

Ce qui est top, c'est que sur Firefox on peut installer un bloqueur de pub. Il bloquera les publicités, et permettra de charger les sites plus rapidements ! Recherchez **"ublock origin"** dans votre moteur de recherche (Qwant ou Google), et installez l'extension ! Ca y est, votre téléphone a un bloqueur de pub d'installé :)

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/screen_ublockorigin.png" alt="">
</figure>


#### Bonus 3 : Ne plus chercher via Google

En ouvrant l'application Firefox, en haut à droite, vous avez 3 petits points qui vous ouvrent un menu. Allez dans les Paramètres, section Recherche. Là Firefox vous proposera plusieurs moteurs de recherches, dont Google par défaut. Sélectionnez en un autre (je vous recommande Qwant ou DuckDuckGo) puis "Définir par défaut". Hop ! Vos recherches se passeront de Google désormais !



### Dans quel cas ça ne marche pas ?

 A priori, si vous avez besoin d'accéder au contenu en hors ligne (sans réseau, dans certains métro par exemple) c'est là que ça coince ! Il est aussi possible que l'application ai des fonctionnalités spécifiques, que n'a pas un site internet (les GPS par exemple), mais ça reste des cas très spécifiques (les jeux, les GPS…)



## Astuce n°2 : Trouver des applications alternatives et libres !

Si pour certains de vos usages passer par le site internet ne fonctionne pas, c'est le moment de se demander si il existe des alternatives libres qui auront les mêmes fonctionnalités mais protègeront mieux votre vie privée !

### Où trouver ces applications ?

<figure>
  <img src="/img/posts/2019_11_Telephone_vie_privee/logo_fdroid.svg" alt="">
</figure>

Je vous suggère de passer par Firefox, et de lancer une recherche "Télécharger F-droid" pour télécharger un magasin d'application libre nommé …F-Droid ! Une fois téléchargé et installé (attention, votre téléphone vous demandera une contre confirmation en vous disant que l'installation est risquée), vous pouvez l'utiliser comme le GooglePlayStore pour y télécharger des applications qui, elles, seront toutes libres !

### Mes recommendations personnelles

Pour vous guider à travers la jungle d'applications existantes, je vous recommande :

- Pour le GPS : Maps (ou OSMand)
- Pour regarder des vidéos YouTube : NewPipe
- Pour avoir des discussions (groupées) : Signal
- Pour discuter par SMS : Silence
- Pour se déplacer avec les transports en commun : Transportr

## Astuce n°3 : Installer un bloqueur de pub… pour tout le téléphone !

Si vous êtes ici, normalement vous avez :
1° Désinstallé (ou désactivé) toutes les applications pour lesquelles vous ne trouvez plus d'utilité
2° Mis en place des raccourcis via Firefox pour aller rapidement sur les sites qui vous intéressent
3° Lorsque c'était possible, remplacé les applications que vous aviez par des alternatives libres.

Oui mais alors, comment faire si il vous reste des applications que vous avez envie de garder, et que celles-ci ne sont pas libres ?

Je vous propose d'installer une application qui se nomme Blokada. Le mieux c'est de l'installer via F-droid, car la version qui s'y trouve contient un petit compteur qui dénombre… combien de pisteurs ont été bloqués !

Une fois l'application installée, vous pouvez appuyer sur le bouton "Play" pour Activer l'application, et Blokada est lancé et filtrera ce qui sort de votre téléphone !

## Pour finir

Si vous avez réussi à tout faire, bravo ! Si mes explications ne sont pas claires, n'hésitez pas à m'écrire pour que je puisse améliorer cet article.

Si vous souhaitez aller plus loin ou trouver un peu d'aide, rien de tel que les rencontres autour du libre pour cela ! Je vous invite à vous renseigner autour de ce qui se fait près de chez vous, si il existe des café vie privée ou des contribateliers cela peut être un bon point d'entrée. Peut-être que vous trouverez votre bonheur sur [l'agenda du libre](https://www.agendadulibre.org/) ?

Cet article est issu de mes discussions et de mon travail avec les membres d'Exodus, merci à elleux.
Vous pouvez retrouver leurs conseils sur leur page ["Mieux comprendre : Et après ?"](https://reports.exodus-privacy.eu.org/fr/info/next/).
