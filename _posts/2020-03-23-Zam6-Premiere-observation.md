---
layout: post
title: "Zam 6: Première observation"
subtitle: ""
date: 2020-03-23 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam6-premiere-observation/
---

# Première observation

Elle est venue l'heure de ma première observation !! Eh oui, c'est le moment de me rendre à Paris, au ministère de la santé et sécurité sociale, pour pouvoir observer les utilisateurs, en plein coup de feu, utiliser le logiciel au sein de l'activité des rédacteurs et coordinateurs. C'est de l'observation terrain, et je suis à la fois flippée car je vais arriver seule dans un endroit complètement inconnu, observer des personnes que je ne connais pas, et excitée à l'idée d'enfin pouvoir me confronter au réel !

Enfin seule, ce n'est pas tout à fait vrai : Je vais rejoindre Mélo, que j'ai déjà rencontrée "en vrai" auparavant, mais une seule fois ! Bref, je suis impressionnée d'avance.

## Mais ! Tu ne les rencontres que maintenant ?

Alors pour le contexte : Nous sommes mi-octobre 2018, et ça fait environ 4 mois que je travaille sur Zam. Mais de mon point de vue, le temps de prendre connaissance du contexte, de l'équipe, commencer à travailler ensemble et de faire les améliorations un peu "basique" d'ergonomie sur l'application, j'avais déjà de quoi faire pour ne pas manquer de matière. Et il faut dire que Mélo, ayant elle-même effectué l'activité et ayant de très bonnes capacités d'analyse, était très pertinente lorsque je lui posais des questions.

## Mais ! Pourquoi tu te déplaces alors ?

Je suis allée observer les utilisateurs du PLFSS car :
- Principalement, c'était notre première mise en production (enfin presque, la seconde pour ces utilisateurs) et donc c'était un très bon moment pour y aller,
- Parce que avoir deux paires d'yeux au même endroit (Mélo faisait partie de l'équipe qui rédigeait les réponses des amendement du PLFSS) c'est toujours mieux qu'une seule
- Et qu'une paire d'yeux qui est hors du contexte et donc pourra poser des questions naïves aux travailleurs c'est bien
- Et pour finir, parceque j'ai un oeil entraîné (via ma formation en ergonomie) à observer les personnes en activité, à regarder certains points précisément et à éviter certains écueils !

## Le contexte

Le déroulé prévu de la soirée était :

- J'arrive en fin d'après-midi au ministère et suis accueillie par Mélodie, qui me présente à ses collègues. Ceux qui sont à l'aise avec le fait que je vienne les observer ont collé un post-it sur leur porte, pratique !

- A 17h, les amendements sont mis en ligne par l'assemblée. Tout le monde est sur le pont pour commencer à travailler le plus vite possible car…

- … d'ici la fin de la soirée, il faut que tous les amendements aient été lus et aient au moins reçu un avis (ce qui en général dure jusqu'à ~3h du matin !)

Je précise que les utilisateurs que j'allais observer avaient déjà utilisé Zam pour le PLFSS de cette année là, pour la séance en "commission", une sorte de séance préalable avant le passage à l'Assemblée. Certains amendements sont redéposés d'une fois sur l'autre, donc ils avaient déjà travaillé sur les réponses aux amendements de ce projet de loi.

## L'observation

### Le démarrage

J'arrive donc au ministère, Mélo vient me chercher et me présente à ses collègues. Je choisis de démarrer mon observation avec l'un de ses collègues qui a un poste de coordinateur (donc surveillance un peu générale de l'avancement des amendements).

Ca y est, c'est l'heure fatidique ! Le site de l'assemblée nationale affiche enfin les amendements !!

Tout le monde passe sur Zam, qui visiblement a un peu de mal et rame… Les amendements ne sont pas encore là, donc l'utilisateur avec lequel je suis rafraichit sa page. Puis encore une fois… Et une autre fois… Zam met de plus en plus de temps à se remettre à jour (nous supposons que c'est dû au fait que de plus en plus de monde rafraîchit la page en même temps !).

L'impatience monte, les utilisateurs sont à deux doigts d'abandonner et de re-partir sur leur méthode habituelle, quand… les amendements sont enfin là ! L'attente a dû être de 20 minutes ou une demi-heure, mais c'était déjà visiblement compliqué d'attendre ce temps pour nos utilisateurs (et autant vous dire que je me faisais toute petite sur ma chaise !).

### Au cours de la nuit

Plutôt que de vous détailler chronologiquement ce qu'il s'est passé, je vais plutôt vous raconter les points d'étonnement de cette soirée.

#### Les identiques

Le point le plus saillant de cette observation a été les identiques. Les identiques (ou similaires) se sont des amendements qui sont (quasiment) identiques en terme de contenu, mais déposés par des députés différents.

Les rédacteurs font donc **très régulièrement** les actions suivantes : *lis les amendements dont iel a la charge*, "Le 56, le 68, le 87, le 92… Ah, tiens, le contenu du 92 me rappelle quelque-chose… ah oui c'est un identique du… euh… du 68 je crois" , *va regarder le 68 dans zam*, Ah oui c'est bien ça !, *retourne sur le 92 et indique en commentaire "identique au 68"*, *ouvre le 165*, "Ah, celui là, j'y ai répondu en commission…", *sors de la lecture "Assemblée"*, *va dans la lecture commission retrouver, grâce à l'objet, l'amendement identique*, *vérifie que c'est le bon*, *copie la réponse donnée (car elle n'a pas changé en 2 semaines)*, *retourne dans l'amendement 165 et y colle la réponse*…

Et ces allers-retours, les rédacteurs les faisaient… tout le temps… Lire un amendement, chercher dans leur mémoire si il leur disait quelquechose, retrouver l'autre amendement, copier, se rappeller du numéro du premier amendement, revenir sur l'amendement, coller la réponse…
C'était atroce pour moi de les voir faire autant d'allers-retours alors que je savais qu'on aurait pu ajouter un élement pour aller plus facilement récupérer une réponse d'un autre amendement. Enfin, au moins, je me suis rendue compte avec précision du prochain point de douleur à travailler.

#### Des façons de faire variées

A un étage, les personnes s'interpellaient à travers les bureaux pour savoir qui en était où. A un autre, les rédacteurs se mettaient 2 par 2 pour se souvenir des numéros d'amendements et retrouver plus facilement les identiques. Encore ailleurs, des rédacteurs imprimaient l'amendement pour ne pas s'abîmer les yeux ! C'était très intéressant de pouvoir observer toutes ces stratégies différentes :)

### Le hack du champ objet

Les utilisateurs éprouvaient le besoin de signaler où en était un amendement : Est-ce qu'une rédaction était en cours ? Est-ce qu'il était temps pour les coordinateurs de le relire ? C'est pourquoi ils ont hacké le champ "objet" en y glissant des informations sur "l'affectation" de l'amendement (à qui est-ce le tour de donner son avis sur l'amendement), afin de pouvoir se répartir le travail :)

#### Des tris ! Des filtres !

Les utilisateurs triaient systématiquement la liste par article (plutôt que par ordre croissant d'amendement, le choix par défaut en attendant l'ordre annoncé par l'Assemblée). Et ils utilisaient presque systématiquement les filtres, et donc cliquaient sur le petit bouton pour Afficher les filtres.

#### Les objets = <3

Les objets sont très utilisés et appréciés, surtout pour la recherche d'identiques, et ils facilitent énormément le travail des utilisateurs (qui, sinon, auraient forcément un petit papier où ils noteraient les thèmes :D)

#### Vérifications

Pour vérifier le contenu des réponses, les coordinateurs doivent cliquer un à un sur chaque amendement, les lignes sont étroites et ils ont peur d'en rater un…

#### Tour Eiffel

Moment magique dans la nuit : Le moment où Mélo est venue me chercher pour me retrouver dans le bureau d'où… il y avait vue sur la Tour Eiffel illuminée dans la nuit. Petit moment de beauté au milieu d'une soirée de travail !

### Fin de soirée

Je me suis enfuie vers la fin de la soirée PLFSS, vers minuit et des brouettes ! De 17h à minuit, cela fait une bonne séance d'observation de l'activité !

### Bilan

Cette nuit fut très riche en enseignements autour du produit : Ce qui était utilisé, ce qui ne l'était pas, ce qui était à améliorer… Et le fait de pouvoir s'immerger dans le contexte réel des utilisateurs, d'autant plus dans un ministère en pleine nuit, c'est toujours appréciable !

## Et maintenant ?

La priorité, au vu de cette observation, c'est [d'améliorer la façon de traiter les identiques](/blog/zam7-les-identiques/), afin de faire gagner autant que possible du temps sur ce point à nos utilisateurs ! Ce sera donc l'objet du prochain article !


## Les prochains articles :

- [Les identiques](/blog/zam7-les-identiques/)
- [Les corbeilles](/blog/zam8-les-corbeilles/)
- [Atelier de conception / Les zamettes](/blog/zam9-les-zamettes/)
- [Nouvelle version : Les tables !](/blog/zam10-les-tables/)
- [Le rôle de Mélo & les démos](/blog/zam11-melo-et-les-demos/)
- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
