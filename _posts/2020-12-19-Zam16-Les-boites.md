---
layout: post
title: "Zam 16: Les boîtes"
subtitle: ""
date: 2020-12-19 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam16-les boites/
---

# Zam 16: Les boîtes

Dans Zam, l'[absence de vérification disant que chaque connexion était celle d'un humain](x) permettait aux utilisateurs de créer des faux comptes, ouvrant des espaces leur permettant de mettre des amendements de coté, ou de signifier un niveau de validation.

Or, lors de la mise en place de l'authentification, cette astuce n'était plus accessible. Et nous ne voulions pas faire régresser Zam dans la souplesse qu'il permettait.

C'est pourquoi nous avons créé…les boîtes !

## Les boîtes ?

Alors qu'une table appartient à une personne, une boîte correspond à un endroit où l'on range les amendements. Ils ne peuvent pas être modifiés tant qu'ils sont dans cette boîte.

On peut créer autant de boites que l'on veut dans une lecture, telles que "Validé CAB" ou "Bureau 2B" ou encore "Modifications après impression dossier". Et pour les distinguer des tables, nous avons rajouté une petite icône sur l'index pour les identifier plus facilement !

<figure>
  <img src="/img/posts/2019_Zam/16/1.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/16/2.png" alt="">
</figure>

Cette intégration comme fonctionnalité d'une régulation des utilisateurs a notamment été possible grâce à nos échanges avec ces utilisateurs : Entre les temps de démonstrations, les discussions, et les observations, nous avons pu voir que cette astuce leur apportait beaucoup de valeur et qu'il était important de la faire perdurer au sein de l'application. Sans cela, nous aurions pu faire disparaitre une régulation en la rendant techniquement impossible, et nos utilisateurs s'en seraient trouvés fortement ennuyés voire mécontents.

## Les prochains articles :

- Drum
