---
layout: post
title: "Zam 11: Le rôle de Mélo & les démos"
subtitle: ""
date: 2020-12-18 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam11-melo-et-les-demos/
---

# Zam 11: Le rôle de Mélo & les démos

Avant de travailler sur Zam, j'avais parfois entendu parler du rôle de Product owner, mais sans jamais savoir précisément ce qu'il signifiait, ni ce qui le différencie du rôle d'UX Designer.

La définition que j'avais à peu près dans ma tête étant "La personne responsable du produit, qui priorise ce qu'il y a à faire". Mais je me suis toujours demandée pourquoi ce rôle alors que, si l'on écoute les utilisateurs, on peut leur demander de prioriser ce qui a de l'importance pour eux et donc pour le produit… Tout cela restait en tout cas assez vague

Pour Zam, c'est Mélodie qui est notre Product Owner. Et autant vous dire que j'ai découvert le rôle avec une personne qui le gère à merveille.

## Les démos

Depuis le début de Zam, Mélodie propose aux futurs utilisateurs de se retrouver toutes les deux semaines, afin de discuter des besoins des utilisateurs ou de l'avancement du projet. Elle prend donc un peu de temps pour leur faire faire un tour de l'application en vidéoprojetant le tout dans une salle de réunion, puis elle répond à leurs questions.

De ces discussions naissent souvent beaucoup de choses très intéressantes. On découvre les angoisses des utilisateurs, qu'on essaie de rassurer, on peut distinguer les spécificités de travail des uns et des autres, les besoins que le groupe priorise et ceux qu'ils mettent de coté plus facilement…

## La priorisation

Et c'est là que Mélodie fait un travail incroyable, qui est pourtant très peu visible : A chaque demande formulée, souvent sous forme de solution, elle réinterroge le besoin, demande des explications vis à vis du proccessus métier, pour comprendre l'origine de problème. Une fois celui-ci explicité, elle trie et priorise ce qui va dans le sens du produit, et ce qui est à écarter. C'est un jeu de stratégie très pointu, car à chaque fois qu'elle se doit de dire "Non", elle doit repréciser systématiquement pourquoi ce non, pourquoi d'autres choses sont prioritaires, ce qui peut être difficile à entendre notamment face à des peurs difficiles à calmer (la peur de la malversation notamment, qu'un utilisateur s'amuse à mettre le bazar), qui demandent des fonctionnalités ajoutant du contrôle à l'intérieur de Zam, alors que nous sommes convaincus que le projet est plebiscité principalement par la souplesse d'organisation qu'il offre.

Pour récapituler, Mélo écoute les utilisateurs, les aide à traduire leurs demandes en besoin métier, les rassure et joue à la cheffe d'orchestre entre les différentes demandes contradictoires et la volonté de développement de Zam.

C'est un rôle qui permet ensuite, lorsqu'elle discute avec l'équipe technique (David, Ronan et moi), d'avoir des orientations et des étapes claires de ce qu'il y a à faire dans les jours ou mois qui viennent, et de l'importance de chacune de ces étapes.

Cet itinéraire clair nous facilite la vie et permet au projet d'être une réussite, et j'ai pris beaucoup trop de temps pour me rendre compte de tout le travail en amont que cela représentait ! Merci Mélo =)

Pour la suite, je vous raconterai une [nouvelle session d'observation sur le terrain !](/blog/zam12-nouvelles-observations/)


## Les prochains articles :

- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
