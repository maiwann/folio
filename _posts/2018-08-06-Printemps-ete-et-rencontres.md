---
layout: post
title: "Printemps, été et rencontres"
subtitle: ""
date: 2018-08-06 15:00:00 +0200
vignette: null
intro: "Le printemps et l'été ont été riches en rencontres et conférences ! Je prends un peu de temps pour coucher sur ~~papier~~ écran ce qui s'est passé ses derniers mois, les rencontres passées, les bonnes idées des conférences et ce qu'il m'en reste plusieurs mois après. C'est parti !"
nom_blog: le blog
which_blog: blog_pro
permalink: blog/printemps-ete-et-rencontres/
---

Le printemps et l'été ont été riches en rencontres et conférences ! Je prends un peu de temps pour coucher sur ~~papier~~ écran ce qui s'est passé ses derniers mois, les rencontres passées, les bonnes idées des conférences et ce qu'il m'en reste plusieurs mois après. C'est parti !


# MiXiT à Lyon - 19 & 20 avril

## Joies
Donner ma première conférence ♥ Mettre des gifs de pokémon partout ♥ Ne pas être dans le grand amphi ♥ Avoir des repas végétariens ♥ Composter les déchets du repas ♥ La chaleur ♥ Faire sa conférence pieds nus ♥ Les crêpes ♥ La visite de Lyon ♥ Les passages secrets de Lyon ♥ Le parc de la tête d'or ♥ Les efforts d'accessibilité de MiXiT ♥ Voir des conférencières ♥ Refaire sa conférence le soir au bar ♥ Entendre "Ah mais c'est pas juste qu'on est mauvais en design dans le logiciel libre" ♥ Rencontrer Marien ♥ Rencontrer Benjamin ♥ Voir pyg ♥ Assister à ma première conférence gesticulée *et* féministe ♥ Être pieds nus sous les lustres de l'Hotel de Ville ♥ Assister à une conférence sur les illusions d'optiques ♥ Croiser Marie ♥ Croiser Marie-Cécile ♥ Voir Mélia ♥ Discuter avec Thomas, Claire et Noémie ♥ La conférence sur "l'entreprise libérée" et les questions qu'elle suscite ♥ La discussion à coeur ouvert du samedi soir ♥ Le repas sur les quais de samedi soir ♥ La salle d'allaitement MiXiT ♥ Comprendre à peu près les conférences en anglais ♥ La vélotypie de MiXiT ♥ Le point "écriture inclusive" de la conférence de Marie ♥ Écouter une conférencière bègue et racisée dans le grand amphi ♥ L'amphi qui s'appelle Ada Lovelace

## Peines
Le bouchon du samedi midi où presque tout était carné ★ La chaleur ★ Les graviers qui font remettre les chaussures ★ Ne pas avoir eu de discussion post-ma-conf ★ La fatigue ★ Les conférences qui pronent l'innovation ★ Ne pas avoir appliqué la loi des deux pieds ★ Ne pas voir la conférence Dégooglisons ★ Avoir raté l'horaire de ma propre conférence


# SudWeb à Anduze - 25 & 26 mai

## Joies
Aller jusqu'à Anduze ♥ Faire le trajet dans la voiture de Julien ♥ Que Aurélie vienne ♥ Que Marien vienne ♥ Que Julien vienne ♥ Que Fabien vienne ♥ Voir Julia ♥ Voir Éric ♥ Aller à pieds jusqu'aux conférences ♥ L'immense salle pour les conférences ♥ Pouvoir aller et venir dans la salle facilement ♥ Ne pas assister à toutes les confs ♥ Suivre ses deux pieds <3 ♥ Rester dehors pour discuter ♥ Voir Raphaël ♥ Que David me demande des nouvelles ♥ Faire du tricot ♥ Faire du pain ♥ Ne pas devoir penser au trajet pour rentrer le soir ♥ Manger végétarien ♥ Être pieds nus ♥ Discuter en off le dimanche avec Noémie ♥ Passer du temps avec Julien & Aurélie ♥ Dire "Je ne sais pas" à une conférence ♥ Assister à une conférence qui parle de handicap ♥ Assister à une conférence qui parle d'homoparentalité et de YouTube ♥ Se reconnaître dans l'impro théatrale du matin ♥ Les gommettes qui disent qu'on ne veux pas être pris·e en photo ♥ Les gommettes qui disent qu'on est pas à l'aise pour discuter ♥ Le formulaire d'inscription qui demande les pronoms que j'utilise ♥ Le soin mis à faire attention à chacun·e dès l'inscription ♥ Les places à tarif réduits ♥ Le nombre de places à tarif réduit ♥ Les hommes qui se taisent et écoutent à l'atelier sur le consentement ♥ Le concept de planète de Clémentine ♥ Le bruit des oiseaux ♥ Tamponner de totoro les consentant·e·s ♥ Recouvrir un bras de totoro ♥ Dessiner comme un rondoudou ♥ La journée de forum ouvert ♥ Les discussions du soir ♥ Les couvertures du gite ♥ Entendre Claire parler de punks ♥ Voir d'autres personnes marcher pieds nus

## Peines
Être mal à laise dans la séance d'introduction ★ Ne pas suivre mes deux pieds pour sortir de cette intro ★ Se reconnaître dans l'impro théatrale du matin ★ N'avoir discuté avec presque personne d'inconnu ★ Ne pas avoir assisté à l'atelier sur la parole et le genre ★ Les hommes qui pensent parler de consentement ★ Ragequitter l'atelier sur le consentement plutôt que d'exprimer ma colère ★ Le froid et l'humidité du soir ★ Se lever le dimanche matin et ne pas voir son oeuvre ★ Les personnes qui partent le samedi soir / dimanche matin tôt

# Framacamp à Matour - 4 au 8 juin

## Joies
Pouvoir venir ♥ Traverser la France pour rejoindre le gite ♥ Dormir chez pyg ♥ Rencontrer le chat de pyg ♥ Rencontrer les membres de Framasoft ♥ La diversité de personnalités ♥ Voir de nouvelles personnes ♥ La couleur de la chartreuse ♥ Animer mes premiers ateliers (et adorer ça) ♥ Organiser un fishbowl au pied levé ♥ Quitter le fishbowl sur l'UX après quelques minutes ♥ Les devs qui n'y connaissent rien à l'UX mais qui en parlent très bien ♥ Organiser un "Petites Histoires / Grandes Histoires" ♥ La quantité d'histoires racontées ♥ La diversité d'histoire racontées ♥ La richesse des histoires ♥ La confiance lors de l'atelier ♥ Être pieds nus ♥ Aller au sauna ♥ Aller à la piscine après le sauna ♥ Voir Pouhiou ♥ Recevoir un merci de fla ♥ Discuter dans les chambres ♥ Regarder Age of Empire ♥ Écouter d'une oreille distraite des concepts qui ressortent des semaines plus tard ♥ Parler du logiciel libre ♥ Parler de framasoft ♥ Organiser au pied levé des tests utilisateurs ♥ Benjamin écrivant les retours utilisateurs ♥ Marien qui fait attention à ne rien dire ♥ Luc qui discute avec saon utilisateurice ♥ Écouter du ukulélé ♥ Remercier Nathanaël ♥ Apprendre à faire des gougères ♥ Voir ma conf' en étant accompagnée ♥ Entendre des histoires qui se sont passées avant que je sois née ♥ Être considérée professionnellement sans avoir à se positionner en sachante ♥ Recevoir du soutien lorsque ça ne va pas ♥ Recevoir du soutien maladroit lorsque ça ne va pas ♥ Chaque merci qui m'a été donné en main propres ♥ Garder contact malgré la distance ♥ Que cela dure 5 jours ♥ L'orage le soir des histoires ♥ Les réponses à mes questions ♥ L'aide pour mettre mon folio sur framagit

## Peines
Ne pas croûler sous le temps et l'argent pour améliorer l'ergonomie de tout les projets libres ★ Ne pas être allée davantage en cuisine ★ Ne pas avoir analysé les retours utilisateur·ice·s de Framasoft ★ Partir avant la fin ★ Ne pas comprendre ce qu'il se passe dans sa tête ★ Mettre mal à l'aise la personne avec laquelle je discute ★ Ne pas prendre soin de l'autre aussi bien que je le voudrais ★ Ne pas avoir pu voir les étoiles ★ Voir ma conférence -


# AgileOpenFrance à la Ferme des Cévennes - 3 au 7 juillet

## Joies
Ne pas faire 6h de voiture pour y arriver ♥ Débarquer en plein Poudlard ♥ Rester pieds nus toute la conférence ♥ Faire un forum ouvert sur 5 jours ♥ Voir Manu ♥ Voir Raphaël ♥ Voir Julia ♥ Voir Noémie ♥ Voir Thomas ♥ Rencontrer des tas de nouvelles personnes ♥ Faire mieux connaissance avec Philippe ♥ L'atelier sur l'écoute empathique ♥ L'atelier en non-mixité ♥ Ce que j'ai appris à cet atelier ♥ Parler ouvertement de féminisme et de non-mixité sans anicroche ♥ Parler avec David ♥ Écouter Julia ♥ Prêter un livre à Noémie ♥ Avoir posé "Trop intelligent pour être heureux" sur une table et voir toutes les réactions que cela amène ♥ Manger végétarien ♥ Voir et parler à Bénédicte ♥ Parler anglais et comprendre mon interlocutrice ♥ Assister à un atelier où l'intervention des hommes est réglementée ♥ Les conversations en tête à tête ♥ Parler de lave-linge ♥ Parler avec Laurent ♥ Parler à coeur ouvert des craintes de l'indépendance ♥ Parler des angoisses de l'itinérance ♥ Se décider sur l'itinérance ♥ La sororité ♥ Parler à une assemblée mixte des protections menstruelles ♥ Parler boulot facilement ♥ Prendre le bus avec tout le monde ♥ Manger au restaurant avant de se quitter ♥ Prendre rendez-vous pour se revoir ♥ Parler de sieste ♥ Faire la sieste ♥ Aller voir les ânes ♥ *Comprendre ce qu'il se passe dans sa tête* grâce à Manu ♥ Faire connaissance avec Thomas ♥ Rencontrer encore plus de boîtes qui vivent leurs valeurs ♥ Rencontrer des meufs ♥ Avoir parlé d'identités LGBTQI+ et avoir compris des concepts ♥ Chanter le dernier soir ♥ Écouter Julia chanter


## Peines
Mon niveau d'anglais qui m'empêche d'avoir accès aux subtilités ★ Arriver à la fin de l'atelier sur la parole et le genre ★ Les mouches qui empêchent de faire la sieste ★ Mettre 2 jours à suivre ses pieds ★ Être impressionnée par le monde ★ Ne pas avoir regardé Aggretsuko ★ Ne pas avoir mis de chaussures pour aller voir les ânes

# Fédérathon à Fougaron - 14 au 17 juillet

## Joies
Avoir rencontré plein de nouvelles personnes ♥ Se sentir isolée à moins de 2h de Toulouse ♥ Rire dans la voiture ♥ Manger vegan tout le séjour ♥ Parler d'aspects philosophiques de la fédération ♥ Comprendre ce qu'est la fédération ♥ Pouvoir partir des problèmes utilisateurs pour aller sur des solutions techniques ♥ Faire des tests utilisateurs semi-guidés ♥ Avoir le droit à des jeux de mots quasi systématiques ♥ Faire découvrir Charlie The Unicorn ♥ Regarder les étoiles ♥ Isa qui raconte les étoiles ♥ Dormir sous les toits ♥ Avoir 50% de non-mangeureuses de viandes ♥ Ne pas être seule à ne pas boire d'alcool ♥ Entendre parler Esperanto ♥ Faire la blague du tunnel ♥ La balançoire ♥ Sketchnoter ♥ Ne pas sketchnoter ♥ Faire la cuisine ♥ Ne pas galérer à décider pour les repas ♥ Organiser un fishbowl ♥ Réaliser qu'on fait de l'agisme ♥ Se faire surprendre par la pertinence de Bat' ♥ Voir Thomas ♥ Ne pas se coucher trop tard (grâce aux couches-tôt) ♥ Faire des câlins au moment de partir ♥ Parler avec Isa ♥ Voir Isa intervenir dans les débats ♥ C'est aussi ça la fédération ♥ Le bot Fédération ♥ Montrer comment s'attacher les cheveux avec un stylo

## Peines
Ne pas avoir anticipé le non-accès au réseau ★ Ne pas réussir à gérer les moments de latence
