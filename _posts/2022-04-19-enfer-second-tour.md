---
layout: post
title: "L'enfer du second tour"
subtitle: ""
date: 2022-04-19 10:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blog/enfer-second-tour/
---

Salut à toi qui me lis :)

Bon, on est pas bien là, à devoir jouer à "Facho et Fasciste sont sur un bateau" ?

Je suis dé-goutée. Je ne pensais pas avoir mis autant d'espoir dans ces élections, mais vu la façon dont je me suis effondrée, en larmes, en voyant s'afficher la tête du binôme du premier tour, il faut croire que si.

J'ai beaucoup pleuré, les jours suivants. J'oscillais entre le désespoir profond de devoir me taper 5 ans supplémentaires de lutte acharnée, et la colère sourde (ou vive) contre celleux qui avaient voté pour le président sortant.

J'ai mille dégoûts qui me donne envie de tout jeter par la fenêtre et d'aller hurler devant un bureau de vote. Qu'on remette au pouvoir quelqu'un qui a passé son temps à piétiner la totalité de la population, et à jouer à copain-copine avec l'extrême droite, avec des dérives autoritaires et un mépris de tous les enjeux de société, ça me fume.

Alors ça fait 3 mois que je dis : "Si le second tour se joue comme en 2017, ça sera sans moi". Parce que j'en peux plus de leurs magouilles, de la fausse surprise "Oh ben tiens dis donc un second tour comme 2017, on s'y attendait pas du tout" savemment orchestré à la fois par la presse des grands médias, par les politiques en place, et même accompagné par certains des supposés "camarades" de gauche qui ont passé un temps démentiel à taper sur l'Union Populaire plutôt que de se concentrer sur les autoritaires et les fascistes (mettez ceux que vous voulez dans cette case).

Si vous saviez comme j'ai les nerfs.

Et pourtant je vais y aller.

Je vais y aller et voter Macron.

Parce qu'entre un candidat qui vire autoritaire une fois au pouvoir, et une candidate qui s'annonce d'office autoritaire avant même d'avoir atteint le fauteuil, je fais un choix.
Je n'ai aucun doute qu'un second mandat Macron, se sera pire que le premier. Parce que cet abruti va être en roue libre, ne plus se sentir puisqu'il aura été réélu sans lever le petit doigt (franchement même quand il essaie de draguer la gauche c'est totalement pitoyable, le pire des exs qui essaie de vous reconquérir n'est pas à ce niveau), et continuer ses dérives autoritaires.

Mais Lepen c'est pire. Non seulement elle, mais tous les dégats qu'elle va faire au peu de contre-pouvoirs qu'on a (sur lesquels on n'a déjà pas trop de mal à s'asseoir) , et surtout tous les groupes d'extrême droite qui ne vont plus se sentir et réaliser des ratonnades envers tous ceux et celles qu'ils ne pourront pas voir en peinture (je vous fais pas une liste des minorités concernées, j'imagine que vous l'avez déjà bien en tête).

Et enfin, parce qu'il a beau être atroce ce président, les élections qu'on a eu, on n'a pas eu à les négocier. Il les a méprisées, certes (comme d'hab) mais il les a pas dynamitées.

Et je ne supporterai pas qu'en plus de nous faire une Trump, on soit dans l'enfer de ne plus pouvoir en sortir parce que les prochaines élections seraient empêchées, comme la tentative d'invasion du Capitole qui a quand même fait plusieurs morts.

Ce n'est pas un bulletin "pour" Macron, c'est un bulletin pour ne pas que Lepen passe.

Mais, évidemment, rien ne sera plaisant dans cette situation.

De toute façon, on est repartis pour lutter contre l'autoritarisme 5 ans de plus.
Je choisis juste mon ennemi sur la base du moins pire.

Mais ma colère reste grande.

Ils m'ont énervée, ça va chier.

Courage ! <3

PS : Je savais pas où le rajouter, mais je voulais vous dire que lorsque je visualise dimanche soir avec la pire des situations, je me vois en train d'aller sur mon ordi et de frénétiquement supprimer toute trace numérique de mon engagement pour la justice sociale. Et en le faisant, je me dis que toutes mes amies n'auront pas la même connaissance du danger, c'est à dire de la facilité de récupérer des infos personnelles permettant de facilement identifier si l'on est "pour" ou "contre" eux. Eh ben autant vous dire que ça fait pas rêver, cette soirée.

PS2 : J'ai oublié de vous citer [cette vidéo de Médiapart](https://www.youtube.com/watch?v=MCjAbMoY8gw) qui fait très bien le bilan.
