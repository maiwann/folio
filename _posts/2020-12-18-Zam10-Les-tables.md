---
layout: post
title: "Zam 10: Nouvelle version : Les tables !"
subtitle: ""
date: 2020-12-18 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam10-les-tables/
---

# Zam 10: Nouvelle version : Les tables !

Grâce à l'atelier sur [les "zamettes"](/blog/zam9-les-zamettes/), nous avons conçu le fonctionnement de cette nouvelle version de Zam. Je vais vous y faire faire un tour :)

## L'identification

Lorsque les utilisateurs doivent travailler sur Zam, ils accèdent à l'écran suivant lors de leur première connexion. On les invite à indiquer un mail qui nous permettra de les identifier à nouveau…

<figure>
  <img src="/img/posts/2019_Zam/10/1.png" alt="">
</figure>

…puis de rentrer leurs noms et prénoms

<figure>
  <img src="/img/posts/2019_Zam/10/2.png" alt="">
</figure>

Ils peuvent ensuite choisir la lecture sur laquelle ils travaillent

<figure>
  <img src="/img/posts/2019_Zam/10/3.png" alt="">
</figure>

et ils arrivent alors dans leur "espace de travail" composé de 2 choses : L'index et la table

## Au sein d'une lecture : Nouvelle version

Lorsqu'un utilisateur arrive pour la première fois sur sa table, elle peut être vide ! On lui propose alors de se rendre sur l'index (là où se trouve l'ensemble des amendements) pour qu'iel puisse récupérer ceux sur lesquelles iel travaille.

<figure>
  <img src="/img/posts/2019_Zam/10/4.png" alt="">
</figure>

<figure>
  <img src="/img/posts/2019_Zam/10/5.png" alt="">
</figure>

En entrant dans un amendement, on peut le lire, l'éditer (mais seulement si il est sur notre table !) ou le transférer sur sa propre table, pour travailler dessus par exemple !

<figure>
  <img src="/img/posts/2019_Zam/10/6.png" alt="">
</figure>

On a rajouté au fur et à mesure des petits avertissements pour que les personnes ne "volent" pas les amendements de la table de leur collègue sans le réaliser, en transférant un amendement sur lequel quelqu'un serait en train de travailler par exemple.

<figure>
  <img src="/img/posts/2019_Zam/10/8.png" alt="">
</figure>

Et une fois qu'on a sur sa table les amendements sur lesquels on veut travailler, il reste le travail de rédaction à réaliser jusqu'à ce que sa table soit vide. On ouvre un amendement, le lit, rédige la réponse du gouvernement, puis le transfère à la personne chargée de relire et vérifier qu'elle est en accord avec ce qui a été écrit (et ainsi de suite au fur et à mesure des échelons jusqu'à ce qu'il parcoure l'ensemble du circuit de validation !)

## Points particuliers

### On peut voler l'amendement sur la table de quelqu'un ?!

Eh oui ! Le vol d'amendement pourrait être considéré comme un risque où, consciemment ou inconsciemment, un utilisateur pourrait récupérer un amendement sur lequel il n'a pas à travailler, ce qui sortirai l'amendement du circuit de validation, voire même permettrai de faire des modifications malveillantes…

Cependant, ce vol d'amendement est en fait une des grandes forces de l'application ! Car ainsi, aucun amendement n'est bloqué, il est possible à chacun d'être autonome et de récupérer des amendements sans devoir attendre l'aval des autres personnes qui ne sont pas forcément disponibles. Quand aux erreurs, elles sont limitées par l'ensemble des relectures, les avertissements que nous avons rajouté lors des transferts, et par un "journal" d'amendement listant les modifications et leurs horaires. Ainsi on sait qui à changé quoi quand, et si malversation il y a, il sera possible de retrouver la personne l'ayant réalisé (cela dit, jusqu'à présent, aucun cas de malversation n'a été remonté, cela est seulement de l'ordre de l'éventualité).

<figure>
  <img src="/img/posts/2019_Zam/10/9.png" alt="">
</figure>

### Qu'arrive-t-il lorsqu'un amendement a fini son circuit de validation ?

Nous nous sommes heurtées à un souhait, celui d'avoir accès à des tables ne correspondant à "personne", existant seulement pour stipuler qu'un amendement avait été relu par l'ensemble de la chaine de validation.

Nous avons donc créé de faux utilisateurs, appellés "Validé CAB" par exemple (CAB = Cabinet du ministre), qui permettait de symboliser de cet état.

### Où est le mot de passe ?

A ce moment là, dans Zam, il y avait un identifiant et un mot de passe unique pour l'ensemble des utilisateurs, afin d'accéder à l'application. Ensuite, nous demandions seulement aux personnes de s'identifier avec un mail, pour les retrouver lors d'une nouvelle connexion. Mais aucune vérification n'était effectuée, car nous avons privilégié dans un premier temps la souplesse à la rigidité. Il était donc possible, pour peu que chacun connaisse le mail de quelqu'un d'autre, d'usurper son identité, pour le meilleur (débloquer des amendements), comme pour le pire. Il y a du nouveau sur ce point car évidemment l'application ne pouvait pas être généralisée avec ce niveau de sécurité, mais je vous en parlerai dans un autre article !


## Les prochains articles :

- [Le rôle de Mélo & les démos](/blog/zam11-melo-et-les-demos/)
- [Nouvelles observations](/blog/zam12-nouvelles-observations/)
- [Rencontre Zam](/blog/zam13-aix-y-zam/)
- Les lots
- L'authentification
- Drum
