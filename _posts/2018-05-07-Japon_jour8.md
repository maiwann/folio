---
layout: post
title: "Japon Jour 8"
subtitle: ""
date: 2018-05-07 16:00:00 +0200
vignette: null
intro: "Musée Ghibli <3"
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/japon_jour8/
---

# Jour 8 : Musée Ghibli

Réveil du matin sans chagrin, nous allons voir le Musée Ghibli !! Direction Mitaka pour prendre le chat-bus qui nous emmènera jusqu'au musée. je suis impatiente !!!! Dès la gare, un petit totoro nous indique le chemin. Nous avons demandé les tampons de la gare au chef de gare puis sommes sorti·e·s à la recherche du bus ! L'arrêt de bus est de toute façon très reconnaissable et le bus aussi (même si ce n'est pas vraiment un chat-bus, mais en fait il se transforme en chat lorsqu'il arrive près du musée =p )

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_1.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_2.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_3.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_4.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_5.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_6.jpg" alt="">
</figure>

Une fois arrivé·e·s devant, nous faisons un peu de queue avant d'entrer. L'extérieur est magnifique, nous sommes à l'entrée d'un grand parc, et l'on voit que les fenêtres du musée sont des vitraux ! On aperçoit même sur le toit le robot du Chateau dans le ciel :3 Totoro est derrière la "caisse" avec les charbons, et certains élements (comme les chapeaux pour signifier les toilettes ladies/gentleman) mettent dans l'ambiance du lieu. J'ai trop hâte de rentrer à l'intérieur !!!

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_7.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_8.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_9.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_10.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_11.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_12.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_13.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_14.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_15.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_16.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_17.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_18.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_19.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_20.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_21.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_22.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_23.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_24.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_25.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_26.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_27.jpg" alt="">
</figure>

Pour l'intérieur, les photos étant interdites vous ne serez pas beaucoup spoilé·e·s. En résumé, l'intérieur était très poétique et composé d'une multitudes de détails rappelant à la fois les films ou juste l'univers du studio, notamment les fenêtres qui étaient bien des vitraux dédiés aux personnages cultes :3 Pour vous faire un résumé rapide des salles visitées, j'ai commencé par une salle dédiée aux techniques d'animation, avec un box composé d'une multitude de statues qui, avec la lumière saccadée, donnait l'impression d'être animés (alors que ce n'était qu'une succession de sculptures). On a beau savoir que c'est une illusion d'optiqu, c'est très impressionnant ! Ensuite, j'ai eu accès à une salle où les studios Ghibli ont affichés un multitude de dessins préparatoires à leurs films. Des crayonnés aux décors **peints à la main** (on oscille entre le génie et la folie quand on voit la précision des peintures et leur splendeur) en passant par des livres entiers de storyboard, il y avait beaucoup à voir (j'aurai pu y rester des heures). L'exposition temporaire était dédiée à la nourriture, et j'avoue que les dessins et animations de plats et personnages en train de manger sont particulièrement bien réalisées dans ces films. Ensuite j'ai assisté à un court métrage 'Boro la petite chenille' dans une mini salle de cinéma dédiée. Le son était 100% composé de bruitages fait à la voix, ce qui rendait le film était assez étrange, mais j'ai bien aimé. Miyazaki envisage d'en faire un long métrage, nous verrons bien si il y arrive :p

Il était 13h et nous nous sommes retrouvé·e·s devant le café Ghibli pour y manger. Il y avait 1h d'attente mais nous avons décidé de le faire et ça valait le coup. Comme nous avions droit de prendre des photos de notre nourriture, voilà nos plats, tirés des films !

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_28.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_29.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_30.jpg" alt="">
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_31.jpg" alt="">
</figure>

C'était superbe pour les yeux et bien bon à manger :D

Pour finir, nous sommes allées sur le toit à la rencontre de…

<figure>
  <img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_34.jpg" alt="">
</figure>

C'était vraiment chouette de le voir en vrai (et de pouvoir lui faire des câlins !). Je vous remets une couche de photos pour le plaisir :D

<figure>
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_32.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_33.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_34.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_35.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_36.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_37.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_38.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_39.jpg" alt="">
<img src="/img/posts/blog_perso/Japon/jour_8/japon_j8_40.jpg" alt="">
</figure>

Nous sommes tout de même rentré·e·s à 10h30 dans le musée et ressorti·e·s à 17h ! La boutique ne m'a pas donné envie de faire des folies (mais il y a suffisamment de goodies dans les magasins de Tokyo pour me rattraper !).
Pour résumer, il faut absolument y aller et j'y retournerai à coup sur ! C'était beau, beau et beau, on en prends plein les yeux, chaque croquis exposé mériterai une demi-heure à lui tout seul et le musée est généreux sur la quantité de dessins exposés ! (J'aurai voulu m'y faire enfermer)

Pour le reste de la soirée, à la sortie du musée il pleuvait des trombes d'eau, ce qui nous a convaincu·e·s de rentrer tranquillement. Trempé·e·s jusqu'aux os, nous nous séchons et décidons de ressortir pour une soirée Curry + Karaoké ! Si le curry était très bon (nous en avons mangé dans la chaîne Coco Curry), le Karaoké a été un vraie catastrophe. Nous partions confiantes car nous retournions dans celui déjà visité mais nous avons eu le droit à une autre salle, une autre interface, impossible de sous-titrer les caractères japonais en latin, impossible de trouver les chansons d'anime comme la dernière fois et parfois, pour une raison inconnue les chansons ne se lançaient pas. Evidemment, c'était extrêmement difficile de se faire comprendre par le personnel du karaoké, les deux heures que nous avions prévues se sont donc déroulées dans un dépit profond :(

  Mais comme demain c'est Disney Sea, j'espère que se sera mieux (et que mes chaussurs seront sèches !). A plus ! :)
