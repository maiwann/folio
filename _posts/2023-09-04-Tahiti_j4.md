---
layout: post
title: "Tahiti - jour 4"
subtitle: ""
date: 2023-09-04 09:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/tahiti_j4/
---

Aujourd'hui, enfin hier parce que j'étais trop claquée pour vous écrire, c'était Maroto.

La Maroto c'est la traversée de l'île par l'intérieur. C'est l'activité pour laquelle on dépendait le plus de la météo car au centre, il pleut plus facilement (et de la pluie était prévue toute la semaine ou presque).

On est récupérés par notre guide-conducteur de pick-up et on file vers le nord-est de l'île pour prendre la route intérieur. On est sur l'arrière du véhicule, le pickup a 2 bancs avec de la mousse aménagés sur chaque coté et il vaut mieux car ça a remué !

J'aime énormément avoir les cheveux dans le vent lorsque l'on est sur la route. Ensuite une fois sur les chemins, on dépasse une carrière (outch la poussière et les très vieux camions), et c'est la fête des bosses et des creux, et encore, notre guide nous dit que la redescente sera pire.

Pêle-mêle :
- on goute des fleurs violettes, gout champignon de paris,
- on se baigne dans un trou d'eau fraiche
- on voit beaucoup de cascades
- on n'a pas vue d'anguilles aux yeux bleu, dommage :(
- mais on a vu des petits poissons
- le tunnel est surnommé "l'éléphant bleu" et j'étais bien contente d'avoir emmené mon ciré
- le tunnel en question a été creusé à la mano, 30cm par jour par les travailleurs, quel boulot !
- il y a une caldera à Tahiti, comme à La Réunion,
- pas d'oiseaux à part des poules et des coqs,
- par contre du miconia partout, une saloperie invasive que j'arrachais déjà quand j'étais gamine il y a 20 ans. Le guide nous dit qu'elle est parmis les 10 plantes les plus invasives du monde et vraiment c'est la catastrophe. Petits restes de ce que j'ai appris en primaire : les racines sont à la surface et captent l'eau, les feuilles très larges ombragent les autres plantes et les graines très nombreuses se dispersent aux 4 vents. Quelle misère :/
- il y avait possibilité d'être debout sur les sièges, la tête au dessus du pick-up. J'ai pas trouvé l'énergie pour me lever entre les phases de pluie, tant pis.

Notre guide nous a expliqué aussi que la température de l'eau change à Tahiti, et qu'ils commencent à voir des orques et même des cachalots par ici, en plus d'une recrudescende de requins tigres. (bon on passera le moment où il a expliqué à 2 autres touristes qu'il n'y avait pas de réchauffement climatique vu que les courants se refroidissaient, ce à quoi les 2 autres mecs ont opiné en balançant 4 conneries climatiques à la seconde, je n'ai pas donné mon avis comme on ne me l'a pas demandé :p )

Le retour nous montre les bouchons quotidiens de Papeete, et vraiment j'ai hâte qu'on ai enfin partout des plans de mobilité sérieux parce qu'ici c'est vraiment la teuf du "tout voiture".

Enfin, c'était une chouette journée, j'ai bien aimé être assise sur des ressorts tout du long grâce aux bosses. Par contre avec les lunettes du réchauffemnt climatique, ça ressemble plutôt à une aberration de faire une journée pick-up (surtout qu'évidemment il y en avait une dizaine, on n'était pas seuls).

J'ai filé me coucher à 20h, autant vous dire que même les fesses sur le siège ça nous a fatigué :p 
