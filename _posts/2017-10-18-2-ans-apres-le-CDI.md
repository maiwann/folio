---
layout: post
title: 2 ans après le CDI
subtitle: ""
date: 2017-10-18 14:00:00 +0200
vignette: null
intro: "La semaine dernière, Facebook m'a proposé un «souvenir d'il y a 2 ans» qui correspondait à un de mes statuts de 2015 où j'annonçais officiellement mon recrutement dans la boîte où j'avais fait mon stage. Ça m'a fait sourire, car ma situation actuelle est à 1000 années lumières de ce statut…"
nom_blog: le blog
which_blog: blog_pro
permalink: /2-ans-apres-le-CDI/
---

La semaine dernière, Facebook m'a proposé un "souvenir d'il y a 2 ans" qui correspondait à un de mes statuts de 2015 où j'annonçais officiellement mon recrutement dans la boîte où j'avais fait mon stage.

<figure>
  <img src="/img/posts/2017_10_18_2_ans_CDI/post_facebook.png " alt="Je suis officiellement embauchée à Toulouse en tant que conceptrice/designer graphique d'IHM o/ :D">
  <figcaption> Un vieux post plus vraiment d'actualité </figcaption>
</figure>

Ça m'a fait sourire, car ma situation actuelle est à 1000 années lumières de ce statut, et je me suis dit que c'était le bon moment pour faire une petite rétrospective de toutes mes pérégrinations.

C'est parti ! Attention c'est un peu long mais j'ai le droit, c'est mon blog ;)


# Le premier emploi

Au fur et à mesure des différentes années d'études se cumulent différents types de pression :
- La pression de ne pas être un·e bon·ne professionnel·le,
- La pression de ne pas trouver ce que tu aimes faire (spoiler les deux vont souvent ensemble),
- La pression de ne pas trouver d'emploi (il parait que c'est la crise),
- La pression de tomber dans une boite pourrie (il parait que c'est toujours la crise),
- etc, etc.

Autant vous dire que venu le moment du stage de fin d'études, la sensation d'arriver au boss de fin de jeu est là. Il faut non seulement trouver un stage, mais en plus qu'il ne soit pas trop pourri, sinon on commence notre ascension dans "le monde des grand·e·s" en ratant la première marche.

Pour ma part, j'ai par chance (vous verrez par la suite que je suis assez chanceuse mine de rien), trouvé assez rapidement mon stage (ce qui était déjà un stress d'évité que de ne pas être dans les derniers à ne pas avoir d'entreprise). Et c'est à la fois ravie et complètement flippée que j'ai déménagé de Limoges à Toulouse pour étudier les IHM (aka Interfaces Homme-Machine) en tant que designer, dans une boîte qui avait visiblement tout pour me plaire.

Par chance, cela a été le cas : une bonne ambiance d'équipe, des personnes qui savent ce qu'elles font et le font bien, qui sont bienveillantes, qui me donnent de vraies responsabilités sans m'en demander trop, qui collaborent… Mes 6 mois de stage se passent et je suis ravie de tout ce que j'apprends, les projets sont intéressants, les méthodes aussi, rien que du positif (pour mon anniversaire on m'a même offert un kigurumi pikachu, mes ex-collègues sont <3)

Comme j'ai aimé travailler avec elleux (et réciproquement), à la fin du stage on me propose de m'embaucher. Autant vous dire que j'étais ravie !

<figure>
  <img src="/img/posts/2017_10_18_2_ans_CDI/pikachu_excited.gif " alt="Pikachu qui est tout en joie">
  <figcaption> Ma tête quand j'ai su que j'allais avoir un vrai job </figcaption>
</figure>

# Le fabuleux monde du travail

Le moral au beau fixe, je commence à travailler en tant que salariée. Et même si je vois bien que j'ai parfois l'oeil sur la montre en ayant hâte que ma journée se finisse, je mesure la chance que j'ai d'avoir obtenu ce premier emploi sans vraiment galérer, d'autant plus qu'autour de moi les témoignages d'amies qui galèrent à trouver une boîte se multiplient… Nous sommes en octobre 2015 et c'est à ce moment que je poste le statut facebook annonçant mon CDI… Je me projette rapidement : quitter ma colocation pour prendre un petit appart mignon, enfin m'installer après 5 ans à déménager tout les 10 mois, commencer des loisirs (= cours de japonais) de façon régulière…

Et puis le temps passe, et au début du mois de décembre, le directeur de la boîte et la designer senior qui me chapeaute me proposent de prendre un peu de temps pour discuter. Nous nous retrouvons tout les trois, et iels me demandent ce que je pense de mes débuts dans cet emploi. Immédiatement sur la défensive, de peur que quelquechose ne me soit reproché (la fin de la période d'essai arrivait à grand pas), je liste l'ensemble des points positifs : l'ambiance de travail idéale, les projets intéressants, le fait que je ne cesse d'engranger de nouvelles compétences, que tout cela me plaise beaucoup. Puis j'attends fébrilement la cause de cette question, qui n'était évidemment pas anodine.

C'est alors que posément, avec beaucoup de douceur, iels m'exposent leurs questionnements. Il leur semble que, malgré ce que je viens de dire, je ne suis pas aussi bien que ce que j'en dis. Que je m'ennuie, que je serais mieux ailleurs… Je me souviens très nettement de la dernière phrase de la designer, qui a avancé : "Si tu es là mais que tu ne penses qu'à ce que tu va faire le week-end… ce n'est peut-être pas la peine…"

Comprenons-nous bien : ces mots sous-tendent qu'il existe un lien à ne pas dissocier entre vie professionnelle et personnelle. Cette façon de penser peut être d'une dureté effroyable pour quiconque cherche un travail alimentaire ou tout du moins pas un travail-passion, dans lequel iel refuse de s'engager plus que nécessaire, et permettant de se rémunérer pour pouvoir vivre le reste de sa vie comme iel l'entends. Je n'ai pas eu une seule fois la sensation que cela ai été le cas ici, sans doute parce que je ne vois mon travail que comme une composante indissociable de mon épanouissement, et que de ce fait cela n'a pas de sens pour moi d'aller travailler sans en ressentir l'envie. En parallèle, mes ex-collègues étaient réellement à l'écoute de ce qui était le mieux pour moi comme pour eux, sachant pertinemment que travailler avec une collègue démotivée serait à la longue pénible pour tout le monde.

Enfin bon, on me propose de réfléchir à cette discussion, et d'en rediscuter quand je le trouverais nécessaire. Il était à peu près 16h et dans mon cerveau c'était :

<figure>
  <img src="/img/posts/2017_10_18_2_ans_CDI/404notfound.gif " alt="Page 404 avec John Travolta">
  <figcaption> Heeeeeeeey………………Y'a quelqu'un ? </figcaption>
</figure>

Le soir même, je m'écroule en pleurs. Je pense avec le recul qu'en une discussion, ce que je ressentais au fond de moi a été formulé. J'ai une chance immense d'avoir un emploi. Je peux prendre un appartement derrière. Cet emploi est intéressant. Les collègues sont super. L'ambiance de travail est bienveillante.

Est-ce que ça me suffisait ? *Non*

Je vous assure que j'ai essayé de me raisonner. Je me suis traitée d'inconsciente, que face à cette chance inouïe d'avoir un emploi idéal, il était inconsidéré de ne serait-ce qu'envisager de partir. Et pourtant, à l'instant où j'essayais de rationnaliser, mes propres émotions me submergeaient et me hurlaient de démissionner. Que je ne pouvais pas rester, car quelque chose n'allait pas. Quelque chose *ne m'allait pas* dans ce travail.

 La prise de conscience était trop forte et il n'y avait rien à faire. Tout ce temps où je me suis convaincue qu'il y avait une attitude professionnelle à adopter et que ce n'était qu'après que je pouvais enfin apprécier de vivre pleinement et de profiter de mon temps libre. Tout cela venait de voler en éclat.

2 jours plus tard, j'annonce ma décision : je dois partir. Je le sens au fond de mes tripes, je ne peux plus rester ici maintenant que je *sais* que cela ne me convient pas. "Où est-ce que tu pars ?", "Pourquoi est-ce que tu pars ?". Je ne sais pas.

Peut-être que je ne fais pas assez de web, que je n'apprécie pas de bosser sur des projets pour l'armée car cela touche mon éthique, que je préférerais être seule à faire du design pour pouvoir m'organiser en toute autonomie malgré mon inexpérience… Je ne sais pas pourquoi et en même temps, *je le sens*, il faut que je parte. Et cela me soulage et me terrorise simultanément.

<figure>
  <img src="/img/posts/2017_10_18_2_ans_CDI/frozen.gif " alt="Elsa de la Reine des neiges qui galère dans le blizzard">
  <figcaption> Que fais-je… Ou vais-je ? </figcaption>
</figure>


# Belote et rebelote

Après mon annonce, il m'a été proposé de rester encore un peu, pour me laisser du temps afin de trouver mon nouvel emploi (je vous avais dit qu'iels étaient cools ?). Me voici partie à la recherche du saint-graal : un CDI, dans le web cette fois car je suis convaincue que ça collera mieux avec ce que je veux faire ( Du responsive \o/ ). Je suis terrorisée à l'idée de passer des entretiens d'embauche et de devoir me justifier à propos de ma démission, mais il faut bien en passer par là…

Dans les mêmes temps, je me demande si je ne ferais pas mieux de revenir au seul emploi qui m'ai jamais fait vibrer, celui d'animatrice en centre de loisirs. Vous savez, ce boulot qui est dédié à être un "job étudiant", mal payé et mal considéré ? Le seul pour lequel j'aimais me lever le matin et pour lequel j'en redemandais à la fin. Je pense que comme il n'y avait pas la pression de devoir avoir l'air "professionnelle" ou de devoir construire une progression dans le temps, comme j'avais une immense autonomie et des responsabilités importantes, comme j'étais au contact direct des enfants et que je pouvais leur permettre de passer une journée agréable, ce métier me convenait parfaitement. La précarité qui y est liée m'a cependant (pour l'instant) amenée à y renoncer.

Je me lance donc dans une recherche d'emploi en tant que UX Designer. Contre toute attente, et même si j'ai peu de rendez-vous, je décroche rapidement une proposition. Encore une petite boîte (tant mieux), où je serai cette fois la seule designer en collaboration étroite avec les développeurs, et où je pourrai mettre en place ce que je veux pour développer la partie Expérience Utilisateur (= mon dada). Banco, je fonce, ravie de pouvoir enfin faire du web et ayant hâte de me prouver à moi-même que je peux aussi être une Adulte-Qui-Contribue-À-La-Société-En-Travaillant.

Pourtant, rapidement, les symptômes réapparaissent, et de façon bien plus marquée. Je suis mal à l'aise la plupart du temps, regarde l'heure quand je suis au travail, ne réfléchis qu'à ce que je vais faire en dehors de mon temps au bureau… Et si je pense sans cesse au design c'est pour créer et concevoir lorsque je rentre chez moi, alors que je suis lessivée de ma journée de salariée. Plus le temps passe, plus je suis déprimée de ne pas réussir à me conformer à ce mode de travail. Et je ne comprends toujours pas ce qui cloche.

# De l'importance des conférences

Le plus déroutant dans tout ça, c'est que je continue à aimer le design et le web. J'y pense tout le temps, et je prends sur mon fameux temps libre post-boulot pour me jeter à corps perdu dans les livres sur le sujet, allant aux apérowebs et autres conférences, quitte à y laisser de l'argent et des jours de congés. Mais alors qu'est-ce qui cloche ?

J'aborde le problème à SudWeb en avril 2016, car après la conférence de <a href="https://vimeo.com/172225264"> Roxane-Debruyker - Questionnements d’une étudiante </a> qui résonne en moi, j'ai besoin d'en parler à coeur ouvert. Je propose une informelle "J'aime mon travail mais…" et les retours de travailleurs en mal-être sont nombreux. Au cours des discussions, je repère chez les personnes qui revendiquent être heureuse au travail un point commun significatif : iels sont tous indépendant·e·s ou ont monté leur propre boîte…

A SudWeb aussi, je rencontre Stéphane, qui me parle de sa façon de travailler en tant qu'indépendant, et me raconte qu'il va bientôt à une non-conférence, un DevOpenSud… Très motivée pour réussir à voir à nouveau de chouettes personnes et à approfondir ces façons de travailler, je me motive à y aller. Je me retrouve au milieu d'indépendant·e·s et me laisse porter par leurs façons de voir le travail:
- Les conférences ? Du travail.
- Le temps pour faire de la veille et te former ? Du travail.
- Le temps passé à DevOpenSud à discuter avec toutes ces personnes ? Du travail.

Notre métier fait partie de nous, et dire qu'il y a une séparation nette entre le temps de travail et le temps personnel, ce n'est pas réaliste. Et je suis *tellement* en accord avec ça ! J'entends enfin une façon d'envisager le travail qui résonne avec ce que je ressens, cohérente avec mon incapacité à m'éloigner du monde du numérique et du design, malgré mon dégoût vis à vis de mon emploi.

# La fin du tunnel

J'entrevois enfin une solution à toutes mes questions !

Ce n'est pas que je ne suis pas faite pour travailler. C'est que je suis faite pour choisir de vivre pleinement mon travail et ma vie personnelle sans nécéssairement devoir les dissocier.

Je peux choisir de passer beaucoup de temps à faire de la veille, à lire, à aller à des conférences, sans que cela soit considéré comme du temps perdu mais comme du travail, afin de progresser et de m'améliorer professionnellement. Le temps où je rencontre et discute, débat et découvre, c'est du temps de travail. Le temps où j'apprends à coder, où j'écris des articles, c'est du travail.

Même le temps où je suis en train de caresser mon chat, c'est une partie de mon travail (qui consiste à me reposer pour mieux faire surgir les idées ;) ).

Et je suis folle de joie de découvrir que ce que je ressentais du fond de mes tripes trouve un écho chez d'autres personnes et d'autres modes de fonctionnement. Que l'envie de vivre son travail comme une composante de soi-même, en accord avec ses valeurs et son éthique, ne soit pas qu'un rêve absurde mais une vraie possibilité. Que ça ne sera jamais facile mais que *c'est possible !*

# Et maintenant ?

Après avoir quitté mon dernier emploi en septembre 2016, j'ai donc ouvert mon auto-entreprise et je suis officiellement designer indépendante !

Même si le chemin est long et semé d'embûches (j'en ferai sans doute plusieurs billets de blog), je reste positivement persuadée que je suis sur la bonne voie, et que ma prise de conscience (que je dois en grande partie à mes ex-collègues qui l'ont accélérée) me permettra de me réaliser à la hauteur de mes envies et de mes valeurs. La route est encore longue mais je suis persuadée qu'il y a des choses géniales qui m'attendent au gré de mes errances. *Et je vous avoue que j'ai vraiment hâte de voir ça !*

<figure>
  <img src="/img/posts/2017_10_18_2_ans_CDI/togepi_happy.gif " alt="Un togepi tout heureux qui danse et un pikachu calme mais content">
</figure>

Je vous remercie de m'avoir lue jusqu'ici et vous dit à très bientôt !
