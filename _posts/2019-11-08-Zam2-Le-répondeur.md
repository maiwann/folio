---
layout: post
title: "Zam 2 : Le répondeur"
subtitle: "Un outil pour travailler de façon collaborative sur la rédaction des amendements"
date: 2019-11-07 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam2-le-repondeur/
---

Deuxième billet Zamois… Zamesque… Deuxième billet sur Zam, et cette fois je vous parle de l'autre partie de l'application : le répondeur !

# Zam 2 : Le répondeur

## Tu étais pas sensée travailler que deux jours ?

Avant de commencer, petite précision : En effet, comme raconté dans l'épisode précédent, initialement je devais travailler seulement 2 jours avec l'équipe. Mais c'était surtout 2 jours pour voir si on se donnait le temps de travailler davantage ensemble, voir si ça collait, si tout se passait bien… Bref si c'était cool entre nous !

Et a priori, c'était cool puisqu'on a continué à travailler ensemble (enfin comme ça fait un an on peut dire que ça a bien matché :D)

## Mais c'est quoi le répondeur ?

Dans le [premier article de cette série](/blog/zam1-premier-contact/), je vous ai présenté la visionneuse, qui permet aux ministres et à leurs proches collaborateur·ices dans l'assemblée (les "cab" pour "conseillers du cabinet" de la ministre) d'accéder aux réponses des amendements.

Mais où sont rédigées ces fameuses réponses ?!

*roulement de tambour*

Dans le répondeur bien sur !

### Mais comment ça se passait avant, Jamy ?

Eh bien Fred c'est très simple :

Avant, les rédacteurs et rédactrices récupéraient les amendements sur le site de l'assemblée nationale et rédigaient leurs réponses dans un tableau Excel. Sauf que, pour chaque article de loi il y a un (et parfois plusieurs) bureau compétent (souvent celui qui a d'ailleurs rédigé le-dit article), composé de 4/5 personnes. Et que, une fois ce tableau rempli au sein d'un bureau, il faut le faire remonter le fil de la hiérarchie. En prenant en compte que tout le monde ne prend pas le même temps pour répondre, qu'il peut y avoir des aller-retours de modification, et que tout cela s'envoie par mail, bonjour les fusions de tableur à la main et les versions de fichiers diverses et variées, et donc les risques d'erreur à vérifier à la main !!

### L'idée de Zam, côté répondeur

L'idée donc c'est d'avoir un outil en ligne qui regroupe les rédactions de tout le monde pour pouvoir avoir, à un seul endroit, l'ensemble des réponses aux amendements à jour. Comme ça, quand tout le monde donne son "go", il est possible d'appuyer sur le gros bouton "Imprimer" si on veut un dossier de banc version papier, mais surtout cela met à jour instantanément la visionneuse… Et donc plus besoin d'imprimer au dernier moment, dans l'assemblée, 5 versions du dernier amendement pour le glisser entre la page 1342 et 1344 des 5 dossiers pré-imprimés.

## Premier pas

C'est parti pour un peu de rafraichissement de façade du répondeur :

Pour démarrer, je m'occupe de l'index. Ce qu'on appelle l'index, c'est la looooongue liste de tous les amendements, à partir de laquelle les rédacteurs vont pouvoir filtrer selon l'article dont ils s'occupent, puis cliquer sur un amendement pour y rédiger une à une les réponses.

Quand j'arrive sur le projet, l'index ressemble à ça :

<figure>
  <img src="/img/posts/2019_Zam/02/zam_0.png" alt="">
</figure>

On a dans l'entête de la page :
- Le titre du projet de loi
- A quelle lecture on en est : le texte passe en commission, puis à l'assemblée, puis au Sénat…
- Un accès aux options avancées,
- Un *énorme bouton* pour visualiser le dossier de banc (aka la visionneuse, qui fait [l'objet de mon 1er article](x))
- Depuis combien de temps date la dernière mise à jour

Et ensuite, dans le contenu lui-même de l'index :
- Une petite étoile pour marquer certains amendements (comme étant à surveiller)
- Une colonne article, filtrable,
- Le numéro d'amendement, (qui est un identifiant car tel quel il ne parle à personne, le numéro dépend simplement de l'ordre dans lequel les amendements ont été déposés),
- L'objet, un résumé de leur contenu,
- L'auteur et son groupe
- Le sort (c'est à dire, est-ce qu'il a été considéré comme hors sujet par l'assemblée et donc retiré)
- L'avis, avec un lien pour remplir cet avis et la réponse correspondante (est-ce que le gouvernement est favorable, défavorable…)


Je commence par questionner Mélo sur les priorités dans tout ça : Le bouton le plus visible est celui pour accéder au dossier de banc (qui d'ailleurs pour moi devrait être un lien). Mais est-ce l'action principale ?
Ce n'est pas le cas. Ici, soit on cherche ses amendements pour les rédiger, soit on surveille l'avancement de l'ensemble des amendements. Si il y a un grand nombre d'amendements qui ne sont pas remplis alors que la date de la séance approche, on commence à transpirer un peu et à vérifier pourquoi ça n'avance pas côté coordinateurs, les personnes en charge de la …coordination des rédacteurs pour que le dossier avance et soit prêt en temps voulu. Priorité à la supervision de l'avancement des amendements et à la rédaction de leurs réponses donc.


Je récupère le style graphique appliqué dans la visionneuse, je tente de l'appliquer à l'index et j'en profite pour mettre certaines choses en avant (et d'autres moins).

L'élément le plus important, c'est le tableau, qui doit pouvoir être parcouru rapidement afin de visualiser l'avancement de l'ensemble des amendements. Un des indicateurs de cet avancement, c'est le fait que la réponse ai été remplie ou non, et donc l'avis changé.

C'est l'avis qui a le plus d'importance, que se soit pour savoir si il a été rempli ou parce que justement l'action va être faite, en cliquant, de le remplir. Le reste sont des informations plus secondaires.

Je m'efforce donc de réduire les autres informations, et de simplifier globalement le contenu du tableau :
- Le nom de l'auteur et son groupe sont rassemblés
- On enlève les icônes pour les remplacer visuellement par des liens (bleu **et** soulignés) (toujours doubler une information donnée par la couleur pour l'accessibilité)
- On met un bouton pour afficher/masquer les filtres, plutôt qu'ils se trouvent juste à coté des labels et soient tout petits.

Voilà le résultat après rafraichissement, malheureusement je n'ai qu'une capture d'écran du tableau avant que les rédactions commence, il semble donc bien vide…

<figure>
  <img src="/img/posts/2019_Zam/02/zam_1.png" alt="">
</figure>

Mais croyez moi sur parole, avec la colonne "Objet" remplie ça devient vite beaucoup plus dense !

D'ailleurs, l'objet est un casse-tête ergonomique, qui va nous suivre (et nous suit encore) tout au long de Zam

## L'objet, quel est le problème ?

L'objet permet aux rédacteurs et rédactrices de connaître, rapidement, le contenu d'un amendement (c'est comme l'objet d'un mail, tout bêtement).

Sauf que, dans notre index, nous avons peu de place en longueur, et ça vaut aussi pour l'objet. Ce qui signifie qu'un objet rempli est un objet sur plusieurs lignes, donc change la hauteur des lignes en les rendant plus ou moins hautes…

Or, un tableau avec des lignes homogènes permet d'être scanné plus facilement ce qui est, à l'origine, le but de cet index. Comment gérer ces contraintes vis à vis de l'objet ?

### Solution n°1 : Limiter le nombre de caractères

Nous l'avons envisagé, mais limiter le nombre de caractères reviendrait à avoir un objet si court qu'il n'aurait pas d'intérêt.

### Solution n°2 : Ré-interroger le format tableau

Après tout, le tableau est souvent un choix de "facilité" qui finalement nous enquiquine plus la vie qu'autre chose. Pourquoi pas un format par "cartes" qui permettrait plus facilement de mettre un contenu plus conséquent ?

Le souci c'est que si nous légitimons dans la conception de l'outil que chaque amendement ne prendrait non pas une hauteur d'une ligne, mais de bien plus, nous multiplions d'autant la hauteur de page. Pour scanner une liste de plus de 1000 amendements, ça ne fonctionne pas très bien. Le tableau est donc le meilleur choix jusqu'à présent pour une lecture "en diagonale" de la totalité des amendements.

### Solution n°3 : Supprimer l'objet

C'est tentant de supprimer tout simplement cette colonne, mais si nous le faisons les utilisateurs vont prendre des petits papiers pour noter à la main l'objet et le numéro des amendements (oui c'est du vécu !). Ce n'est donc pas satisfaisant non plus, même si très tentant coté simplification d'interface !

### Solution choisie : On ne touche à rien

Et donc après moults discussions entre nous, nous choisissons de ne pas toucher à cet objet, mais ce n'est que partie remise ! L'interface va évoluer et nous nous saisirons du souci à ce moment là. Car il nous apparait de plus en plus clairement que l'index sert à deux choses en simultané :

- Pour les coordinateurs, avoir une vue d'ensemble sur le fait qu'aucun amendement n'est trop à la traîne.
- Pour les rédacteurs, voir de quels amendements ils doivent s'occuper, et les retrouver facilement une fois qu'ils les ont envoyés à la validation de leurs supérieurs ! (Sans l'objet, ça ressemblerai à : "Alors l'amendement sur la taxe sur les serviettes de bain… c'était le 1234 ou le 853… (ouvre le 1234) Ah non ce n'est pas le 1234… (ouvre le 853) ah non plus… bon ce devait être le…" et ainsi de suite)


## Et maintenant ?

Maintenant qu'on a un peu toiletté le répondeur, et que la date de la première utilisation arrive (Notre 1ere prod' a été le Projet de loi de financement de la Sécurité sociale 2019, suivi du Projet de loi de finances, qui reviennent annuellement en octobre), nous nous heurtons à un nouveau souci, important à régler rapidement : Sur la visionneuse, le chargement est lent. Très lent, trop lent, car charger 1800 éléments, ce n'est pas rien, et améliorer les performances coté technique ne suffit plus. On va donc [chercher une solution coté ergonomie !](/blog/zam-circuler-au-sein-visionneuse/)
