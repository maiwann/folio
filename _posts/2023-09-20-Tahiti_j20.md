---
layout: post
title: "Tahiti - jour 20"
subtitle:
date: 2023-09-20 09:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/tahiti_j20/
---

Ce matin, l'angoisse est haute.

Je pense à la notaire et je suis stressée, le genre de stress pourri lié à une impression de non respect de l'autorité.

Bon en vrai, je pense surtout que je suis anxieuse et que l'anxiété se fixe sur n'importe quoi.

J'ai pas envie d'aller aux baleines, je sais pas pourquoi.

On y va.

Bon eh je vous le dis tout de suite, de mon point de vue, ça a été un moment pénible.

Déjà parce qu'on arrive à un centre de plongée avec beaucoup d'allers-retours, beaucoup de touristes qui viennent louer des kayaks, on nous accueille un peu froidement (enfin bref y a pas trop l'time quoi). On est entre blancs, et c'est un peu la gêne.

Ensuite, on nous demande si on est bons nageurs, et franchement c'est dur à dire… avec des palmes, la nage n'a rien à voir avec celles d'une piscine par exemple.

On monte sur le bateau, et on fait étape au spot des requins raies. Le guide nourrit les raies, mauvais point.

Ensuite, on nous indique les consignes de sécurité, notamment qu'une fois à l'eau, le groupe ne peut pas se séparer, si quelqu'un veut rentrer tout le monde doit rentrer, et je me dis que c'est le genre de truc qui doit bien mettre la pression si tu ne te sens pas bien dans l'eau : si tu mouftes, tu fais rentrer tout le monde.

Bon cela dit, une fois dans l'eau, on est tellement loin du bateau que je comprends bien l'importance de ce point.

Ensuite, on nous dit que la distance légale, c'est à 100m d'une baleine pour le bateau, à 30m pour les nageurs. Ok c'est noté.

On passe un long moment à chercher les baleines, on voit le dos d'une qui rentre dans la passe, on va au bout de l'île mais rien, on rentre et voit le dos de 2 qu'on réussit à… perdre (!!) alors qu'on reste 30min le temps qu'elles remontent.

Bref, ça s'annonce mal, mais vers la toute fin de la matinée on en revoit 2 (toujours une mère et son baleineau). On se met à l'eau, et on s'éloigne du bateau. Et là, mon enfer personnel commence : pour celleux qui n'aiment pas la foule, notamment le fait de se passer devant/ se faire un peu toucher-pousser par les autres, je vous présente le level au-dessus : le groupe en pleine mer, avec des palmes et un peu de houle.

Je me suis pris des coups de palmes (de ma mère notamment, faut dire que j'étais juste en train de me calmer), ce qui est extrêmement désagréable. Ensuite il était difficile de voir quelque chose car l'eau était trouble, et enfin lorsque vous essayez de voir quelque chose dans l'eau, ben vous pouvez pas regarder par l'épaule du voisin. Donc si il y a quelqu'un devant vous, vous vous prenez juste ses palmes et vous ne voyez rien.

De plus, on s'est approchés et avons attendus que la baleine remonte, mais clairement le guide nous amenait beaucoup trop près par rapport aux distances qui nous étaient indiquées, nous étions à une dizaine de mètres.

Alors autant vous dire que quand vous ne pouvez pas vous écarter d'un groupe qui vous piétine au sens nautique du terme, et que vous sentez que la distance que vous avez avec l'animal n'est pas le "bon", eh bien moi j'ai trouvé que ça avait tout gâché.

Alors oui, j'ai vu quelque fois la baleine et le baleineau qui ont été très généreux avec nous, nous ont tourné autour, sont restés près de nous, le baleineau a fait des tours dans l'eau…

Mais aucun "waouh c'est magique" qui m'aurait semblé évident vu l'exceptionnalité de la chose. J'étais émue par les dauphins croisés en Bretagne, mais dégoutée par la façon dont ça c'est passé ce matin.

Et en même temps, en allant dans un spot à touristes, j'imagine que j'ai ce que je mérite.

On rentre, on mange (un plat de nouilles immondes, je ne sais pas comment on a pu se foirer à ce point), je vais me coucher, je me réveille et joue un peu avec ma soeur, repas puis écriture d'articles.

Je sens que je vais mieux, mais ce qui s'est passé ce matin ne m'a pas aidé à recharger mes batteries.

Demain, pendant qu'iels vont randonner (ça m'a permis de voir qu'il y a des icônes "cascades" et "traversé de gué" sur OSM !!) je vais squatter à l'éco-musée, j'espère que ça sera reposant, en tout cas je serai seule personne ne m'embêtera :p

En parallèle de tout ça, je lis Mutismes de Titaua Peu qui parle d'indépendance polynésienne et ça cogne fort face aux activités que je fais. C'est bien, ça me fait me sentir blanche, c'est ce qu'il faut.

A demain :)
