---
layout: post
title: "Positiver"
subtitle: ""
date: 2022-03-23 14:00:00 +0200
vignette: null
intro: "Je vous livre, en vrac pour le publier, un échange avec un frama-copain qui m'a fait bien positiver dans un moment où j'ai, à l'approche des élections, une grosse tentation de voir les choses du coté du verre à moitié vide :)"
nom_blog: le blog
which_blog: blog_pro
permalink: blog/positiver/
---

**booteille :**
Et sinon, depuis ce week-end, la dynamique autour de LFI décolle énormément, je trouve !

Toustes mes potes sont motivé·es à aller voter. Mon ancien voisin a convaincu son fils d'aller voter Mélouche.

Sur les réseaux sociaux, les gens qui apprécient ou non Mélouche s'expriment pour dire qu'elles iront voter.

J'pense qu'il faut pas entrer trop dans l'émotionnel pour pas trop s'faire mal en cas de pépin, mais j'y crois de plus en plus !

La mère d'une amie l'a appelée aussi et a changé son discours alors qu'au dernier repas de famille c'était mort de voter Mélenchon. Elle s'est renseignée sur le programme écolo' et lui a dit « j'irai voter pour lui » :

**maiwann :**
Ah ouaiiiis ça me fait trop plaisir ce que tu dis !

Alors quand à l'espoir, j'ai lu un jour un bouquin ou l'autrice disait qu'avant, elle essayait de pas espérer et 1/ ça marchait pas, 2/ les personnes autour d'elle comprenaient pas pourquoi elle était déçue

Du coup maintenant j'espère à fond, j'ai peur à fond, et je serai sans doute très déçue :)

Je crois très fort à un second tour, mais je vomis d'avance au fait que Macron soit réélu parce que franchement je comprends p.a.s. qui le trouve défendable :)

Mais enfin !

Merci :pray:

**booteille :**
Héhé !

Je pense que ça se tient, ce que tu dis !

Concernant qui vote et pourquoi, je me fais pas d'illusion. Sachant comment il est facile d'influencer tout le monde et observant depuis des années à la fois l'échiquier de la politique politicienne et les influences subies par les gens, je ne m'étonne pas des gens qui subissent leur syndrome de Stockholm et retournent à leur ravisseur.

Tous les jours, j'entends les mêmes constructions dans le discours dans la bouche de plein de personnes différentes.

L'exemple du « Je sens pas Mélenchon parce qu'il est colérique »  ou « Il me fait peur » est typique de ça.

Et le fait que les travaux de Noam Chomsky et Edward Hermann montrent comment les médias de masse construisent le consentement montrent bien que ce n'est pas simple affabulation. Surtout avec l'existence de Facebook et YouTube qui sont pensés pour orienter le comportement des gens.

**maiwann :**
T_T

(La meuf qui parle de Cambridge Analytica en conf' mais qui est quand même dépitée quand on lui rapelle qu'il y a de la manipulation :D )

**booteille :**
Je pense que cette élection est une sacrée épreuve pour l'humanité.
J'ai l'impression qu'on est en train de voir les limites du système d'influence et qu'on saura bientôt si l'humanité peut être suffisamment résiliente face à ces influences pour les dépasser.

(Haha ! Je comprends !)

**maiwann :**
Hahaha je suis totalement défaitiste mais on peut être surpris !!

Je crois que j'ai l'impression qu'on se paie une élection Trump pépouze et j'ai envie de hurler :')

**booteille :**
Bah, ça c'était 2017.

**maiwann :**
lalala ça va bien se passer

**booteille :**
Mais les moyens pour faire élire Macron sont bels et bien présents, encore.

Par contre, le peuple français n'est plus le même.

Les nombreuses réformes dévastatrices, la crise climatique approchante, la COVID, les gilets jaunes, le nombre de scandales où Macron affiche ouvertement se moquer du peuple et du pays.

Tout ça sont autant d'événements qui ont contribué à forger la résilience du peuple?.

**maiwann :**
Aaaah j'adore, c'est doux ce que tu dis ^_^

**booteille :**
Tu dois le savoir mais les épreuves difficiles nous permettent, si on y parvient, à nous renforcer et à grandir.

Je pense qu'une partie du peuple a grandit. Une autre partie, encore énorme, est encore fragile et en train de subire?.

**maiwann :**
Doux parce que révolutionnaire ma notion de la douceur est un peu attaquée je crois ^^'

**booteille :**
Mais hé, pas besoin de tout le monde pour avancer. Il suffit juste qu'on arrive à être assez pour insuffler la dynamique et après, l'inertie sera de moins en moins présente !

Haha ! Bah, en même temps, vivement que ça bouge car les défis qui nous attendent sont monstrueux et ce n'est que le premier pas !

C'est une époque passionnante !

**maiwann :**
Roh tu m'enthousiasme c'est trop chouette !!

Je voudrais mettre cette conversation dans mes ressources-liens pour la conf' sur la justice sociale :p
*[Note: voilà c'est fait]*

**booteille :**
Nous avons une chance et une responsabilité incroyable. Nous sommes à la frontière entre deux ères et avons la possibilité de déterminer à quoi ressemblera la prochaine.

C'est à la fois génial et tragique.
Mais toi comme moi devrions nous en réjouir car cela signifie que nos actions aurons bien plus de poids qu'à un autre moment !

**maiwann :**
Ohlala c'est ça moi je vois que le coté tragique !

**booteille :**
Bah, il y a un pan des possibles qui est réellement tragique. Triste. Extinction de l'espèce humaine dans le pire des cas mais je penche plutôt sur le fait que l'espèce s'adaptera et que nos futures générations devront, si ces possibles deviennent, faire preuve d'une sacrée adaptation pour faire face aux défis que nous leur aurons laissé (comme nos ancêtres nous ont laissé la merde dans laquelle nous nous sentons).
Donc ouais, c'est triste de cet aspect là.

Mais moi, j'suis convaincu qu'il existe tout un autre champs des possibles qui ouvrent l'humanité à un avenir radieux. Je pense que notre génération en a la charge. Celle de construire la structure d'une société qui pourra prendre de la meilleure manière qui soit toutes les futures décisions.

Notre rôle, c'est d'apporter une réelle démocratie. Le cadre qui permettra aux futures générations de faire face à tous les défis qui se pointeront à elles.

Haha ! Je suis content que ça te booste !

Franchement, on a tendance à voir ce qui va mal en ce monde. On est éduqué·es comme ça, biberonné·es par les « infos » de nouvelles presque exclusivement négatives.
Mais le monde n'est pas que négativité, au contraire ! Si on se focalise autant sur ce qui va mal c'est parce que ce qui va bien est la norme. Il nous semble normal de manger et boire (dans nos pays, pas dans plein de pays du monde, évidemment), de dormir à l'abris du climat. Mais tout ça, c'est merveilleux !

Quand j'étais plus jeune, j'suis parti en Norvège avec un pote. C'était en plein été. On a acheté une canne à pêche, deux kilos de riz, et on a cherché à se nourrir par nous-même en longeant la côte.

On ne savait pas pêcher. Ça nous a pris cinq jours à sortir notre premier maquereau !
Pour manger, nous devions cueillir des baies, récolter des racines de plantes qu'on ne soupçonnait pas être comestibles (merci les bouquins). Pour boire, demandait aux gens dans les maisons qu'on croisait. Il y en avait régulièrement.

C'était la première fois que je vivais de manière nomade.

Trouver à manger, trouver à boire, trouver où dormir. Ces trois objectifs remplis, nous nous endormions comme des masses. Même si il était encore l'après-midi, épuisés de l'expérience.

J'ai appris pendant cette expérience ce que ça signifie avoir faim et de ne pas avoir à trouver sa nourriture ou à réfléchir comment la conservere.
Sérieusement, le frigo est une sacrée invention mais le supermarché ? C'est un luxe inouï !
J'ai appris aussi ce que ça signifie « ne pas savoir où dormir ».
J'ai appris ce que c'est d'être privé de confort, et encore... dans une certaine limite. Car tout ça, nous l'avions choisi. C'est déjà le plus grand des conforts. Puis nous avions une tente, un peu de riz pour être sûrs de ne pas être trop en difficulté, une canne à pêche. Nous savions que ça ne durerait que trois semaines. Tout ça, ça représente un confort de dingue. Pas que physique, mais aussi mental.
Le pire quand nous subissons une difficulté est de ne pas savoir quand celle-ci prendra fin. Nous, nous le savions.

Tout ça pour dire plusieurs chose.

Nous mangeons à notre faim (sauf pour les plus démuni·es, bien sûr), nous ne réfléchissons pas où obtenir de l'eau potable (le réseau d'eau est un truc de dingue), nous savons où dormir en sécurité...
... Tout ça relève d'un confort incroyable et c'est génial de l'avoir ! Il ne nous reste plus qu'à faire en sorte de le conserver et chercher à ce que toustes puissent y avoir le droit aussi.

Mais aussi... que dans la difficulté, quand tu es plein d'appréhension, le moral c'est la chose la plus importante qui soit.
Tu ne peux pas te permettre de te plaindre. Si tu dois dire quelque chose de négatif, c'est uniquement si l'information est nécessaire.
Que j'ai mal au pied ? Est-ce que je peux y faire quelque chose ? Si oui, je peux en parler pour qu'on traite le sujet. Si non... bah je peux le dire une fois mais ça suffit, pas besoin de le répéter...
... que j'ai faim ? On a rien mangé de la journée, on sait bien qu'on a faim. Pas besoin d'en parler, nous oeuvrons toute la journée pour chercher à résoudre ça.
Que j'ai de l'appréhension ? C'normal, c'est l'inconnu.
Ça fait peur. C'est pas grave d'avoir peur, au contraire.
Il faut juste ne pas se laisser dominer par celles-ci.
T'sais, c'est comme quand t'es en haut d'une falaise et que tu souhaites sauter dans l'eau (ou à l'élastique, si t'as fait l'expérience). La peur, si tu la laisses te dominer, te hurle dans les oreilles et te crie que tu as peur. Ça te paralysie et tu sautes pas... jusqu'à ce que tu réduises le volume. Une peur, ça doit être un chuchotement utile. Qui t'indique qu'une truc ne va pas. Sauter à l'élastique donne l'impression de se suicider, c'est normal d'avoir peur. Mais en raisonnant, en acceptant de se confronter à la source de notre peur, nous pouvons y faire face et décider ou non de sauter.

Et donc, j'en viens à mon dernier point.

C'est formidable. La vie est formidable. L'univers est formidable. Tu t'es déjà posée deux secondes à essayer d'entrevoir toutes les interactions nécessaires au fait que nous puissions vivre ? Ou même bouger la main ? Ou que ce que l'on mange devient ce que l'on est ? Ou à la forme qu'ont les choses, aux couleurs que l'on voit ? À ce que l'on goûte ou entend ? À ce sens merveilleux qu'est le toucher qui fait que l'on apprécie la caresse du soleil ou des êtres aimés ?
C'est juste dingue, merveilleux !
Et pourtant, c'est rare qu'on prenne le temps de s'arrêter sur tout ça, sur la merveille qu'est notre curieuse planète et notre énigmatique univers. Parce que c'est normal de vivre et ça nous semble acquis... mais bon sang que c'est important d'y revenir, de se le rappeler, de le constater de nouveau ou sous une nouvelle forme car ça nous rappelle qu'essentiellement, le monde est beau.

Notre combat a pour but de préserver cette beauté. Notre combat a pour but de la sublimer.

Mais, je pense, qu'il ne faut pas que notre combat soit le prétexte pour l'oublier, cette beauté !
Ce serait bien dommage, t'en conviendras !

*Oui, j'en conviens bien ^_^*
