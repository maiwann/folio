---
layout: post
title: "Japon Jour 16"
subtitle: "Seule"
date: 2018-07-10 15:50:00 +0200
vignette: null
intro: "Seule"
nom_blog: le blog
which_blog: blog_perso
permalink: blogperso/japon_jour16/
---

Pour que vous compreniez comment s'est passée cette journée, j'ai besoin de faire un petit saut dans le temps:

C'est ma dernière journée japonaise, le lendemain nous prenons l'avion. C'est donc le dernier moment pour faire les coins que nous voulions faire dans Tokyo, acheter les derniers souvenirs… Comme le planning de chacun·e est assez chargé, pour la première fois, je m'organise seule pour faire les coins que j'ai envie de faire et surtout, aller au musée Ghibli qui organise une cérémonie en hommage à Isao Takahata, présidée par Hayao Miyazaki(un super réalisateur de dessins animés qui déchirent). Hors de question que je rate une chance de pouvoir le voir, je cale donc ma journée autour de cet événement qui se déroule de 14 à 17h. Donc, le programme: Le quartier traditionnel d'Asakusa le matin, pour acheter de derniers souvenirs en espérant trouver de petits magasins sympas, puis la cérémonie au Musée Ghibli à partir de 14h, avant de retrouver les copines en ville et finir la journée ensemble.

# Le réveil

Je me suis couchée avec un grand enthousiasme pour enfin pouvoir vadrouiller seule et pouvoir prendre toutes mes décisions tout comme j'en aurai envie sur le moment. Et pourtant, au réveil, c'est *l'angoisse avec un grand A.* Venue de nulle part, elle me prend à la gorge et me fait me dire que je ne vais pas sortir de la journée. J'ai peur, mais impossible de savoir de quoi. Je suis juste tétanisée à l'idée de sortir de cet appartement, de sortir d'une zone très confortable où je pourrais regarder des animes tranquillement. Je ne sais d'ailleurs toujours pas ce qui fait que je me suis retrouvée si mal à l'aise, mais le souvenir est encore très marqué 1 mois plus tard. :( Le plus frustrant, c'est le contraste entre mon enthousiasme de la veille, et ma tétanie au réveil.

Je me lève et décide de faire les choses lentement, pour ne pas me brusquer et provoquer une augmentation de l'angoisse, mais pour tout de même faire quelquechose (après tout, c'est le dernier jour !). Je pense au passage que la fatigue et le manque de sommeil ne doit rien arranger ! Petit déjeuner très lent, check des affaires de la journée, puis je sors (quand même :3) en allant tranquillement vers la gare. Je prie pour que les wagons ne soient pas bondés (c'est encore un peu l'heure de pointe) mais ça va. Ouf :3


# La matinée à Asakusa
 Ma ligne de métro ne va pas jusqu'à Asakusa même, mais à une station un peu au sud. Pas grave, je marcherai jusqu'au quartier, ça me fera découvrir de nouveaux coins ! Je descends, et commence à marcher. Sur le trajet je vois des tas de choses un peu marrantes, bien estampillées japonaises (comme ces ratons laveurs/ours/tanuki/? trop mignons en cônes de chantier !) (ou encore ces noms de lieu "inspirés du français")

 <figure>
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_4.jpg" alt="">
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_5.jpg" alt="">
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_6.jpg" alt="">
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_7.jpg" alt="">
 </figure>

 J'ai aussi le droit à un joli petit temple (avec du Saké comme celui de Your Name \o/ \o/)

 <figure>
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_1.jpg" alt="">
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_2.jpg" alt="">
   <img src="/img/posts/blog_perso/Japon/jour_16/japon_j16_3.jpg" alt="">
 </figure>

 Je ne sais plus si j'en ai déjà parlé, mais à Tokyo, mon sens de l'orientation a complètement disparu. Et si à Kyoto, tout allait bien mieux, cela m'a fait oublier que je galérais dans les rues de la capitale. Eh bien ça n'a pas raté, j'ai commencé à tourner en rond, sans réussir à savoir vers où j'allais (j'ai même tenté de m'orienter avec le soleil, pour vous dire !). Au bout de 3 passages devant les tanukis de chantier, je me décide à demander la route à quelqu'un. J'aborde un vieux monsieur dans la rue, qui lui-même demande à une dame dans une boutique, vers où se trouve Asakusa. Bon eh bien même eux ne savaient pas me l'indiquer, le papy m'a répété que l'arrêt auquel je m'étais arrêtée n'était pas le bon (Ouiiii je sais !), et finit par m'indiquer une direction, vers laquelle je devais trouver une rivière que je devrais ensuite longer. Je les remercie, leur dit que j'ai bien compris, commence à marcher et… ne trouve jamais la rivière T_T Je crois que je suis même partie à contre-sens, le monsieur a du vraiment se demander si je l'avais écouté x)

Je continue à errer un peu et finit par tomber sur une ligne de métro qui rejoint l'arrêt d'Asakusa. Comme ça faisait bien 1h que je tournais en rond, je descends dans l'arrêt (puis remonte, car c'était pas le bon coté, mais comment je peux savoir qu'on ne peux pas changer de coté à un arrêt de métro moi ? Le gardien m'a jeté un regard énervé :<), je prends le métro et… j'arrive à Asakusa \o/

Mais une fois sur place, c'est la désillusion : Le quartier est blindé de touristes, je me fait aborder par un tireur de pousse-pousse qui me fait la causette (en français !) (et me parle de Philippe Stark qui a conçu un monument qu'il me montre, une grande flamme) alors que j'ai juste envie d'être tranquille. Bon, je me force un peu, j'essaie de sortir des rues commerciales, mais au bout de 15 minutes j'en ai marre, la matinée a été foireuse à souhait et je veux garder un peu d'énergie pour l'après-midi ! Du coup, je trouve un conbini pour acheter mon repas du midi, et file au métro pour aller jusqu'au Musée Ghibli. J'aurai 3/4 d'heure d'avance mais au moins je serai tranquille !

# Début d'aprem !

Arrivée devant le musée Ghibli, je vois tout de suite un changement d'ambiance, entre notre visite en touristes la semaine dernière et aujourd'hui. Les personnes qui viennent semblent toutes se connaître, pas trop une ambiance triste, c'est déjà pas mal. Il y a même des journalistes ! Je me dirige vers une des personnes ayant l'uniforme du musée, et j'essaie, capture d'écran du site à la main, de faire comprendre que je veux assister à la cérémonie. On m'indique d'aller vers la gauche, et je passe devant de grandes tentes blanches où se tiennent des personnes en tenue très chic. L'une d'elle me parle en anglais (Ouf !), je lui explique que je viens pour la cérémonie (comment dit-on cérémonie en anglais ?), et elle me propose d'attendre dans le parc jusqu'à l'heure de début. Je vais me poser un peu plus loin, observe les personnes qui arrivent… Puis l'hotesse qui m'a proposé d'attendre surgit, et m'explique qu'elle me cherchait car elle m'a donné une mauvaise indication (oui, elle a commencé un tour du parc pour me retrouver ! "Je me souvenais de votre visage" qu'elle m'a dit x) -> Des européennes à sac à dos il ne devait pas en avoir des tonnes ! ), et m'accompagne derrière le musée, où d'immenses tentes ont été installées. A l'intérieur, plusieurs dizaines de rangées de chaises, et un film biographique sur Takahata était diffusé (d'aileurs je me suis un peu fait spoil le Tombeau des Lucioles mais bon). Je remercie mon accompagnatrice et j'attends sagement, assise, curieuse de voir ce qu'il va se passer. Il y a une sacré organisation, je me demande si il y a des entreprises spécialisées dans les évènements de ce genre, en tout cas il y a du personnel pour gérer chaque nouvelle personne qui arrive.

A 14h tapantes, on vient nous chercher et on nous dirige vers le musée. Je suppose qu'il y a une salle de conférence cachée, car nous ne rentrerions pas tous dans la salle de cinéma !

Une fois entrée dans le musée, je découvre qu'ils en ont complètement changé l'aménagement. Ce qui est le plus impressionnant, c'est l'escalier principal, complètement fleuri avec un portrait de Takahata au milieu ! Nous marchons en file indienne, les uns derrière les autres. Une hôtesse nous donne à chacun·e une fleur blanche, et, par groupe de 10, nous nous inclinons devant le portrait, posons la fleur puis pouvons nous recueillir quelques instants, avant de laisser la place aux 10 suivants… Il y a beaucoup de monde, mais comme partout au Japon, tout se déroule de façon très méthodique et organisée. A la sortie de la salle principale, une seconde salle où se trouvent plusieurs livres, dont "Kirikou et la Sorcière"… Est-ce que se sont des ouvrages sur lesquels il a travaillé ? Qui l'ont inspiré ? Je ne sais pas ! Puis en sortant du musée, de grandes plaques où se trouvent des tas de photos de lui (et des croquis si je me rappelle bien) sont exposées. Il y a trop de monde, donc je continue le trajet.

Et là, je me retrouve dehors ! On me demande d'écrire mon nom sur de grands livres d'or, qui sont sous les tentes blanches du début. Puis on me souhaite une bonne journée.


Voilà, voilà… Je suis surprise et surtout pas mal frustrée. Il est 14h30 et je m'attendais à une cérémonie dans une salle avec des discours… Mais rien de tout ca ! Je suis déçue, si j'avais su j'aurai pris plus de temps à regarder les photos, car le moment est à peine commencé et déjà terminé :( Ca m'apprendra à mieux profiter du moment présent, et à ne pas essayer d'anticiper ce qui va se passer)

# Bon eh bien faisons des trucs quand même…

Ce qui est bien (quand même) c'est que j'avais pas fini de trouver des cadeaux pour celleux resté·e·s en France. Bon c'est la première et la dernière fois que je fais des expéditions spécialement pour trouver des cadeaux, c'est un truc à s'arracher les cheveux !! Mais là, j'ai plein de temps. Je reprends le chat-bus, le JR pour filer à Harajuku, près duquel une des copines m'a parlé d'un super magasin de souvenirs pas cheap. Je passe devant un Starbuck et je sens un grand besoin d'occidentalité, je monte me recharger en sucre en prenant un Frappé au Thé matcha (une tuerie). Puis je file au magasin, fait quelques emplettes, et rejoint Shibuya, vais dans la tour Loft dont le premier étage est composé de milliers d'articles de papeterie (dur de résister) (d'ailleurs j'ai pas résisté) (j'ai acheté mes supers tampons totoro et rien que ça, ça valait le coup). Puis je rejoins les copines, on va faire des purikura, c'est à dire des photomatons où l'on peut s'agrandir les yeux comme des mangas et mettre des effets tout à fait discutables graphiquement (mais tellement amusants :D )

# This is the end…

Puis c'est la soirée, on part à Akihabara pour faire des derniers jeux de rythme (je vous ai dit que je kiffais les jeux de rythme ???), acheter des bêtises pour nos derniers billets dans les magasins pour otakus (les fans de mangas), puis c'est le retour à l'appart ou des copains nous ont achetés plein de trucs étranges à tester… Pas convaincue mais ça finit chouettement le voyage :p Je suis encore un peu crevée de ma matinée complètement éprouvante, entre les angoisses du matin amplifiées par le fait de tourner en rond puis la cérémonie qui ne se déroule pas comme prévu… x.x

Mais il reste une mission: Tout faire rentrer dans la valise !! Je triche et prends mes pokémon à la main, je vais passer pour une gaijin un peu folle mais c'est pas grave :p

Et pour finir nos derniers yens, on file au conbini pour prendre des trucs un peu nuls qui nous font plaisir ! Ca sent la fin :<

# Le matin du départ

On se lève tôôôôôôôôôôôt pour ne pas se faire prendre dans les transports en commun bondés ! Avec nos valises blindées, heureusement qu'il y a des escalators :3

Mais arrivé·e·s sur le quai, on se rend compte que même si il est 6:30, les trains sont déjà blindés o.o Comment faire ??? Après en voir passer 2, nous décidons de prendre la ligne en sens inverse, descendre au terminus pour entrer dans un train qui n'est pas rempli de japonais x.x

Nous le faisons, et, arrivé·e·s à destination, à peine descendus sur le quai, on nous dit que c'est le même train dans lequel il faut remonter. Nous nous y engouffrons, en passant devant tout les japonais qui font la queue (dans la précipitation on n'y a pas du tout fait attention, on commençait à être très stressé·e·s de rater l'avion, mais d'après mon prof de japonais on ne leur a pas grillé la politesse).

Enfin bon, on est dans le train, on se retrouve un peu coincé·e·s au fur et à mesure du chemin car les personnes montent, mais on est dedans ! Un changement à Shibuya et on se retrouve dans la navette pour l'aéroport.

C'est un peu le bad de voir les bâtiments et de sentir qu'on s'en va :( Se sont les derniers instants et j'ai tellement envie de rester plus !!

# Home sweet Home

Pour le retour rien de spécial, de l'avion jusqu'à Londres, un peu d'attente à l'aéroport (où je découvre une boutique Harry Potter \o/), Londres -> Paris puis je prends le train le lendemain jusqu'à Toulouse.

# Bilan

Je vous prépare un petit article récapitulatif sur tout ça, et ça en sera fini de cette série sur le Japon :3 Allez à la prochaine !!
