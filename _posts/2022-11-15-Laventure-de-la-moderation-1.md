---
layout: post
title: "Un contexte compliqué"
subtitle: "L'aventure de la modération : Chapitre 1"
date: 2022-11-15 10:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/laventuredelamoderation1/
---

Il me semble que mon parcours rassemble plusieurs spécificités qui me permettent d'analyser sous un angle particulier les différentes particularités de la modération :

- Je suis une femme féministe, et une "geek",
- Je suis de gauche (= progressiste), et soutiens tous les mouvements pour plus de justice sociale,
- Je suis conceptrice d'outils numériques, donc je connais le pouvoir que nous avons lorsque nous créons un produit numérique,
- Je suis critique du numérique, et notamment du numérique capitaliste (de son petit nom, capitalisme de surveillance). J'enrichis cette réflexion grâce aux discussions que nous avons au sein de l'association [Framasoft](https://framasoft.org) dont je suis membre bénévole.
- J'ai fait de la modération pour un réseau social et ce réseau social n'était **pas capitaliste.**


C'est parti !

## 3615 ma vie : D'où je parle

<details>
  <summary>
   Un ordi dès l'enfance
  </summary>

J'ai toujours été utilisatrice d'ordinateurs. J'ai retrouvé récemment des photos d'un Noël où j'avais peut-être 4 ou 5 ans et où j'ai eu le droit à un ordinateur format jouet, le genre de trucs qui doit faire des chansons ou proposer des mini-jeux en répétant les mêmes phrases à voix robotique à longueur de journée. Quand j'ai vu cette image, j'ai tout de suite pensé à cette courbe qui montre que l'intérêt des enfants pour l'informatique est similaire pour les filles et les garçons… jusqu'au moment où l'on offre des ordinateurs aux petits garçons et pas aux petites filles.

<div>
<video width="100%" controls>
 <source src="https://framapiaf.s3.framasoft.org/framapiaf/media_attachments/files/000/754/193/original/6c46f0379fa98150.mp4" type="video/mp4">
La courbe montre le pourcentage de femmes diplômées dans 4 sections : médecine, droit, physique, informatique, qui augmente au fur et à mesure du temps. Mais on observe un décrochage des diplomées en informatique juste après que l'ordinateur soit promu comme un jouet pour les garçons
</video>
</div>

Or, j'ai eu la chance d'avoir une petite sœur, aussi pas de compétition genrée au sein de la fratrie. Par la suite en 1999 nous avons déménagé en Polynésie Française et dans notre chambre d'enfants nous avions un ordinateur, un Windows 98 que nous pouvions utiliser pour jouer à de nombreux jeux vidéos avec bonheur.

Internet n'était qu'une possibilité très lointaine, réservée à mon père qui parfois tentait de m'aider à me connecter pour pouvoir accéder aux jeux de réseau… mais ça n'a jamais fonctionné (peut-être dû au décalage horaire de 12h avec l'hexagone, je ne le saurai jamais !).

Une fois de retour en France hexagonale, mon père qui était le technicien de la maison, décède. Ma mère n'étant absolument pas intéressée par le sujet, je deviens l'informaticienne de la maison : c'est souvent moi qui installe les logiciels (par exemple le scrabble pour jouer en ligne), qui sait comment libérer de la place sur l'ordinateur en désinstallant des logiciels (parce que je le faisais souvent sur le Windows 98, j'avais donc déjà de la pratique).

Dans mon milieu familial, si personne n'est admiratif de mes bidouilles, personne ne m'empêche de les faire et je deviens assez vite autonome. De plus, le collège porte un projet de numérisation et on offre aux 4e et aux 3e un ordinateur portable. Il est difficile de se connecter à Internet, il n'y a pas de Wifi, mais cela me permet déjà de jouer, de regarder des films de façon indépendante, bref encore plus d'autonomie pour moi !

</details>


<details>
<summary>
 Un début de pratique collective du numérique (forums, skyblogs, réseaux sociaux)
 </summary>

Plus tard au lycée, l'accès internet est plus simple (on a du wifi !) et je traîne sur des forums notamment liés aux génériques de dessins animés (oui c'est un de mes trucs préférés de la vie, je ne sais toujours pas pourquoi). J'ai souvenir d'être déjà pas mal méfiante, assez poreuse aux discours de "on ne sait pas qui nous parle sur Internet". C'est le grand moment des skyblogs, qu'on ouvre en 2 clics pour raconter sa vie (quel bonheur !). Et enfin, c'est l'arrivée de Facebook qui devient un outil très répandu.

Si je n'ai pas de souvenirs de harcèlement via les réseaux sociaux, je me souviens déjà d'une pression sociale pour s'y inscrire. Nous organisions toutes nos sorties cinéma, anniversaires, soirées via les évènements Facebook, et l'une de nos amies n'y était pas inscrite… nous oubliions de la prévenir systématiquement =/

Pour la suite je vous la fais rapide, je vieillis, c'est le temps des études que j'oriente d'abord malgré moi vers le design web (j'avais envie de faire des dessins animés mais j'étais pas assez bonne dessinatrice !)

</details>

### Le début d'un lien entre politique et numérique

Je prends conscience du pouvoir mêlé à l'enfer de l'instantanéité des réseaux sociaux en 2015, au moment des attentats de Charlie Hebdo : je suis fébrilement l'actualité via le hashtag… avant de voir une vidéo de meurtre et de fermer lentement mon ordinateur, horrifiée par ce que je viens de voir. C'est une période où je fais "connaissance" avec les discours islamophobes qui superposent les raccourcis.

Je ne suis, à l'épqoue, pas très politisée, mais pas à l'aise avec ces discours… jusqu'à ce que je fasse connaissance avec une personne qui a un discours que je trouve très juste sur ce sujet (il doit me faire découvrir le concept d'islamophobie, ce qui est déjà pas mal). Et il s'avère que cette personne étant également dans la mouvance du logiciel libre : je découvre alors un monde numérique qui a une intention éloignée de celles du capitalisme, et notamment [l'association Framasoft](https://framasoft.org).

## Alternatif, vous avez dit alternatif ?

Je découvre donc ce monde numérique alternatif, ou "libre" et au bout de quelques années, un réseau social alternatifs : Début 2018, alors que j'utilise beaucoup Twitter, une alternative nommée Mastodon se lance. Je m'y inscris, et l'utilise peu, avant de m'y constituer un petit réseau qui me fait y rester (autant que sur Twitter).

Les années passent encore, nous sommes en 2019 et on me propose de rejoindre Framasoft, ce que j'accepte ! Je découvre un collectif qui développe une réflexion très poussée sur ce qu'il y a de délétère dans la société et notamment le numérique, et qui propose des alternatives émancipatrices.

Lorsque je rejoins l'association, au printemps 2019, un sujet est brûlant au sein de l'asso comme à l'extérieur : La modération du réseau social Framapiaf.

### Framapiaf ? Mastodon ? Kezako ?

En 2017, un réseau social alternatif commence à faire parler de lui : il s'agit de Mastodon.

Ce réseau social a un fonctionnement un peu particulier : il est décentralisé, en opposition a plein d'autres réseaux sociaux, notamment les réseaux capitalistes, qui sont eux tous centralisés : Il y a 1 Facebook, 1 Twitter, 1 Instagram, 1 TikTok… etc

Pour comprendre en moins de 2 minutes le concept, je vous laisse regarder cette petite vidéo, sous-titrée en français : [https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD](https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD)

Il y a donc une multitude de Mastodon, proposés par une multitude de personnes, associations ou entreprises différentes. Mais ces Mastodon sont tous connectés les uns aux autres, ce qui permet que les personnes inscrites sur le Mastodon de Framasoft peuvent tout à fait discuter avec des personnes inscrites sur les Mastodon d'autres associations, exactement comme pour les mails ! (si vous avez un mail en @orange.fr et moi un mail en @laposte.net, nous pouvons nous écrire).

Cette spécificité de fonctionnement a plusieurs intérêts, et une grand influence sur les possibilités de modération. Par exemple :
- Le modèle économique de chaque Mastodon est choisi par la structure qui le porte. Chez Framasoft par exemple, se sont les dons qui permettent de tout faire tourner.
- Les règles de modération de chaque Mastodon sont choisies par la structure qui le porte. Aussi, c'est à elle de définir ce qu'elle accepte ou n'accepte pas, en tout indépendance.
- Si les règles ou le modèle économique d'un Mastodon ne vous conviennent pas, vous pouvez… déménager sur un autre Mastodon, tout simplement !


### Quel était le problème avec la modération ?

Il faut savoir que Framasoft, entre 2014 et 2019 lançait en moyenne un service par mois (framapad, framadate, framaforms…). Elle a donc lancé Framapiaf pour participer à la promotion du réseau social, sans prendre particulièrement en compte la nécessité de gérer les interactions qui se dérouleraient sur ce média social.

L'association n'était à l'époque composée que de 8 salarié·es pour plus de 40 services proposés, des conférences réalisées, des interventions médias, des billets de blog, des campagnes de dons et financement participatif… bref, l'existence du réseau social n'était qu'un petit élément de cet univers, et sa modération un sous-élément en plus de l'aspect technique…

Je parle des interactions et non pas de la modération car sur d'autres services, les salariés géraient déjà de la modération de contenu : Images violentes, notamment de pédopornographie… Mais cela impactait uniquement les salariés et ne se propageait pas vers l'extérieur, donc restait du travail totalement invisible.

Or, avec Framapiaf, si il y avait des contenus problématiques qui étaient diffusés, ils n'impactaient pas seulement les salariés qui faisaient la modération mais aussi leurs destinataires, et potentiellement tous les usagers du média social.

Aussi de nombreuses utilisatrices ont signalé à l'association (parfois violemment, j'y reviendrais) que l'état de la modération leur semblait insuffisant, et que des personnes avaient des interactions problématiques avec iels.

Les salariés n'ayant pas la disponibilité pour gérer pleinement ce sujet, se sont des bénévoles qui s'en sont chargés.


## Avant la charte : Un flou qui crée des tensions
### Recette pour abîmer des modérateurices

En effet, jusqu'à présent quelques salarié·es de l'association géraient la modération avec les moyens du bord, étaient souvent seuls à s'occuper des signalements dans leur coin,  alors que cette tâche est particulièrement complexe.

Pourquoi complexe ? Parce que lorsque l'on est modérateurice, et qu'on souhaite faire un travail juste, notre notion de ce qui est juste est sans cesse réinterrogée.

Déjà dans l'analyse de la situation : Est-ce que la personne qui a posté ce contenu l'a fait en sachant sciemment qu'elle risquait de blesser quelqu'un, ou non ? Dois-je lui donner le bénéfice du doute ? Est-elle agressive ou seulement provocante ? Est-ce qu'on doit agir contre une personne provocante ? Est-ce que ce contenu est vraiment problématique, ou est-ce que c'est moi qui me suis levé·e du mauvais pied ? qui suis particulièrement sensible à cette situation ?

Ensuite dans les actions à effectuer : Dois-je supprimer ce contenu ? Supprimer ce compte ? Prévenir la personne pour lui expliquer le pourquoi de mes actions, par transparence ? Comment cela va-t-il être reçu ? Quel est le risque de retour de flammes pour moi ? Pour l'association ?

Et le meilleur pour la fin : On se sent seul responsable lorsque des personnes extérieures viennent critiquer, parfois violemment, la façon dont la modération est réalisée.

Il n'y a aucune réponse universelle à ces questions. Ce qui permet en général de simplifier le processus de modération, c'est :
- un collectif qui permet de prendre une décision collégiale (ce qui permet de ne plus faire reposer une décision difficile et ses conséquences sur un seul individu)
- une vision politique qui permet d'autant plus facilement de prendre ces décisions

Nous avons une chance immense à Framasoft, c'est que toutes les personnes actives au sein de l'association sont très alignées politiquement (ça se sent dans [le manifeste](https://framasoft.org/fr/manifest) que nous venons de publier)

Mais il n'y avait pas de collectif de modérateurices pour traiter ces questions et prendre les décisions qui sont les plus dures : celles pour les situations qui se trouvent "sur le fil".

### Structurer un groupe de modérateurices pour s'améliorer
J'arrive donc à Framasoft avec l'envie d'aider sur le sujet de la modération, qui est instamment priorisé par le directeur de l'association au vu du stress dans lequel étaient plongés les salariés-modérateurs.

Nous sommes un petit groupe (3 personnes) à proposer de rédiger une charte qui transposera notre vision politique en discours lisible, sur laquelle nous pourrons nous appuyer et nous réferrer pour expliciter nos choix de modération futurs et trancher des situations problématiques qui ont profité d'un flou de notre politique de modération.

Nous n'avons pas de débat majeur, étant assez alignés politiquement c'est la rédaction de la charte qui a été la plus complexe, car nous étions 2 sur 3 à être novices en modération, nous ne savions donc pas précisément ce qui nous servirait dans le futur. La charte est disponible [sur cette page](https://framasoft.org/fr/moderation/) et a été publiée le 8 juillet 2019.


### Publication de la charte

Donc une fois la charte réalisée, il a fallu la poster.

Et là.

J'ai déjà raconté en partie ce qui s'est passé sur un précédent billet de blog nommé [Dramasoft](https://www.maiwann.net/blog/dramasoft/), du nom du hashtag utilisé pour parler de la vague de réactions suite à cette publication.

Pour résumer, nous avons subit une vague de harcèlement, de la part de personnes directement touchées par des oppressions systémiques (ça fait tout drôle). Il s'agit d'un type particulier de harcèlement donc je reparlerai : il n'était pas concerté. C'est à dire que les personnes ayant contribué au harcèlement ne l'ont pas fait de façon organisée. Mais l'accumulation de reproches et d'accusations a exactement le même effet qu'une vague de harcèlement opérée volontairement.

D'ailleurs pour moi, elle était pire qu'une vague de masculinistes du 18-25 que j'avais vécu un an auparavant. Car il est bien plus facile de les mettre à distance, en connaissant les mécanismes utilisés et leurs méthodes d'intimidation. Ce n'est pas la même chose de se faire harceler par des personnes qui sont dans le même camp que nous. Et je n'ai pas réussi à créer de la distance, en tout cas sur le moment (maintenant j'ai compris que les façons de communiquer comptaient ! ).

## La charte

Je reviens sur ses différents articles :

### 1 - Cadre légal

> Nous sommes placé·e⋅s sous l’autorité de la loi française, et les infractions que nous constaterons sur nos médias pourront donner lieu à un dépôt de plainte. Elles seront aussi modérées afin de ne plus apparaître publiquement.

> Cela concerne entre autres (sans être exhaustif) :
- l’injure
- la diffamation
- la discrimination
- les menaces
- le harcèlement moral
- l’atteinte à la vie privée
- les appels à la haine
- La protection des mineurs, en particulier face à la présence de contenu à caractère pornographique :
  - Loi n° 98-468 du 17 juin 1998
  - Article 227-22-1 et Article 227-23 du code pénal


Alors respecter la loi, ça peut sembler bête de le rapeller, mais ça nous permet de faire appel à un argument d'autorité qui peut être utile lorsqu'on n'a pas envie de faire dans la dentelle.

### 2 - Autres comportements modérés

Ici nous avons fait en sorte d'être, autant que possible, "ceinture et bretelles" pour rendre explicites que nous trouvons ces différents comportements problématiques. Et si vous avez l'impression qu'il s'agit parfois de choses évidentes, eh bien sachez que ça n'est pas le cas pour tout le monde.

> Nous n’accepterons pas les comportements suivants, qui seront aussi modérés :
- Envoyer des messages agressifs à une personne ou à un groupe, quelles que soient les raisons. Si on vous insulte, signalez-le, n’insultez pas en retour.

Permet de ne pas tomber dans le "mais c'est pas moi c'est lui qui a commencé"

> - Révéler les informations privées d’une personne. Dévoiler ses pseudonymes et les caractères de son identité est un choix personnel et individuel qui ne peut être imposé.

Ici par exemple, on explicite clairement un comportement qui n'a rien de répréhensible en soit (c'est pas interdit de donner des infos sur une personne ! ) mais qu'on sait avoir des effets délétères sur les personnes visées (si on donne le numéro de téléphone ou l'adresse d'une personne, même si rien ne se passe ensuite, il y a a minima la peur d'être contacté·e par des inconnus qui est vraiment problématique.)

> - Insister auprès d’une personne alors que cette dernière vous a demandé d’arrêter. Respectez les refus, les « non », laissez les gens tranquilles s’iels ne veulent pas vous parler.

On rappelle la base de la notion de consentement, ce qui permet aussi d'encourager les utilisateurices à poser leurs limites si elles souhaitent arrêter un échange.

> - Tenir des propos dégradants sur les questions de genre, d’orientation sexuelle, d’apparence physique, de culture, de validité physique ou mentale, etc. et plus généralement sur tout ce qui entretient des oppressions systémiques. Si vous pensiez faire une blague, sachez que nous n’avons pas d’humour sur ces questions et que nous ne les tolérerons pas. Dans le doute, demandez-vous préalablement si les personnes visées vont en rire avec vous.

Ici, petit point "oppression systémique" aussi résumable en "on est chez les woke, déso pas déso" + on coupe court à toute tentative de négocier par un prétendu "humour"

> - Générer des messages automatiques de type flood, spam, etc. Les robots sont parfois tolérés, à condition d’être bien éduqués.

Les bots pourquoi pas mais selon ce qui nous semble être "une bonne éducation" et oui c'est totalement arbitraire :)


> - Partager des émotions négatives intenses (notamment la colère), en particulier à l’égard d’une personne. Les médias sociaux ne sont pas un lieu approprié pour un règlement de compte ou une demande de prise en charge psychologique. Quittez votre clavier pour exprimer les émotions intenses, trouvez le lieu adéquat pour les partager, puis revenez (si nécessaire) exposer calmement vos besoins et vos limites.

Ici on partage notre vision des réseaux sociaux : Ils ne permettent pas d'avoir une discussion apaisée, ne serait-ce que parce qu'ils limitent grandement le nombre de caractères et par là même, la pensée des gens. Donc si ça ne va pas, si vous êtes en colère, éloignez-vous-en et revenez quand ça ira mieux pour vous.

> - Participer à un harcèlement par la masse, inciter à l’opprobre générale et à l’accumulation des messages (aussi appelé dogpiling). Pour vous ce n’est peut-être qu’un unique message, mais pour la personne qui le reçoit c’est un message de trop après les autres.

Ici on place la différence entre une action et l'effet qu'elle peut avoir sur les personnes. En effet, si le harcèlement organisé est facile à imaginer, on se rend moins compte de l'effet que peut avoir un flot de messages envoyé à une personne, quand bien même les expéditeur·ices ne se sont pas coordonnés pour générer une vague de harcèlement… le ressenti final est similaire : On reçoit des tonnes de messages propageant de la colère ou des reproches, alors qu'on est seul·e de l'autre coté de l'écran.

> - Dénigrer une personne, un comportement, une croyance, une pratique. Vous avez le droit de ne pas aimer, mais nous ne voulons pas lire votre jugement.

On conclut avec une phrase plus généraliste, mais qui nous permet d'annoncer que le réseau social n'est pas là pour clamer son avis en dénigrant les autres.


### 3 - Les personnes ne sont pas leurs comportements

> Nous différencions personnes et comportements. Nous modérerons les comportements enfreignant notre code de conduite comme indiqué au point 5, mais nous ne bannirons pas les personnes sous prétexte de leurs opinions sur d’autres médias.

Nous changeons et nous apprenons au fil de notre vie… aussi des comportements que nous pouvions avoir il y a plusieurs années peuvent nous sembler aujourd'hui complètement stupides… Cette phrase est donc là pour annoncer que, a priori, nous aurons une tolérance pour les nouveaux arrivants et nouvelles arrivantes.

> Cependant, nous bannirons toute personne ayant mobilisé de façon répétée nos équipes de modération : nous voulons échanger avec les gens capables de comprendre et respecter notre charte de modération.

Cette phrase est parmi celles qui nous servent le plus : A Framasoft, ce qui nous sert de ligne de rupture, c'est l'énergie que nous prend une personne. Quand bien même ce qui est fait est acceptable du point de vue de la charte, si nous sommes plusieurs à nous prendre la tête pour savoir si oui, ou non, les contenus partagés méritent une action de modération… Eh bien c'est déjà trop. Après tout, nous avons d'autres projets, l'accès à nos services est gratuit, mais il n'y a pas de raison de nous laisser vampiriser notre temps par quelques utilisateurs qui surfent sur la frontière du pénible.

### 4 - Outils à votre disposition

> Nous demandons à chacun⋅e d’être vigilant⋅e. Utilisons les outils à notre disposition pour que cet espace soit agréable à vivre :

> - Si quelqu’un vous pose problème ou si vous vous sentez mal à l’aise dans une situation, ne vous servez pas de votre souffrance comme justification pour agresser. Vous avez d’autres moyens d’actions (détaillés dans la documentation) :
      - fuir la conversation,
      - bloquer le compte indésirable
      - bloquer l’instance entière à laquelle ce compte est rattaché (uniquement disponible sur Mastodon)
      - signaler le compte à la modération.

Une partie de l'action des modérateurices est de faire (ce que j'appelle personnellement) "la maîtresse". J'ai plusieurs fois l'impression de me faire tirer la manche par un enfant qui me dirait "maîtresse, untel est pas gentil avec moi".
J'y reviendrai, mais c'est pour cela que nous listons les façons d'agir: pour que dans les cas mineurs, les personnes puissent agir elles-même. Déjà parce que se sera toujours plus rapide, que cela permet à la personne de faire l'action qu'elle trouve juste, et puis parce que ça permet de ne pas nous prendre du temps pour des cas qui restent des petites prises de bec individuelles.

> - Si quelqu’un vous bloque, entendez son souhait et ne cherchez pas à contacter cette personne par un autre biais, ce sera à elle de le faire si et quand elle en aura envie.

Petit point consentement, ça fait pas de mal.

> - Si vous constatez des comportements contraires au code de conduite menant des personnes à être en souffrance, signalez-le à la modération afin de permettre l’action des modérateurs et modératrices. Alerter permet d’aider les personnes en souffrance et de stopper les comportements indésirables.

Il ne nous est pas possible d'être pro-actif sur la modération, pour 2 raisons simples :
1. On a autre chose à faire que passer notre temps à lire les contenus des un·es et des autres
2. Nous avons nos propres comptes bloqués, masqués, et nous ne sommes pas au sein de toutes les conversations. Aussi il n'est pas possible de tout voir (et de toute façon, ça nous ferait bien trop de boulot !)

> - S’il n’est pas certain qu’une personne est en souffrance, mais que les comportements à son égard sont discutables, engagez la conversation autour de ces comportements avec chacune des personnes concernées, dans le respect, afin d’interroger et de faire évoluer votre perception commune des comportements en question.

Petit point : chacun·e peut être médiateurice, il n'y a pas que les modérateurices qui doivent le faire. J'insiste sur ce point, car on a tendance à créer une dichotomie entre "les utilisateurs" qui peuvent faire preuve d'immaturité, et les modérateurices qui eux seraient parole d'évangile ou au moins d'autorité. Il est dommage de ne pas avoir davantage de personnes qui se préoccupent d'un entre deux où elles pourraient ménager les 2 parties afin que chacun·e clarifie pacifiquement sa position, ce qui permettrait sûrement d'éviter les actions de modération.

> - Dans le cadre du fediverse (Framapiaf, PeerTube, Mobilizon,…), vous pouvez également masquer un compte (vous ne verrez plus ses messages dans votre flux mais vous pourrez continuer à discuter avec la personne). Vous pouvez aussi bloquer un compte ou une instance entière depuis votre compte : à ce moment, plus aucune interaction ne sera possible entre vous, jusqu’à ce que vous débloquiez.

> Référez-vous à la documentation sur les façons de signaler et se protéger suivant les médias utilisés.

Et pour conclure, on donne d'autres indications sur la façon de s'auto-gérer sur une partie de la modération.

### 5 - Actions de modération

> Lorsque des comportements ne respectant pas cette charte nous sont signalés, nous agirons dans la mesure de nos moyens et des forces de nos bénévoles.

> Dans certains cas, nous essayerons de prendre le temps de discuter avec les personnes concernées. Mais il est aussi possible que nous exécutions certaines de ces actions sans plus de justification :
- suppression du contenu (des messages, des vidéos, des images, etc.)
- modification (ou demande de modification) de tout ou partie du contenu (avertissement de contenu, suppression d’une partie)
- bannissement de compte ou d’instance, en cas de non respect des CGU (usage abusif des services) ou de non-respect de cette charte.

> NB : Dans le cadre du fediverse (Framapiaf, PeerTube, Mobilizon,…), nous pouvons masquer les contenus problématiques relayés sur notre instance, voire bloquer des instances problématiques, mais non agir directement sur les autres instances.

> La modération est effectuée par des bénévoles (et humain⋅es !), qui n’ont pas toujours le temps et l’énergie de vous donner des justifications. Si vous pensez avoir été banni⋅e ou modéré⋅e injustement : nous sommes parfois injustes ; nous sommes surtout humain⋅e⋅s et donc faillibles. C’est comme ça.

> Utiliser nos services implique d’accepter nos conditions d’utilisation, dont cette charte de modération.

Nous faisons ici un récapitulatif des différents pouvoirs que nous avons et surtout que nous n'avons pas :
- le fonctionnement de Mastodon fait que nous pouvons discuter avec des personnes qui ne sont pas inscrites chez nous : nous ne pouvons donc pas supprimer leurs comptes (mais nous pouvons couper leur communication avec nous)
- nous sommes principalement des bénévoles, et de ce fait notre temps et énergie est limitée. Aussi, souvent, alors que nous aimerions discuter posément avec chacun·e pour expliquer les tenants et les aboutissants d'une situation… nous n'en avons simplement pas l'énergie, alors nous allons au plus simple. Et c'est frustrant, c'est injuste, mais c'est l'équilibre que nous avons trouvé.


### 6 - En cas de désaccord

> Si la façon dont l’un de nos services est géré ne vous plaît pas :
- Vous pouvez aller ailleurs ;
- Vous pouvez bloquer toute notre instance.

> Framasoft ne cherche pas à centraliser les échanges et vous trouverez de nombreux autres hébergeurs proposant des services similaires, avec des conditions qui vous conviendront peut-être mieux. Que les conditions proposées par Framasoft ne vous conviennent pas ou que vous ayez simplement envie de vous inscrire ailleurs, nous essaierons de faciliter votre éventuelle migration (dans la limite de nos moyens et des solutions techniques utilisées).

J'ai dit plus haut que les personnes qui rejoignaient Mastodon ne s'inscrivaient pas toutes chez nous. Cela nous permet par ailleurs de dire aux personnes qui se sont inscrites chez nous "désolés mais vous nous prenez trop d'énergie, trouvez vous un autre endroit". Cela ne prive en rien les personnes car elles peuvent s'inscrire ailleurs et avoir accès exactement aux mêmes contenus… mais nous ne sommes plus responsables d'elles !

### 7 - Évolution de la Charte

> Toute « Charte de modération » reste imparfaite car il est très difficile de l’adapter à toutes les attentes. Nous restons à votre écoute afin de la faire vivre et de l’améliorer. Pour cela vous pouvez contacter les modérateurs et modératrices pour leur faire part de vos remarques.

Vous pouvez nous faire vos retours, on est pas des zânes ni des têtes de mules.

## La suite

Après ce tour sur mon arrivée dans le monde de la modération, la découverte de ce qu'est Mastodon, et ce par quoi nous avons démarré pour structurer une petite équip de modération face aux difficultés vécues par les salarié·es, [le second article de cette série](https://www.maiwann.net/blog/laventuredelamoderation2/) parlera de cas concrets.

Si vous voulez savoir comment on fait pour gérer les fachos, ça va vous plaire.
