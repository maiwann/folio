---
layout: post
title: "Zam 3 : Circuler au sein de la visionneuse"
subtitle: ""
date: 2019-11-25 15:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/zam-circuler-au-sein-visionneuse/
---

Dans ce 3e billet, je vous propose de refaire un tour du coté de la visionneuse, où des problèmes importants de performances se posent !

# Circuler au sein de la visionneuse

## Au secours, ça rame !!

Sur la visionneuse, il peut y avoir jusqu'à 1800 amendements à charger pour une lecture. Si nous avons bien fait attention lors de la conception de réaliser une interface la plus légère possible tout en répondant aux besoins des utilisateurs, nous sommes tout de même en galère. La seule façon dont nous pourrions encore gagner un peu de performance serait de retirer les bandeaux "sticky" indiquant dans quel article on se trouve. Mais si on les supprime, adieu le contexte pour les utilisateurs et au milieu d'une page immense, comment se repérer pour savoir si l'amendement n°827 est dans l'article 67 ou 83 ?

Bref, la solution n'est pas simple !

## Une porte de sortie

Néanmoins, grâce à de premiers tests au sein de l'assemblée, nous avons eu une bonne nouvelle : Il y a du réseau au sein de l'assemblée !

Eh oui… ce n'était pas gagné a priori :)

Mais ce que cela nous permet, c'est de changer d'angle pour la conception : Nous étions partis sur une page unique afin d'être sur·es que la visionneuse ne serait pas en rade faute de réseau. Or :
- D'après les tests, niveau réseau, on s'en sort
- Et la page unique est trop lourde

Donc on se remonte les manches, et on change d'orientation niveau conception pour gagner en performances.

## Des pages ! Enfin des pages !

Joie et félicité, nous pouvons donc organiser l'application avec un menu et des sous-pages !

A présent, l'accès à la visionneuse se fait par un écran qui ressemble beaucoup à un sommaire, listant l'ensemble des articles du projet de loi. Il est possible de passer par la recherche pour arriver plus rapidement à un article donné.

<figure>
  <img src="/img/posts/2019_Zam/03/zam_1.png" alt="">
</figure>

Et une fois cliqué sur cet article, les amendements sont listés, dans l'ordre de lecture annoncé par la séance. Nous avons également ajouté une barre de sous-navigation pour que les utilisateurs puissent aller d'article en article, au fur et à mesure de la séance, sans devoir se souvenir du contexte (ce qui aurait été le cas si un retour au menu avait été nécessaire, une attention accrue aurait été nécessaire afin de devoir se souvenir de quel est l'article passé et l'article qui vient d'être commencé).

<figure>
  <img src="/img/posts/2019_Zam/03/zam_2.png" alt="">
</figure>

On y a rajouté un champ de recherche en haut à droite pour que, si l'un des utilisateurs veut promptement aller lire un amendement, se soit possible via la recherche (sans avoir à retrouver l'article, remonter au menu et faire défiler la page).


<figure>
  <img src="/img/posts/2019_Zam/03/zam_3.png" alt="">
</figure>


## Et les performances ?

Sans surprise, les performances sont devenues tout à fait acceptables. En même temps, nous passions de 1800 amendements à une 30aine maximum, donc le gain était évident.

## Le design d'interface, la solution pour les performances

Ce que j'aime avec cette anecdote, c'est que, pour une problématique qui pourrait sembler purement technique initialement, c'est une re-conception de l'interface et de l'organisation du parcours utilisateur qui a permis d'améliorer les performances. Car du point de vue du développement, il n'y avait plus rien à optimiser, et l'on aurait pu se trouver dans une impasse si nous n'avions pas ré-interrogé notre hypothèse de départ qui pourrait sembler hors-sujet: "Mais, est-ce que c'est vraiment compliqué d'avoir du réseau dans l'assemblée ?"

## Et ensuite ?

Dans le prochain article, je vous parlerai de notre [première rétrospective](/blog/zam-premiere-retro/) à 5 ! Moins de design d'interface et plus de communication inter-humains ! A bientôt :)
