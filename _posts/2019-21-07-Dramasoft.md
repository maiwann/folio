---
layout: post
title: "Dramasoft…"
subtitle: ""
date: 2019-07-21 19:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: none
permalink: blog/dramasoft/
---

*Préambule : J'écris pour poser des mots sur ce qu'il s'est passé pour moi ces derniers jours, dans l'espoir que les mots cessent de tourner sans arrêt dans ma tête. Cet écrit est donc fortement biaisé,* **les citations sont altérées par ce que j'en retenais,** *et je n'entends pas représenter une quelconque autre réalité que la mienne.*

Discuter avec les membres de Framasoft. Se sentir écoutée, souvent. Jamais remise en question sur mes positions féministes. Rencontrer des hommes qui acceptent de se sentir inconfortables face à ce que je leur raconte, en remettant en cause leurs croyances. Se sentir écoutée par des hommes. Se sentir incluse. Discuter souvent de déconstruction. Se sentir entendue. Apprécier.

Entendre "J'utilise l'écriture inclusive et je vous emmerde". Demander pourquoi. Apprendre qu'à chaque article, il y a toujours des commentaires "J'arrête de donner à l'asso car cette écriture c'est vraiment des conneries". Prendre l'ampleur de l'insistance de ces personnes. Voir que l'asso n'en a rien à foutre. Apprécier.

Rejoindre l'asso.

Arriver au Framacamp. Entendre que des décisions autour de la modération seront prises d'ici la fin du camp. Éprouver de la gratitude que le sujet soit au premier plan. Entendre d'une personne que la charte devrait être au niveau de la loi et pas plus. S'y opposer. Être soutenue dans cette opposition. Ne même pas avoir à argumenter pour que la personne s'aligne avec la volonté d'être plus protectrice que la loi. Travailler sur la charte. Reformuler ensemble pour que "les oppressions envers les groupes de personnes ne sont pas acceptées" devienne "les oppressions systémiques ne sont pas acceptées", pour qu'on puisse rire des nazis mais pas des personnes queer. Essayer de trouver la limite entre protéger et paterner. Décider de participer à la déconstruction des personnes qui auraient des propos problématiques, en leur expliquant pourquoi ils ne sont pas tolérables, malgré l'énergie que cela va demander. Ressentir la lourdeur et l'importance d'un travail de pédagogie. Ressentir l'urgence d'avancer. Réussir à travailler rapidement tout en étant satisfaite du résultat. Sortir du Framacamp avec une première version de la charte.

Rediscuter la charte au sein de l'association à la marge, sans que la question de protéger les personnes oppressées ne soit remise en cause, jamais. Être fière que le sujet avance. Être admirative de l'énergie que d'autres mettent dessus.

Demander l'accès au compte modération. Prendre conscience que je risque d'être confrontée à des contenus de pédopornographie. Être horrifiée. Avoir peur. Accéder au compte modération. Prendre la mesure de la difficulté de modérer. Prendre la mesure de la difficulté de modérer sans charte à laquelle se référer. Modérer. Modérer trop rapidement et avoir des demandes d'explication. Avoir peur de faire une erreur. Tout le temps. Réaliser que les signalements sont souvent en anglais, et que je n'arrive pas à détecter les subtilités, si importantes. Avoir peur de faire des erreurs, encore. Lire que "personne ne modère sur framapiaf". Voir que la liste des signalements non modérés est vide. Ne pas comprendre. Se sentir gênée. Ne pas se sentir considérée.

Demander en interne quelle sera la position pour Gab. Avoir un consensus rapide pour bloquer l'instance. Être soulagée. Relire le post l'annonçant, le valider. Publier l'annonce sur le forum. Avoir très peur au moment de presser le bouton de publication. Se sentir soulagée que les réactions soient positives.

Voter pour valider la charte. Voir que beaucoup de personnes au sein de l'asso ont voté "Pour". Ressentir de la gratitude quand à l'engagement en interne. Lire l'article de blog présentant la charte. Voir l'illustration. Rire en lisant le mot "safiste". Se souvenir d'avoir été insultée de "social justice warrior" et d'en avoir ressenti de la fierté. Se souvenir de la vague de harcèlement du 18-25 contre moi, et que chaque tweet scandalisé me remplissait de fierté : celle d'avoir pris position pour protéger celleux qui en avaient besoin.

Valider l'article et l'illustration.

Valider la publication. Lire les premières réactions. Positives. Souffler. Voir la seconde vague de réaction. Lire "Vous mettez toutes les oppressions au même niveau c'est ça ?". Lire qu'il faut que l'on se renseigne sur le paradoxe de la tolérance. Lire "Vous voulez garder l'oncle nazi à table". Lire "Vous voulez que les queers et les nazis discutent ensemble, mais ils veulent nous buter !!! ". Lire que nous sommes forcément contre eux, car nous sommes privilégiés.

Réaliser la violence de chaque subtoot.

Réaliser que le nombre en multiplie la violence.

Voir l'hétérogénéité des discussions et le soin apporté en interne éclaté en milles morceaux au fil des toots. Entendre que de toute façon, nous ne sommes que des hommes cis privilégiés. Voir une féministe expliquer que l'on ne peut pas compter sur nous. Se sentir trahie par sa propre communauté. Voir toutes ses années de lectures et de déconstructions balayées du revers de la main. Comme illégitimes. Ressentir du désespoir face à temps de travail personnel rejeté. Voir des demandes de nous bloquer aux administrateur.ices d'autres instances. Avoir peur de perdre l'hétérogénéité de sa timeline.

Avoir envie de hurler. Hurler que non, on ne met pas au même niveau les oppressions queerphobes et le reste. Que non, on n'accueille pas les nazis. Que je sais ce qu'est subir une oppression systémique. Que je suis légitime. Que nous sommes légitimes. Que nous sommes renseignés. Que nous avons réfléchi à ce que nous disons. Que nous savons que bloquer plutôt que discuter serait mille fois plus simple. Et que nous avons choisi la voie la plus difficile, celle de la discussion. Pour faire notre part.

Hurler en privé pour soulager la colère. Être entendue. Respirer. Ne pas parvenir à être soulagée.

Vouloir rappeller que je suis concernée par ce qui se dit depuis la publication. Par la violence qui pleut. Avoir peur de le dire, qu'on me vise directement. Le faire quand même, dans l'espoir que cela fasse réaliser que je reçois en pleine face la violence partagée. Recevoir du soutien. Respirer. Voir en parallèle que des personnes que j'estime continuent d'écrire. Sont en colère. Continuent les partages. Avoir l'impression de ne pas exister. D'être quantité négligeable. Avoir la sensation que tous ses efforts sont réduits en miettes. Pleurer. Demander de l'aide et du soutien. Le retrouver au sein de l'asso et en privé.

Prendre conscience que ce que je lis fait mille fois plus mal que la vague de harcèlement du 18-25. Presque à un an près, presque risible. Vouloir comprendre la colère. Ne pas y arriver. S'éloigner de Mastodon. Être dégoutée que ce que je considérais comme un lieu paisible avait été plus délétère que ne l'a jamais été Twitter pour moi. Pleurer.

Se lever avec la gorge serrée. Considérer que c'est important que le travail soit fait. S'y remettre. Commencer à répondre, ensemble. Expliquer. Se sentir mieux. S'envoyer des câlins en interne. Se sentir soutenue. Lire les premières réponses. Lire que peut-être, il y a eu mauvaise interprétation. Respirer. Lire des mêmes personnes que la veille que, en fait, le travail n'est pas si mal et qu'il n'y a que quelques points à améliorer. Ne pas comprendre. Avoir mal. Être soulagée en même temps. Ne plus savoir où j'en suis.

Publier que des personnes sont concernées au premier degré au sein de l'asso, car elles sont queer. Voir des personnes changer leur formulation de "pas concernés" à "une majorité pas concernée". Respirer. Lire que "ça ne veut rien dire". Lire que "les [tokens](https://fr.wikipedia.org/wiki/Token), ça se trouve". Ressentir de la colère. Vouloir protéger. Ressentir de la fureur face aux affirmations que la phrase est mensongère. Vouloir protéger les personnes queer de l'asso. Vouloir protéger les personnes queer de l'asso qui ne seraient pas out, et de ce fait, ne pas pouvoir le faire. Vouloir leur dire qu'elles n'ont pas à être visibles pour être valides, et ressentir de la colère rien qu'à l'idée qu'elles aient pu ressentir cela. Ressentir de la fureur face à la violence qui se déverse sur elles dans ces phrases, venant de ce qui pourrait être leur propre communauté. Ne rien pouvoir faire. Abandonner. Se coucher avec la gorge serrée.

Ressentir de la fatigue. Voir que la fureur se calme. Baisser la garde. Respirer. Avoir les mots qui tournent. "Pas concernés". "Pouvez pas comprendre". "Pro-nazis". "Vous vous en foutez qu'on meure". Étrangement, lorsque le 18-25 me traitait de nazie, ça ne me faisait rien. Mais de la part de personnes dont je veux prendre soin, ça fait tellement plus mal.

Avoir mis tellement d'énergie pour ce résultat.

Ne même plus avoir envie de modérer. Se dire "à quoi bon".

Y penser tout le temps. Être fatiguée. Se sentir inutile.

Recevoir du soutien en interne et en privé. Souffler. Respirer.

Voir que pour les autres, le drama est passé.

Vouloir le laisser de coté. Ne pas y arriver. Avoir les mots qui tournent dans ma tête.

"Mais vous, vous voulez garder l'oncle nazi à table. Et les nazis, ils veulent nous buter"

"Et les nazis, ils veulent nous buter"

Écrire. Espérer que enfin les mots s'arrêtent de tourner.

Avoir envie de publier. Avoir peur que l'on me tombe dessus. Avoir envie d'être considérée comme une humaine et donc de publier. Avoir peur que l'on me tombe dessus.

Ne pas savoir quoi faire. Être perdue. Être triste. Être fatiguée. Y penser tout le temps.
