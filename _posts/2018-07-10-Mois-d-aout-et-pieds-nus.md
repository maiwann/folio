---
layout: post
title: "Mois d'août et pieds nus"
subtitle: "J'ai envie de tester l'itinérance… et si c'était l'occasion de se voir ?"
date: 2018-07-10 17:30:00 +0200
vignette: null
intro: "J'ai envie de tester l'itinérance… et si c'était l'occasion de se voir ?"
nom_blog: le blog
which_blog: blog_perso
permalink: blog/mois-d-aout-et-pieds-nus/
---

Depuis quelques mois, je croise et recroise des personnes qui se disent nomades ou en itinérance. C'est à dire qu'elles n'ont pas de point fixe, et se déplacent de lieu en lieu, souvent avec l'ensemble de leurs affaires dans leur sac à dos.

Et ça m'a donné envie.

Envie de tenter, voir ce que c'était de se séparer de ses possessions pour partir à l'aventure. De changer d'endroit, de rencontrer des gens différents, de découvrir de nouveaux lieux, de revoir celles et ceux qui ont envie de me voir, sans avoir à me sentir reliée à un point qui m'oblige à faire des détours.

Et comme au mois d'août je n'ai rien de prévu, je me suis dit que c'était un bon timing pour faire un test d'itinérance sur le mois.

Donc, **je pars en itinérance du 27 juillet au 31 août.** Si, toi qui me lis, tu as envie que l'on se voit, même pour juste boire un verre, **ça me ferait plaisir que tu me le dises.**

Le top, ce serait que tu viennes écrire dans ce pad pour que je sache où et quand tu es disponible pour que l'on se croise :) [https://mypads.framapad.org/p/aout-et-pieds-nus-4pwgx7xq](https://mypads.framapad.org/p/aout-et-pieds-nus-4pwgx7xq)

# Ok mais pour faire quoi ?

Alors il n'y a rien à prévoir, mais j'ai des idées de trucs possibles:

- Faire des **blindtests dessins animés <3**
- Découvrir des endroits chouettes
- Faire du design pour des projets libres
- Réfléchir à comment on pourrait travailler ensemble
- Organiser des cafés design (je me pose dans un endroit et celleux qui veulent venir parler / connaître mieux le design, l'ergonomie, l'UX, sont les bienvenu·e·s)
- Boire des diabolos
- Concevoir des walkingdevs au pied levé !
- Faire la sieste dans un parc
- Discuter de féminisme
- Aller voir les étoiles
- Fêter mon anniversaire

Mais je me laisse le droit de ne rien faire de tout ça, et d'être surprise par ce qu'il va se passer :)
