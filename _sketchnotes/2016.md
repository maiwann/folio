---
title: 2016
vignette_url: /img/vignette/sketchnote/2016_10_29_7.jpg
vignette_title: Paris Web · Libérée, délivrée du syndrôme de l'imposteur ?
sketchnotes:
  - title: "Paris Web · Lightning talks 3"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_10.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Lightning talks 2"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_9.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Lightning talks 1"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_8.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Open Design (informelle)"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_7.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Le Bon Coin : Coulisses d'une refonte responsive"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_5.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Kit de survie en atelier de co-conception fonctionnelle UX"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_3.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Écoconception: mon site web au régime"
    vignette_url: null
    image: /img/sketchnotes/2016_10_30_2.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Organisez des cryptoparties !"
    vignette_url: null
    image: /img/sketchnotes/2016_10_29_10.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Questionnement sur l'internet prêt à porter"
    vignette_url: null
    image: /img/sketchnotes/2016_10_29_8.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Libérée, délivrée du syndrôme de l'imposteur ?"
    vignette_url: /img/sketchnotes/2016_10_29_7.JPG
    image: /img/sketchnotes/2016_10_29_7.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Lancer son site à l'international, facile ?"
    vignette_url: /img/vignette/sketchnote/2016_10_29_6.JPG
    image: /img/sketchnotes/2016_10_29_6.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Vers un web + inclusif"
    vignette_url: /img/vignette/sketchnote/2016_10_29_5.JPG
    image: /img/sketchnotes/2016_10_29_5.JPG
    legende: null
    sous_titre: null
  - title: "Paris Web · Anatomie d'une désintoxication au web sans surveillance"
    vignette_url: /img/vignette/sketchnote/2016_10_29_1.JPG
    image: /img/sketchnotes/2016_10_29_1.JPG
    legende: null
    sous_titre: null
---
