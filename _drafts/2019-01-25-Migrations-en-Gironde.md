---
layout: post
title: "Migrations en Gironde"
subtitle: "Chapitre 1"
date: 2018-08-28 20:00:00 +0200
vignette: null
intro: ""
nom_blog: le blog
which_blog: blog_pro
permalink: blog/migrations-en-gironde-1/
---

Cet article est le premier d'une série qui me permettra de partager avec vous des retours d'expérience de comment se passent mes projets et quels sont les différents choix auxquels je suis confrontée en tant que designer. Ce travail qui est souvent invisibilisé au fur et à mesure de la réflexion, et auquel je souhaiterai redonner toute sa place, car il n'y a rien de plus frustrant que d'arriver à la fin d'un projet et d'avoir le droit à un "Ben oui c'est ça qu'on voulait, c'était évident non ?"

Pour éviter de procrastiner, je me donne le droit de peu relire ces articles, afin de laisser une trace imparfaite plutôt que pas de trace du tout.

# Pourquoi ce projet ?

Les agents territoriaux de Gironde utilisent les données de migration à leur disposition pour décider des implantations de nouveaux équipements sur le département, selon la population et ses besoins (plutôt de nouvelles familles, des étudiants ou des personnes agées ? Plutôt piscines, collèges ou EHPAD ?). Or, les données à leur disposition datent de 2013, et nécessitent l'intervention d'une personne ayant des connaissances spécifiques pour pouvoir être utilisées. De plus, avec l'arrivée de la LGV (Ligne Grande Vitesse) à Bordeaux, et le fait que le département est parmi les plus attractif de France, pouvoir objectiver les phénomènes migratoires est d'autant plus intéressant pour les agents.

Or, l'INSEE propose des données qui datent de 2015, donc plus récentes que celles à disposition du département actuellement. De plus, ces données sont nationales, il est donc possible d'envisager qu'un système fonctionnant pour la Gironde pourrait éventuellement être répliqué pour d'autres départements interessés (car le développement se fait en open-source).

Et enfin, avoir une interface ergonomique pour travailler, filtrer les données et les visualiser rapidement, pourrait aussi bien servir les agents territoriaux que les citoyens interessés par ce sujet. Actuellement, il est nécessaire de télécharger le document de l'INSEE qui est… eh bien trop lourd pour mon propre ordinateur, et complètement illisible tel quel, sans avoir un bac +10 en filtres Excel ce qui ne m'enthousiasme guère.

# Le lancement du projet !

Au cours de la réunion de lancement du projet, à Bordeaux, je retrouve :

- David de [dtc innovation](https://dtc-innovation.org/)
- Pascal que j'ai déjà croisé à La contrée,
- Rodolphe, qui apparemment est connaisseur de ces histoires de données de migration

Ce que j'apprends :

Les personnes pour qui le projet est fait :
- Les agents territoriaux (de la direction départementale, des SCoT)
- Les citoyens, journalistes ou curieux, qui voudraient manipuler les données

L'utilité : Pouvoir observer les flux et les analyser afin de savoir où placer les nouveaux équipements, objectiver par des données les phénomènes de migration.

En pleine crise "Gilets Jaunes", cela permet aussi de voir ce qu'il se passe à travers le territoire.

J'apprends que la Gironde est un territoire divisé entre la métropole bordelaise et le reste du département, où se partagent à parts égales la population. Apparemment, on arrive en Gironde en emménageant à Bordeaux, puis lorsque la famille grandit elle s'éloigne de la métropole.

On parle à plusieurs reprises d'un "effet waouh" que j'associe à une envie d'avoir un outil convivial, plus sympathique qu'un tableau excel à filtrer, et donc plus attrayant que ce qui existe actuellement. Sans oublier l'aspect ergonomique, evidemment.

La wishlist :
- Pouvoir comparer 2 territoires
- Pouvoir avancer et reculer dans le temps, pour observer les flux sur plusieurs années (et même prévoir ce qu'il pourrait se passer dans le futur !)
- Rentrer via la donnée territoire pour accéder aux autres données

Note : Il faut rester attentif à ne pas nourrir un argumentaire éventuel sur les migrations d'origines étrangères…

J'apprends que ce qui est le plus important, c'est le solde (la différence entre les entrées et sorties du territoire)

Sur tous les critères proposés par l'INSEE, seuls 4 sont retenus comme à afficher prioritairement :
- L'âge
- Le nombre de personnes par ménage
- La catégorie socio-professionnelle (artisan, ouvrier, cadre…)
- Le type d'activité (en activité, à la retraite, étudiant…)

Nous nous accordons aussi sur un fonctionnement en sprint, qui dureront 2 semaines chacuns. C'est parti pour un premier sprint, et les premières maquettes pour réussir à synthétiser tout ce qui est espéré.

# La première itération : Qu'est-ce que je fais de toutes ces données ?

Je commence en listant les différentes données que je vais devoir intégrer à l'interface :

Des données statiques :
- Les entrées sur le territoire
- Les sorties sur le territoire
- Le solde de ces entrées et sorties

Le temps (3 et +): Entre 2013 et 2015 pour l'instant, mais dans l'objectif de rajouter des années à chaque set supplémentaire sorti par l'INSEE

Les différentes échelles de territoire (3):
- Les ScoT (8 sur le département)
- Les EPCI ou intercommunes (28 sur le département)
- Les communes (535, autant vous dire qu'une sélection sur la carte sera peu aisée)

L'origine des populations nouvellement arrivées (4):
- Autre territoire de Gironde (EPCI donc 28 possibilités)
- Autre région de France
- Union Européenne
- Hors Union européenne

L'âge (25 paliers, que j'envisage de réduire à 8)

Le nombre de personnes par foyer (7 paliers)

Le type d'activité (7)

La Catégorie Socio-Professionnelle (9)

Sans oublier le territoire sélectionné, celui concerné par toutes ces infos.

Ça en fait des infos !!

Je commence par essayer de regrouper tout ça en grandes catégories, ce qui fait :

- Les filtres (Origine, Age, Nombre personnes par foyer, CSP, Type d'activité)
- Sélection du territoire : Échelle du territoire (SCoT, intercommunes, communes) et Territoire sélectionné
- Infos statiques
- Le temps

Pfiou on y voit plus clair !

Je pars sur une organisation avec un écran divisé en deux, une carte à gauche car j'ai beaucoup de données géographiques et que le but est tout de même d'avoir des infos sur les migrations, et une partie droite plus centrée sur les informations et les filtres.

En découle mes premiers schémas. Je cherche à la façon de placer au mieux les filtres, en m'inspirant de Sarenza et Google Font. Il faut pouvoir facilement switcher d'un filtre à l'autre pour faire ses réglages et voir les incidences sur le reste des données.
J'hésite entre une navigation en onglets ou en menu dépliant.
Les onglets permettent de passer rapidement de l'un à l'autre, mais limitent la visualisation à un type d'informations à la fois. Le menu dépliant permet de voir plus d'informations à la fois, mais est-ce qu'il sera souvent nécessaire de voir plusieurs infos en même temps ?

J'en viens rapidement à me reposer la question de ma catégorie "Filtres", et surtout du positionnement de l'onglet "Origine" qui est l'information la plus recherchée, puisque l'application est quand même faite pour… visualiser les données de migration.

De plus, la carte à gauche, qui prendrait une grande partie de l'écran, serait juste là pour…sélectionner un territoire ? Quel gâchis de ne l'utiliser que pour ça !

Je revois mes plans :

A gauche, sur la carte, la sélection d'un territoire et les données géographiques à visualiser.

A droite, les données chiffrées et les filtres qui permettent de les moduler.

En plus, comme ça il est possible de moduler les filtres et de voir en même temps leur incidence sur les données géographiques.


Ok, je parle des données géographiques mais…quelles données géographiques ?
D'où viennent les personnes ? Où vont les personnes ? Je ne peux pas représenter les deux en même temps sur la carte…

Je décide donc de rajouter un bouton switch pour que, lorsque l'on sélectionne un territoire, soit visualisé :
- D'où viennent les personnes ?
- Où vont les personnes ?

Et j'ai un vrai intérêt à avoir une grande carte sur le coté !

# Les prochains points à voir…

- Comment rendre cette interface plus jolies
- Avoir des graphiques plus enthousiasmants que des histogrammes mais qui représentent tout de même correctement les données
- Amener le système de filtres de façon ergonomique, pour passer de l'un à l'autre notamment.
