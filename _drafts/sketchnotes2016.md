---
title: 2016
sketchnotes:
  - title: "ApéroWeb · Kit de survie à l'attention des aventuriers du web au pays du Big Data"
    vignette_url: null
    image: /img/sketchnotes/2017_02_06.JPG
    legende: null
    sous_titre: null
  - title: "La santé au travail - partie 2"
    vignette_url: null
    image: /img/sketchnotes/2016_12_13_3.JPG
    legende: null
    sous_titre: null
  - title: "La santé au travail - partie 1"
    vignette_url: null
    image: /img/sketchnotes/2016_12_13_2.JPG
    legende: null
    sous_titre: null
  - title: "Le cerveau au travail: Optimiser la performance humaine par la neuroergonomie"
    vignette_url: null
    image: /img/sketchnotes/2016_12_13_1.JPG
    legende: null
    sous_titre: null
  - title: "Les fantômes au Japon"
    vignette_url: null
    image: /img/sketchnotes/2016_12_12.JPG
    legende: null
    sous_titre: null
  - title: "Agile Tour · La puissance de l'agilité"
    vignette_url: null
    image: /img/sketchnotes/2016_12_01.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · Rétribuer la contribution aux communs"
    vignette_url: null
    image: /img/sketchnotes/2016_11_20_4.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · BiblioDebout"
    vignette_url: null
    image: /img/sketchnotes/2016_11_20_3.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · Google connait la couleur de votre slip"
    vignette_url: null
    image: /img/sketchnotes/2016_11_20_2.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · Le cloud, la data et le développeur"
    vignette_url: null
    image: /img/sketchnotes/2016_11_20_1.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · Priorité au logiciel libre"
    vignette_url: null
    image: /img/sketchnotes/2016_11_19_3.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · De l'importance de l'éducation populaire au numérique"
    vignette_url: null
    image: /img/sketchnotes/2016_11_19_2.JPG
    legende: null
    sous_titre: null
  - title: "Capitole du Libre · Introduction aux communs"
    vignette_url: null
    image: /img/sketchnotes/2016_11_19_1.JPG
    legende: null
    sous_titre: null
  - title: "ApéroWeb · Conception multidisciplinaire - partie 2"
    vignette_url: null
    image: /img/sketchnotes/2016_11_07_02.JPG
    legende: null
    sous_titre: null
  - title: "ApéroWeb · Conception multidisciplinaire - partie 1"
    vignette_url: null
    image: /img/sketchnotes/2016_11_07_01.JPG
    legende: null
    sous_titre: null
---
